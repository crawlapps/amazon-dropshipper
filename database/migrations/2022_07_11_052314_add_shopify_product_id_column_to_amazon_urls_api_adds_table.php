<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShopifyProductIdColumnToAmazonUrlsApiAddsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amazon_urls_api_adds', function (Blueprint $table) {
            $table->bigInteger('shopify_product_id')->unsigned()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amazon_urls_api_adds', function (Blueprint $table) {
            $table->bigInteger('shopify_product_id')->unsigned()->after('id');
        });
    }
}
