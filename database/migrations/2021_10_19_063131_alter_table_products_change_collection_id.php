<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use DB as DBS;
class AlterTableProductsChangeCollectionId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    public function __construct()
    {
        DBS::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }


    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
          //  $table->longText('collection_id')->default(0)->change();
            $table->longText('collection_id')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}
