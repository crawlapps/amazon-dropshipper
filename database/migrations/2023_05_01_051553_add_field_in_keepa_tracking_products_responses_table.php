<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldInKeepaTrackingProductsResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('keepa_tracking_products_responses', function (Blueprint $table) {
            $table->decimal('old_amazon_price', 8, 2)->default('0.00');
            $table->decimal('new_amazon_price', 8, 2)->default('0.00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('keepa_tracking_products_responses', function (Blueprint $table) {
            $table->removeColumn('old_amazon_price');
            $table->removeColumn('new_amazon_price');
        });
    }
}
