<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('db_plans_id')->unsigned();
            $table->json('contents')->nullable();
            $table->timestamps();

            $table->foreign('db_plans_id')->references('id')->on('plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_contents');
    }
}
