<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImportedProductPlanColumnToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

            Schema::table('products', function (Blueprint $table) {
               $table->string('imported_product_plan')->after('status')->nullable()->comment('Displaying product stats in the MY PRODUCTS page, we will need to display a message for BASIC plan products.');
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('imported_product_plan');
        });

    }
}
