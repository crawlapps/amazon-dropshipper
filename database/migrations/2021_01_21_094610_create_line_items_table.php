<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('line_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('db_order_id');
            $table->string('shopify_lineitem_id')->nullable();
            $table->string('variant_title')->nullable();
            $table->string('product_title')->nullable();
            $table->string('source')->nullable();
            $table->longText('source_url')->nullable();
            $table->string('sku')->nullable();
            $table->string('currency')->nullable();
            $table->decimal('last_price', 8, 2)->nullable();
            $table->decimal('sold_price', 8, 2)->nullable();
            $table->decimal('quantity')->nullable();
            $table->timestamps();

            $table->foreign('db_order_id')->references('id')->on('orders')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('line_items');
    }
}
