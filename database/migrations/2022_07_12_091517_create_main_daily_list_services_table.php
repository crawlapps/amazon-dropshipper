<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainDailyListServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_daily_list_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->bigInteger('shopify_product_id')->default(0);
            $table->bigInteger('product_id')->default(0);
            $table->string('product_url')->nullable();
            $table->decimal('source_price_amazon', 16, 2)->default('0.00');
            $table->string('amazon_new_price')->nullable();
            $table->text('error')->nullable();
            $table->bigInteger('value')->default(0);
            $table->string('list_type')->nullable();            
            $table->json('response')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_daily_list_services');
    }
}
