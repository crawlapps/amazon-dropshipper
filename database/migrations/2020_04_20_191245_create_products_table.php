<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('shopify_id')->unsigned()->nullable();
            $table->bigInteger('collection_id')->unsigned()->nullable();
            $table->string('type', 150);
            $table->char('locale', 2)->default('US');
            $table->enum('source', ['Amazon', 'Walmart'])->default('Amazon');
            $table->string('source_url', 2100);
            $table->longText('description')->nullable();
            $table->string('amazon_associate_link', 2100)->nullable();
            $table->boolean('show_prices')->default(1);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
