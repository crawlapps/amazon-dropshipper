<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsSideBarMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_side_bar_menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('menu_slug')->nullable();
            $table->longText('menu_title')->nullable();
            $table->longText('menu_link_redirect_to')->nullable();
            $table->longText('menu_icon')->nullable();
            $table->boolean('status')->default(1)->comment('0 = Deactiv, 1 = Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_side_bar_menus');
    }
}
