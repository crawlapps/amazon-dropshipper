<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsFaqContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_faq_contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('db_cms_faq_id');
            $table->string('field_type')->nullable()->comment('text,htmlcontent,number.checkbox etc.');
            $table->longText('faq_question')->nullable();
            $table->longText('faq_answer')->nullable();
            $table->boolean('status')->default(1)->comment('0 = Deactiv, 1 = Active');
            $table->foreign('db_cms_faq_id')->references('id')->on('cms_faqs')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_faq_contents');
    }
}
