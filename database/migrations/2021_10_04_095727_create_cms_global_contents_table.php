<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsGlobalContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_global_contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('side_bar_logo')->nullable();
            $table->string('side_bar_logo_text1')->nullable();
            $table->string('side_bar_logo_text2')->nullable();
            $table->longText('footer_cpr_text1')->nullable();
            $table->longText('footer_cpr_link')->nullable();
            $table->longText('footer_cpr_text2')->nullable();
            $table->boolean('status')->default(1)->comment('0 = Deactiv, 1 = Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_global_contents');
    }
}
