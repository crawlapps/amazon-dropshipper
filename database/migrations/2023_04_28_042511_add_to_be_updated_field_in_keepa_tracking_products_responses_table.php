<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddToBeUpdatedFieldInKeepaTrackingProductsResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('keepa_tracking_products_responses', function (Blueprint $table) {
            $table->boolean('to_be_updated')->default(false)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('keepa_tracking_products_responses', function (Blueprint $table) {
            $table->removeColumn('to_be_updated');
        });
    }
}
