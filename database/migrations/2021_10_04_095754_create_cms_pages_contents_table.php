<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsPagesContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_pages_contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('db_cms_pages_id');
            $table->string('field_slug')->nullable();
            $table->string('field_type')->nullable()->comment('text,htmlcontent,number.checkbox etc.');
            $table->longText('field_label')->nullable();
            $table->longText('field_value')->nullable();
            $table->boolean('status')->default(1)->comment('0 = Deactiv, 1 = Active');
            $table->timestamps();
            $table->foreign('db_cms_pages_id')->references('id')->on('cms_pages')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_pages_contents');
    }
}
