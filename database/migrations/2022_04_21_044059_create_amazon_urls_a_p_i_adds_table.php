<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmazonUrlsAPIAddsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amazon_urls_api_adds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('variant_db_id')->default(0);
            $table->bigInteger('record_no')->default(0);
            $table->bigInteger('product_id')->default(0);
            $table->string('product_url')->nullable();
            $table->text('error')->nullable();
            $table->bigInteger('value')->default(0);
            $table->string('old_price')->nullable();
            $table->string('price')->nullable();
            $table->json('response')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amazon_urls_api_adds');
    }
}
