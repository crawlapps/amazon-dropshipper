<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeepaTrackingProductsResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keepa_tracking_products_responses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_id')->default(0);
            $table->string('asin')->nullable();
            $table->string('source_url')->nullable();        
            $table->json('webhook_response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keepa_tracking_products_responses');
    }
}
