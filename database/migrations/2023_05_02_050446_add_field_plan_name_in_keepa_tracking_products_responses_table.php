<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldPlanNameInKeepaTrackingProductsResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('keepa_tracking_products_responses', function (Blueprint $table) {
            $table->string('plan_name', 12)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('keepa_tracking_products_responses', function (Blueprint $table) {
            $table->removeColumn('plan_name');
        });
    }
}
