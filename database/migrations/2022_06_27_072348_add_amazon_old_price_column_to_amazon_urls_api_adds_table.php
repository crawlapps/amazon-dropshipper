<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAmazonOldPriceColumnToAmazonUrlsApiAddsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amazon_urls_api_adds', function (Blueprint $table) {
            $table->string('amazon_old_price')->nullable()->after('amazon_new_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amazon_urls_api_adds', function (Blueprint $table) {
            $table->dropColumn('amazon_old_price');
        });
    }
}
