<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\CmsPages;
use App\Models\CmsPagesContent;
use Illuminate\Support\Str;

class ImportProductsPageAddSectionToCmsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $rows_data = [
                    [
                        'field_slug' => "section2_top_title",
                        'field_type' => "text",
                        'field_label' => "section2_top_title",
                        'field_value' => "Product of the Week",
                    ],
                    [
                        'field_slug' => "section2_col1_link",
                        'field_type' => "text",
                        'field_label' => "section2_col1_link",
                        'field_value' => "#",
                    ],
                    [
                        'field_slug' => "section2_col1_image",
                        'field_type' => "file",
                        'field_label' => "section2_col1_image",
                        'field_value' => "/images/product-import-services/affiliates.png",
                    ],
                    [
                        'field_slug' => "section2_col1_heading",
                        'field_type' => "text",
                        'field_label' => "section2_col1_heading",
                        'field_value' => "Title Title",
                    ],
                    [
                        'field_slug' => "section2_col1_sub_title",
                        'field_type' => "text",
                        'field_label' => "section2_col1_sub_title",
                        'field_value' => "Sales",
                    ],
                    [
                        'field_slug' => "section2_col1_content",
                        'field_type' => "htmlcontent",
                        'field_label' => "section2_col1_content",
                        'field_value' => "Reviews",
                    ],
                    [
                        'field_slug' => "section2_col2_link",
                        'field_type' => "text",
                        'field_label' => "section2_col2_link",
                        'field_value' => "#",
                    ],
                    [
                        'field_slug' => "section2_col2_image",
                        'field_type' => "file",
                        'field_label' => "section2_col2_image",
                        'field_value' => "/images/product-import-services/affiliates.png",
                    ],
                    [
                        'field_slug' => "section2_col2_heading",
                        'field_type' => "text",
                        'field_label' => "section2_col2_heading",
                        'field_value' => "Title Title",
                    ],
                    [
                        'field_slug' => "section2_col2_sub_title",
                        'field_type' => "text",
                        'field_label' => "section2_col2_sub_title",
                        'field_value' => "Sales",
                    ],
                    [
                        'field_slug' => "section2_col2_content",
                        'field_type' => "htmlcontent",
                        'field_label' => "section2_col2_content",
                        'field_value' => "Reviews",
                    ]
            ];


            $db_cms_pages_id = CmsPages::select("id")->where('page_slug',"import-products")->value("id");
            if($db_cms_pages_id) {

                $details = $rows_data;

                foreach ($details as $key => $value) {

                    $exist_content = CmsPagesContent::select("id")->where('db_cms_pages_id',$db_cms_pages_id)->where('field_slug',$value['field_slug'])->value("id");
                    if(!$exist_content) {

                        $details_data = [
                            'db_cms_pages_id' => $db_cms_pages_id,
                            'field_slug' => $value['field_slug'],
                            'field_type' => $value['field_type'],
                            'field_label' => $value['field_label'],
                            'field_value' => $value['field_value'],
                        ];

                        CmsPagesContent::insert($details_data);
                    }
                }
            }

     }
}
