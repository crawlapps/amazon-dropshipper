<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\CmsPages;
use App\Models\CmsPagesContent;
use Illuminate\Support\Str;

class CmsPagesTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //  DB::table('cms_pages')->delete();

        if(CmsPages::count() == 0){
            $rows_data = [
                [
                    'page' => [
                        'page_slug' => "import-products",
                        'page_title' => "Import Products",
                        'page_route_link' => "/import-home",
                        'page_route_name' => "import"
                    ],
                    'page_details' => [
                            [
                             'field_slug' => "import_top_info_text_1",
                             'field_type' => "text",
                             'field_label' => "import_top_info_text_1",
                             'field_value' => "Walmart Import Issue is being investigated.",
                            ],
                            [
                                'field_slug' => "import_top_info_text_2",
                                'field_type' => "text",
                                'field_label' => "import_top_info_text_2",
                                'field_value' => "Download our new Chrome Extenstion to Import with 1 click and Auto fulfill orders. (soon)",
                            ],
                            [
                                'field_slug' => "import_search_heading",
                                'field_type' => "text",
                                'field_label' => "import_search_heading",
                                'field_value' => "Import and DropShip any Product from Amazon or Walmart",
                            ],
                            [
                                'field_slug' => "import_search_sub_title",
                                'field_type' => "text",
                                'field_label' => "import_search_sub_title",
                                'field_value' => "Amazon US, CA, UK, DE, FR, SP, IT, BR, MX, IN and Walmart US",
                            ],
                            [
                                'field_slug' => "import_guid_text1",
                                'field_type' => "text",
                                'field_label' => "import_guid_text1",
                                'field_value' => "Step 1: Go to Amazon or Walmart and Copy the Product Page URL. Step 2: Paste in the box below and click",
                            ],
                            [
                                'field_slug' => "import_guid_text2",
                                'field_type' => "text",
                                'field_label' => "import_guid_text2",
                                'field_value' => "Get Product or download our Chrome extenstion and directy import from the Product page (soon).",
                            ],
                            [
                                'field_slug' => "ad_product_url_placeholder",
                                'field_type' => "text",
                                'field_label' => "ad_product_url_placeholder",
                                'field_value' => "e.g., https://www.amazon.com/dp/B07B7QHGR6/",
                            ],
                            [
                                'field_slug' => "ad_product_btn_title",
                                'field_type' => "text",
                                'field_label' => "ad_product_btn_title",
                                'field_value' => "Get Product",
                            ],
                            [
                                'field_slug' => "ad_amazon_affiliate_label_text",
                                'field_type' => "text",
                                'field_label' => "ad_amazon_affiliate_label_text",
                                'field_value' => "Import for Amazon Affiliate Program",
                            ],
                            [
                                'field_slug' => "report_an_error_text",
                                'field_type' => "text",
                                'field_label' => "report_an_error_text",
                                'field_value' => "Report an error",
                            ],
                            [
                                'field_slug' => "send_suggestion_text",
                                'field_type' => "text",
                                'field_label' => "send_suggestion_text",
                                'field_value' => "Send suggestion",
                            ],
                            [
                                'field_slug' => "import_service1_text",
                                'field_type' => "text",
                                'field_label' => "import_service1_text",
                                'field_value' => "Go with Amazon/Walmart <br> dropship or Amazon Affiliates",
                            ],
                            [
                                'field_slug' => "import_service1_link",
                                'field_type' => "text",
                                'field_label' => "import_service1_link",
                                'field_value' => "https://youtu.be/aS6NjeMxgus",
                            ],
                            [
                                'field_slug' => "import_service1_image",
                                'field_type' => "file",
                                'field_label' => "import_service1_image",
                                'field_value' => "/images/product-import-services/affiliates.png",
                            ],
                            [
                                'field_slug' => "import_service2_text",
                                'field_type' => "text",
                                'field_label' => "import_service2_text",
                                'field_value' => "Set automatic price update <br> for Amazon imported products",
                            ],
                            [
                                'field_slug' => "import_service2_link",
                                'field_type' => "text",
                                'field_label' => "import_service2_link",
                                'field_value' => "#",
                            ],
                            [
                                'field_slug' => "import_service2_image",
                                'field_type' => "file",
                                'field_label' => "import_service2_image",
                                'field_value' => "/images/product-import-services/priceUpdateAuto.png",
                            ],
                            [
                                'field_slug' => "import_service3_text",
                                'field_type' => "text",
                                'field_label' => "import_service3_text",
                                'field_value' => "Import Amazon <br> product reviews",
                            ],
                            [
                                'field_slug' => "import_service3_link",
                                'field_type' => "text",
                                'field_label' => "import_service3_link",
                                'field_value' => "#",
                            ],
                            [
                                'field_slug' => "import_service3_image",
                                'field_type' => "file",
                                'field_label' => "import_service3_image",
                                'field_value' => "/images/product-import-services/productReviews.png",
                            ],
                            [
                                'field_slug' => "import_service4_text",
                                'field_type' => "text",
                                'field_label' => "import_service4_text",
                                'field_value' => "Import with one click and <br> semi-automate orders (soon)",
                            ],
                            [
                                'field_slug' => "import_service4_link",
                                'field_type' => "text",
                                'field_label' => "import_service4_link",
                                'field_value' => "#",
                            ],
                            [
                                'field_slug' => "import_service4_image",
                                'field_type' => "file",
                                'field_label' => "import_service4_image",
                                'field_value' => "/images/product-import-services/semi-automate.png",
                            ],
                            [
                                'field_slug' => "bestseller_resources_logo",
                                'field_type' => "file",
                                'field_label' => "bestseller_resources_logo",
                                'field_value' => "/images/product-import-services/resources.png",
                            ],
                            [
                                'field_slug' => "bestseller_resources_tab1_text",
                                'field_type' => "text",
                                'field_label' => "bestseller_resources_tab1_text",
                                'field_value' => "Search Amazon Best Seller Products ( Only Amazon US )",
                            ],
                            [
                                'field_slug' => "bestseller_resources_tab2_text",
                                'field_type' => "text",
                                'field_label' => "bestseller_resources_tab2_text",
                                'field_value' => "Import with just one Click with our Chrome Extension",
                            ],
                            [
                                'field_slug' => "bestseller_tab1_search_btn_title",
                                'field_type' => "text",
                                'field_label' => "bestseller_tab1_search_btn_title",
                                'field_value' => "Search Best Seller <br> Products",
                            ],
                            [
                                'field_slug' => "bestseller_tab1_search_btn_link",
                                'field_type' => "text",
                                'field_label' => "bestseller_tab1_search_btn_link",
                                'field_value' => "https://www.amazon.com/Best-Sellers/zgbs/amazon-devices/ref=zg_bs_nav_0",
                            ],
                            [
                                'field_slug' => "bestseller_tab2_text",
                                'field_type' => "text",
                                'field_label' => "bestseller_tab2_text",
                                'field_value' => "Install our Chrome Extension (soon)",
                            ],

                    ]
                ],
                [
                    'page' => [
                        'page_slug' => "my-products",
                        'page_title' => "My Products",
                        'page_route_link' => "/products",
                        'page_route_name' => "products"
                    ],
                    'page_details' =>[
                         [
                            'field_slug' => "top_right_note_text",
                            'field_type' => "text",
                            'field_label' => "top_right_note_text",
                            'field_value' => "If after the recent App upgrade you cannot see your products in this page, please email us your store URL including the .myshopify.com to info@AmaZoneDropshipping.com",
                        ],
                        [
                            'field_slug' => "top_left_text1",
                            'field_type' => "text",
                            'field_label' => "top_left_text1",
                            'field_value' => "Current Plan:",
                        ],
                        [
                            'field_slug' => "top_left_text2",
                            'field_type' => "text",
                            'field_label' => "top_left_text2",
                            'field_value' => "Products Imported this Month:",
                        ],
                        [
                            'field_slug' => "top_left_text3",
                            'field_type' => "text",
                            'field_label' => "top_left_text3",
                            'field_value' => "Auto Updates this month:",
                        ],
                        [
                            'field_slug' => "top_left_text4",
                            'field_type' => "text",
                            'field_label' => "top_left_text4",
                            'field_value' => "Affiliate Products Imported this Month:",
                        ],
                        [
                            'field_slug' => "product_per_page",
                            'field_type' => "text",
                            'field_label' => "product_per_page",
                            'field_value' => "15",
                        ],

                    ]
                ],
                [
                    'page' => [
                        'page_slug' => "my-orders",
                        'page_title' => "My Orders",
                        'page_route_link' => "/orders",
                        'page_route_name' => "orders"
                    ],
                    'page_details' => [
                        [
                            'field_slug' => "top_text1",
                            'field_type' => "text",
                            'field_label' => "top_text1",
                            'field_value' => "Here you can view the orders for your imported products. You can see Amazone/Walmart Link, Shopify Link, Client Details, Order Details, Price.",
                        ],
                        [
                            'field_slug' => "top_text2_content",
                            'field_type' => "htmlcontent",
                            'field_label' => "top_text2_content",
                            'field_value' => '<p style="color: #841111;"> The user is responsible for placing and fulfilling your orders. Please <a href="FAQ.php#orderhandling" target="_blank">read the FAQ section \'Order Handling\' or click here</a> for more info. If you have questions please visit our FAQ section or Contact Us.</p>',
                        ],
                    ]
                ],
                [
                    'page' => [
                        'page_slug' => "help-center",
                        'page_title' => "Help Center",
                        'page_route_link' => "/help",
                        'page_route_name' => "help"
                    ],
                    'page_details' => [
                        [
                            'field_slug' => "top_text1",
                            'field_type' => "text",
                            'field_label' => "top_text1",
                            'field_value' => "Help section content (topics, FAQ, etc.)",
                        ],
                        [
                            'field_slug' => "video_tutorial1_text",
                            'field_type' => "text",
                            'field_label' => "video_tutorial1_text",
                            'field_value' => "Video Tutorial #1: Import Products",
                        ],
                        [
                            'field_slug' => "video_tutorial1_youtube_embed_link",
                            'field_type' => "text",
                            'field_label' => "video_tutorial1_youtube_embed_link",
                            'field_value' => "https://www.youtube.com/embed/PMTZUpH8xGs",
                        ],
                        [
                            'field_slug' => "video_tutorial2_text",
                            'field_type' => "text",
                            'field_label' => "video_tutorial2_text",
                            'field_value' => "Video Tutorial #2: Amazon Affiliate Set Up",
                        ],
                        [
                            'field_slug' => "video_tutorial2_youtube_embed_link",
                            'field_type' => "text",
                            'field_label' => "video_tutorial2_youtube_embed_link",
                            'field_value' => "https://www.youtube.com/embed/jAntA3n6epE",
                        ],
                        [
                            'field_slug' => "video_tutorial3_text",
                            'field_type' => "text",
                            'field_label' => "video_tutorial3_text",
                            'field_value' => "Video Tutorial #3: Add AliExpress Reviews+Images to Imported Product",
                        ],
                        [
                            'field_slug' => "video_tutorial3_youtube_embed_link",
                            'field_type' => "text",
                            'field_label' => "video_tutorial3_youtube_embed_link",
                            'field_value' => "https://www.youtube.com/embed/h3Mqp8OmOQc",
                        ],
                        [
                            'field_slug' => "video_tutorial4_text",
                            'field_type' => "text",
                            'field_label' => "video_tutorial4_text",
                            'field_value' => "Video Tutorial #4: How to Install/Activate AliExpress Reviews and Amazon Associates Program IDs",
                        ],
                        [
                            'field_slug' => "video_tutorial4_youtube_embed_link",
                            'field_type' => "text",
                            'field_label' => "video_tutorial4_youtube_embed_link",
                            'field_value' => "https://www.youtube.com/embed/BUt2qTDtOwc",
                        ],
                    ]
                ],
                [
                    'page' => [
                        'page_slug' => "settings",
                        'page_title' => "Settings",
                        'page_route_link' => "/settings",
                        'page_route_name' => "settings"
                    ],
                   'page_details' =>  [
                [
                    'field_slug' => "reviews_step_title",
                    'field_type' => "text",
                    'field_label' => "reviews_step_title",
                    'field_value' => "Steps to Display Reviews:",
                ],
                [
                    'field_slug' => "reviews_step_content",
                    'field_type' => "htmlcontent",
                    'field_label' => "reviews_step_content",
                    'field_value' => '<p>1) Copy code: <b><div class="dropshipping-product-review" data-id="{{ product.id }}"></div> </b></p>
                                    <p>2) Go to Store > Theme > Edit Code</p>
                                    <p>3) Depending on your Theme, you need to find product.liquid </p>
                                    <p>4) Please insert or paste the code below to the section in the Product page where you want to display the Reviews (be carefull to paste code correctly) </p>
                                    <p>5) This code can be removed manually later in teh future if desired.</p>',
                ],
            ]
                 ],

            ];



            foreach ($rows_data as $key => $value) {

                $db_cms_pages_id = CmsPages::insertGetId($value['page']);

                $details = $value['page_details'];

                foreach ($details as $key => $value) {

                    $details_data = [
                        'db_cms_pages_id' => $db_cms_pages_id,
                        'field_slug' => $value['field_slug'],
                        'field_type' => $value['field_type'],
                        'field_label' => $value['field_label'],
                        'field_value' => $value['field_value'],
                    ];

                    CmsPagesContent::insert($details_data);
                }

            }


        } else {
            logger("SEED::CmsSideBarMenu Table is not empty");

        }



    }
}
