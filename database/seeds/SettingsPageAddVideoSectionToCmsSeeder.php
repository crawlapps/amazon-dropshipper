<?php

use App\Models\CmsPages;
use App\Models\CmsPagesContent;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsPageAddVideoSectionToCmsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows_data = [
            [
                'field_slug' => "video_tutorial1_text",
                'field_type' => "text",
                'field_label' => "video_tutorial1_text",
                'field_value' => "Video Tutorial",
            ],
            [
                'field_slug' => "video_tutorial1_youtube_embed_link",
                'field_type' => "text",
                'field_label' => "video_tutorial1_youtube_embed_link",
                'field_value' => "https://www.youtube.com/embed/h3Mqp8OmOQc",
            ],
        ];


        $db_cms_pages_id = CmsPages::select("id")->where('page_slug',"settings")->value("id");
        if($db_cms_pages_id) {

            $details = $rows_data;

            foreach ($details as $key => $value) {

                $exist_content = CmsPagesContent::select("id")->where('db_cms_pages_id',$db_cms_pages_id)->where('field_slug',$value['field_slug'])->value("id");
                if(!$exist_content) {
                    $details_data = [
                        'db_cms_pages_id' => $db_cms_pages_id,
                        'field_slug' => $value['field_slug'],
                        'field_type' => $value['field_type'],
                        'field_label' => $value['field_label'],
                        'field_value' => $value['field_value'],
                    ];

                    CmsPagesContent::insert($details_data);
                }
            }
        }
    }
}
