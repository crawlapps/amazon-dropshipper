<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(PlansTableSeeder::class);
        $this->call(NewSubscriptionPlansTableSeeder::class);
        $this->call(AdminTableDataSeeder::class);
        $this->call(CmsGlobalContentTableDataSeeder::class);
        $this->call(CmsPagesTableDataSeeder::class);
        $this->call(CmsSideBarMenuTableDataSeeder::class);
        $this->call(cmsFaqTableDataSeeder::class);
        $this->call(ImportProductsPageAddSectionToCmsSeeder::class);
        $this->call(SettingsPageAddVideoSectionToCmsSeeder::class);

    }
}
