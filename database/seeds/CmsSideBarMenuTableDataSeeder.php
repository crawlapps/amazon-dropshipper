<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\CmsSideBarMenu;
use Illuminate\Support\Str;

class CmsSideBarMenuTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // CmsSideBarMenu::truncate();

        if(CmsSideBarMenu::count() == 0){

            $rows_data = [
                            [
                                'menu_slug'=>Str::slug("Import Products"),
                                'menu_title'=>"Import Products",
                                'menu_link_redirect_to' =>"/import-home",
                                'menu_icon' =>"/images/sidebar/import.png"
                            ],
                            [
                                'menu_slug'=>Str::slug("My Products"),
                                'menu_title'=>"My Products",
                                'menu_link_redirect_to' =>"/products",
                                'menu_icon' =>"/images/sidebar/task.png"
                            ],
                            [
                                'menu_slug'=>Str::slug("My Orders"),
                                'menu_title'=>"My Orders",
                                'menu_link_redirect_to' =>"/orders",
                                'menu_icon' =>"/images/sidebar/shopping-bag.png"
                            ],
                            [
                                'menu_slug'=>Str::slug("Help / Videos / FAQ"),
                                'menu_title'=>"Help / Videos / FAQ",
                                'menu_link_redirect_to' =>"/help",
                                'menu_icon' =>"/images/sidebar/information.png"
                            ],
                            [
                                'menu_slug'=>Str::slug("Settings"),
                                'menu_title'=>"Settings",
                                'menu_link_redirect_to' =>"/settings",
                                'menu_icon' =>"/images/sidebar/settings.png"
                            ]
                       ];


            CmsSideBarMenu::insert($rows_data);


        } else {
            logger("SEED::CmsSideBarMenu Table is not empty");
        }
    }
}
