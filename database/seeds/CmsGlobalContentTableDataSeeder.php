<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\CmsGlobalContent;
class CmsGlobalContentTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // CmsGlobalContent::truncate();

        if(CmsGlobalContent::count() == 0){
            $rows_data = [
                [
                    'side_bar_logo'=>"/images/sidebar/logo.png",
                    'side_bar_logo_text1'=>"AmaZone",
                    'side_bar_logo_text2'=>"DropShipper",
                    'footer_cpr_text1' =>"AmaZone Dropshipper",
                    'footer_cpr_link' =>"https://amazonedropshipper.com",
                    'footer_cpr_text2' => "© 2019 AmaZone Dropshipper.",
                ]
            ];

            CmsGlobalContent::insert($rows_data);

        } else {
            logger("SEED::CmsGlobalContent Table is not empty");
        }

    }
}
