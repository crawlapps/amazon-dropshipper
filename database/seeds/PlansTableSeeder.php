<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        if (DB::table('plans')->count() == 0) {
            DB::table('plans')->insert([
                'type' => 'ONETIME',
                'name' => 'FREE',
                'price' => 0.5,
                'capped_amount' => 10.00,
                'terms' => 'FREE',
                'trial_days' => 0,
                'test' => 1,
                'on_install' => 0,
                'max_regular_product_import' => 10,
                'max_affiliate_product_import' => 10,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);

            DB::table('plans')->insert([
                'type' => 'RECURRING',
                'name' => 'PRO',
                'price' => 4.99,
                'capped_amount' => 15.00,
                'terms' => 'PRO',
                'trial_days' => 0,
                'test' => 1,
                'on_install' => 0,
                'max_regular_product_import' => 15,
                'max_affiliate_product_import' => 'unlimited',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);

            DB::table('plans')->insert([
                'type' => 'RECURRING',
                'name' => 'GOLD',
                'price' => 9.99,
                'capped_amount' => 100.00,
                'terms' => 'GOLD',
                'trial_days' => 0,
                'test' => 1,
                'on_install' => 0,
                'max_regular_product_import' => 100,
                'max_affiliate_product_import' => 'unlimited',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);

            DB::table('plans')->insert([
                'type' => 'RECURRING',
                'name' => 'ULTIMATE',
                'price' => 25.99,
                'capped_amount' => 500,
                'terms' => 'ULTIMATE',
                'trial_days' => 0,
                'test' => 1,
                'on_install' => 0,
                'max_regular_product_import' => 500,
                'max_affiliate_product_import' => 'unlimited',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);

            DB::table('plans')->insert([
                'type' => 'RECURRING',
                'name' => 'ULTIMATE PLUS',
                'price' => 99.99,
                'capped_amount' => 5000.00,
                'terms' => 'ULTIMATE PLUS',
                'trial_days' => 0,
                'test' => 1,
                'on_install' => 0,
                'max_regular_product_import' => 5000,
                'max_affiliate_product_import' => 'unlimited',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);
        }
    }
}
