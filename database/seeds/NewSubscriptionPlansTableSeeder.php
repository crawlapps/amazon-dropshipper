<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Plans;
use App\PlanContents;
use App\User;
class NewSubscriptionPlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $plans =[
              [
                'type' => 'RECURRING',
                'name' => 'Basic',
                'price' => 15,
                'interval' => "EVERY_30_DAYS",
                'capped_amount' => 10.00,
                'terms' => 'Every 30 days cycle starts at subscription day',
                'trial_days' => 0,
                'test' => 1,
                'on_install' => 0,
                'max_regular_product_import' => 10,
                'max_affiliate_product_import' => 500,
                'contents' => [
                        "is_popular" => "false",
                        "header_background_color" => "#337AB7",
                        "header_plan_background_color" => "rgba(51,122,183,0.2)",
                        "save_text" => "<span>Make it 5$/mo with yearly plan!</span>",
                        "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
                        "row2_html" => "<p class='manul-price-stock'>Manual price/stock sync </p>",
                        "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
                        "row4_html" => "<p class='no-product-stat'>No Product Data/Statistics</p>",
                        "row5_html" => "<p><b>Unlimitted Amazon <br> Affiliates Import</br></p>",
                        "row6_html" => "<p class='n-a'>n/a</p>",
                       ]
              ],
              [
                    'type' => 'RECURRING',
                    'name' => 'Suite',
                    'price' => 50,
                    'interval' => "EVERY_30_DAYS",
                    'capped_amount' => 10.00,
                    'terms' => 'Every 30 days cycle starts at subscription day',
                    'trial_days' => 0,
                    'test' => 1,
                    'on_install' => 0,
                    'max_regular_product_import' => 100,
                    'max_affiliate_product_import' => 500,
                    'contents' => [
                        "is_popular" => "true",
                        "header_background_color" => "rgb(134 193 71)",
                        "header_plan_background_color" => "rgba(134,193,71,0.2)",
                        "save_text" => "<span>Just 25$/mo with yearly plan!</span>",
                        "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
                        "row2_html" => "<p><b>Automated daily prices/stock Sync</b> with Amazon to update your <br>store prices/stock automatically <br>(Only for 30 days) </p>",
                        "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
                        "row4_html" => "<p><b>Get Product Data & Statistic</b> <ul><li>* Estimated Monthly Sales</li><li>* Best Seller Rank (BSR)</li><li>* Price History Chart</li><li>* Best Seller Categories<br> (Only Amazon)</li></ul> </p>",
                        "row5_html" => "<p><b>Unlimitted Amazon <br> Affiliates Import</br></p>",
                        "row6_html" => "<p><b>Automated Order <br> Fulfilment option </b><br>(soon)</p>",
                    ]
              ],
              [
                'type' => 'RECURRING',
                'name' => 'Startup',
                'price' => 35,
                'interval' => "EVERY_30_DAYS",
                'capped_amount' => 10.00,
                'terms' => 'Every 30 days cycle starts at subscription day',
                'trial_days' => 0,
                'test' => 1,
                'on_install' => 0,
                'max_regular_product_import' => 25,
                'max_affiliate_product_import' => 500,
                'contents' => [
                    "is_popular" => "false",
                    "header_background_color" => "#f1620f",
                    "header_plan_background_color" => "rgba(241,98,15,0.2)",
                    "save_text" => "<span>Just 25$/mo with yearly plan!</span>",
                    "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
                    "row2_html" => "<p><b>Automated daily prices/stock Sync</b> with Amazon to update your <br>store prices/stock automatically <br>(Only for 30 days) </p>",
                    "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
                    "row4_html" => "<p><b>Get Product Data & Statistic</b> <ul><li>* Estimated Monthly Sales</li><li>* Best Seller Rank (BSR)</li><li>* Price History Chart</li><li>* Best Seller Categories<br> (Only Amazon)</li></ul> </p>",
                    "row5_html" => "<p><b>Unlimitted Amazon <br> Affiliates Import</br></p>",
                    "row6_html" => "<p><b>Automated Order <br> Fulfilment option </b><br>(soon)</p>",
                  ]
              ],
              [
                'type' => 'RECURRING',
                'name' => 'Pro',
                'price' => 300,
                'interval' => "EVERY_30_DAYS",
                'capped_amount' => 10.00,
                'terms' => 'Every 30 days cycle starts at subscription day',
                'trial_days' => 0,
                'test' => 1,
                'on_install' => 0,
                'max_regular_product_import' => 500,
                'max_affiliate_product_import' => 500,
                'contents' => [
                    "is_popular" => "false",
                    "header_background_color" => "#000",
                    "header_plan_background_color" => "rgba(0,0,0,0.2)",
                    "save_text" => "<span>Just 165/mo with yearly plan!</span>",
                    "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
                    "row2_html" => "<p><b>Automated daily prices/stock Sync</b> with Amazon to update your <br>store prices/stock automatically <br>(Only for 30 days) </p>",
                    "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
                    "row4_html" => "<p><b>* Get Product Data & Statistic</b> <ul><li>* Estimated Monthly Sales</li><li>* Best Seller Rank (BSR)</li><li>* Price History Chart</li><li>* Best Seller Categories<br> (Only Amazon)</li></ul> </p>",
                    "row5_html" => "<p><b>Unlimitted Amazon <br> Affiliates Import</br></p>",
                    "row6_html" => "<p><b>Automated Order <br> Fulfilment option </b><br>(soon)</p>",
                  ]

              ],
              [
                'type' => 'RECURRING',
                'name' => 'Basic',
                'price' => 60,
                'interval' => "ANNUAL",
                'capped_amount' => 10.00,
                'terms' => 'only one time at subscription time',
                'trial_days' => 0,
                'test' => 1,
                'on_install' => 0,
                'max_regular_product_import' => 10,
                'max_affiliate_product_import' => 500,
                'contents' => [
                    "is_popular" => "false",
                    "header_background_color" => "#337AB7",
                    "header_plan_background_color" => "rgba(51,122,183,0.2)",
                    "save_text" => "<span>Save 180$ with yearly plan!</span>",
                    "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
                    "row2_html" => "<p class='manul-price-stock'>Manual price/stock sync </p>",
                    "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
                    "row4_html" => "<p class='no-product-stat'>No Product Data/Statistics</p>",
                    "row5_html" => "<p><b>Unlimitted Amazon <br> Affiliates Import</br></p>",
                    "row6_html" => "<p class='n-a'>n/a</p>",
                  ]
              ],
              [
                    'type' => 'RECURRING',
                    'name' => 'Suite',
                    'price' => 300,
                    'interval' => "ANNUAL",
                    'capped_amount' => 10.00,
                    'terms' => 'only one time at subscription time',
                    'trial_days' => 0,
                    'test' => 1,
                    'on_install' => 0,
                    'max_regular_product_import' => 100,
                    'max_affiliate_product_import' => 500,
                    'contents' => [
                        "is_popular" => "true",
                        "header_background_color" => "rgb(134 193 71)",
                        "header_plan_background_color" => "rgba(134,193,71,0.2)",
                        "save_text" => "<span>Save 300$ with yearly plan!</span>",
                        "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
                        "row2_html" => "<p><b>Automated daily prices/stock Sync</b> with Amazon to update your <br>store prices/stock automatically <br>(Only for 30 days) </p>",
                        "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
                        "row4_html" => "<p><b>Get Product Data & Statistic</b> <ul><li>* Estimated Monthly Sales</li><li>* Best Seller Rank (BSR)</li><li>* Price History Chart</li><li>* Best Seller Categories<br> (Only Amazon)</li></ul> </p>",
                        "row5_html" => "<p><b>Unlimitted Amazon <br> Affiliates Import</br></p>",
                        "row6_html" => "<p><b>Automated Order <br> Fulfilment option </b><br>(soon)</p>",
                    ]

                ],
              [
                'type' => 'RECURRING',
                'name' => 'Startup',
                'price' => 180,
                'interval' => "ANNUAL",
                'capped_amount' => 10.00,
                'terms' => 'only one time at subscription time',
                'trial_days' => 0,
                'test' => 1,
                'on_install' => 0,
                'max_regular_product_import' => 25,
                'max_affiliate_product_import' => 500,
                'contents' => [
                    "is_popular" => "false",
                    "header_background_color" => "#f1620f",
                    "header_plan_background_color" => "rgba(241,98,15,0.2)",
                    "save_text" => "<span>Save 240$ with yearly plan!</span>",
                    "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
                    "row2_html" => "<p><b>Automated daily prices/stock Sync</b> with Amazon to update your <br>store prices/stock automatically <br>(Only for 30 days) </p>",
                    "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
                    "row4_html" => "<p><b>Get Product Data & Statistic</b> <ul><li>* Estimated Monthly Sales</li><li>* Best Seller Rank (BSR)</li><li>*  Price History Chart</li><li>*  Best Seller Categories<br> (Only Amazon)</li></ul> </p>",
                    "row5_html" => "<p><b>Unlimitted Amazon <br> Affiliates Import</br></p>",
                    "row6_html" => "<p><b>Automated Order <br> Fulfilment option </b><br>(soon)</p>",
                  ]

              ],
              [
            'type' => 'RECURRING',
            'name' => 'Pro',
            'price' => 2000,
            'interval' => "ANNUAL",
            'capped_amount' => 10.00,
            'terms' => 'only one time at subscription time',
            'trial_days' => 0,
            'test' => 1,
            'on_install' => 0,
            'max_regular_product_import' => 500,
            'max_affiliate_product_import' => 500,
            'contents' => [
                "is_popular" => "false",
                "header_background_color" => "#000",
                "header_plan_background_color" => "rgba(0,0,0,0.2)",
                "save_text" => "<span>Save 1,588$ with yearly plan!</span>",
                "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
                "row2_html" => "<p><b>Automated daily prices/stock Sync</b> with Amazon to update your <br>store prices/stock automatically <br>(Only for 30 days) </p>",
                "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
                "row4_html" => "<p><b>Get Product Data & Statistic</b> <ul><li>* Estimated Monthly Sales</li><li>* Best Seller Rank (BSR)</li><li>* Price History Chart</li><li>* Best Seller Categories<br> (Only Amazon)</li></ul> </p>",
                "row5_html" => "<p><b>Unlimitted Amazon <br> Affiliates Import</br></p>",
                "row6_html" => "<p><b>Automated Order <br> Fulfilment option </b><br>(soon)</p>",
             ]
        ]
        ];

            $plan_free_contents =  [
                "is_popular" => "false",
                "header_background_color" => "#6c6c6c",
                "header_plan_background_color" => "rgba(108,108,108,0.2)",
                "save_text" => "<span class='no-save-text'> </span>",
                "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
                "row2_html" => "<p><b>Automated daily prices/stock Sync</b> with Amazon to update your <br>store prices/stock automatically <br>(Only for 30 days) </p>",
                "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
                "row4_html" => "<p><b>Get Product Data & Statistic</b> <ul><li>* Estimated Monthly Sales</li><li>* Best Seller Rank (BSR)</li>
                                <li>* Price History Chart</li><li>* Best Seller Categories<br> (Only Amazon)</li></ul> </p>",
                "row5_html" => "<p>10 Amazon Affliates <br> Imports</p>",
                "row6_html" => "<p>Automated Order <br> Fulfilment option <br>(just 1st order) <br>(soon)</p>",
            ];

           $matchTheseContent = ['db_plans_id' => 1];

           $first_plan_info =  DB::table('plan_contents')->updateOrInsert($matchTheseContent, [
                'db_plans_id' => 1,
                'contents' =>  json_encode($plan_free_contents)
            ]);


        foreach($plans as $plan) {

            // $matchThese = ['name' => $plan['name']];

//            $planData = [
//                'type' => $plan['type'],
//                'name' => $plan['name'],
//                'price' => $plan['price'],
//                'interval' => $plan['interval'],
//                'capped_amount' => $plan['capped_amount'],
//                'terms' => $plan['terms'],
//                'trial_days' => $plan['trial_days'],
//                'test' => $plan['test'],
//                'on_install' => $plan['on_install'],
//                'max_regular_product_import' => $plan['max_regular_product_import'],
//                'max_affiliate_product_import' => $plan['max_affiliate_product_import'],
//                'created_at' => date("Y-m-d H:i:s"),
//                'updated_at' => date("Y-m-d H:i:s")
//            ];

            $existPlan = Plans::where('name', '=', $plan['name'])->where('interval', $plan['interval'])->first();

            if ($existPlan) {

                $user = User::where('plan_id', $existPlan->id)->first();
                if ($user) {
                    $user->plan_id = null;
                    $user->save();
                }

                $deleteCharges = DB::table('charges')->where('plan_id', $existPlan->id)->delete();

                $deleteCounter = DB::table('counters')->where('plan_id', $existPlan->id)->delete();

                $deletePlan = DB::table('plans')->where('id', $existPlan->id)->delete();

            }

        }


         foreach($plans as $plan) {

            $newPlan = $existPlan ? $existPlan : Plans::firstOrNew(['name' => $plan['name'],'interval' => $plan['interval']]);
            $newPlan->type =  $plan['type'];
            $newPlan->name = $plan['name'];
            $newPlan->price = $plan['price'];
            $newPlan->interval = $plan['interval'];
            $newPlan->capped_amount = $plan['capped_amount'];
            $newPlan->terms = $plan['terms'];
            $newPlan->trial_days = $plan['trial_days'];
            $newPlan->test = $plan['test'];
            $newPlan->on_install = $plan['on_install'];
            $newPlan->max_regular_product_import = $plan['max_regular_product_import'];
            $newPlan->max_affiliate_product_import = $plan['max_affiliate_product_import'];
            $newPlan->save();


            //$newPlan = DB::table('plans')->insert($planData);


            if($newPlan->id){

                $matchTheseContent = ['db_plans_id' => $newPlan->id];

                DB::table('plan_contents')->updateOrInsert($matchTheseContent, [
                    'db_plans_id' => $newPlan->id,
                    'contents' => json_encode($plan['contents'])
                ]);


            }

        }
    }
}
