<!doctype html>
<html lang="en">
    <head>
        <title>Amazone Dropshipping - The App to DropShipp Amazon Products</title>
         <meta name="description" content="Sell and DropShip Amazon proucts from youur website. Dropshipping Amazon marketplace products made easy!">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <!-- Main CSS -->
        <link rel="stylesheet" href="{{ asset('css/front-style.css') }}">

        <!-- Font Awesome -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <style>
            body{
                background-color: white;
                color: #000;
            }
            h2, h3 {
                color: #000;
            }
            .container .jumbotron {
                background: #fff !important;
            }
            #loom-companion-mv3{
                display: none;
            }
            @media (min-width: 576px) {
                .jumbotron {
                    padding: 5rem 2.5rem;
                }
            }


        </style>
    </head>

    <body id="home" data-spy="scroll" data-target="#navbar-top" data-offset="200">

        <!-- Main navigation -->
        <nav id="navbar-top" class="navbar fixed-top navbar-expand-md navbar-dark">
            <div class="container">

                <!-- Company name shown on mobile -->
                <a class="navbar-brand" href="#"><span>Amazone</span><span style="color:#ffee00;">DropShipping</span></a>

                <!-- Mobile menu toggle -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNavbar" aria-controls="mainNavbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- Main navigation items -->
                <div class="collapse navbar-collapse" id="mainNavbar">
                    <ul class="navbar-nav mr-auto">
                        <li data-toggle="collapse" data-target=".navbar-collapse.show" class="nav-item">
                                <a class="nav-link" href="/#price">What is this Amazone DropShipping App?</a>
                        </li>

                         <li data-toggle="collapse" data-target=".navbar-collapse.show" class="nav-item">
                                <a class="nav-link" href="/#install">Download App</a>
                        </li>

                         <li data-toggle="collapse" data-target=".navbar-collapse.show" class="nav-item">
                                <a class="nav-link" href="/#pricing">Price</a>
                        </li>

                        <li data-toggle="collapse" data-target=".navbar-collapse.show" class="nav-item">
                                <a class="nav-link" href="/#FAQ">FAQ</a>
                        </li>

                        <li data-toggle="collapse" data-target=".navbar-collapse.show" class="nav-item">
                                <a class="nav-link" href="/#about">About</a>
                        </li>

                        <li data-toggle="collapse" data-target=".navbar-collapse.show" class="nav-item">
                            <a class="nav-link" href="#">Privacy Policy</a>
                    </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="" class="">
            <div class="container">
                <div class="jumbotron px-0">
                    <h2 class="pb-0">Privacy Policy</h2>
                    <p class="">Last updated: July, 2023</p>

                    <p class="pt-2">This Privacy Policy describes Our policies and procedures on the collection, use and disclosure of Your information when You use the Service and tells You about Your privacy rights.</p>
                    <p>By using the Service, You agree to the collection and use of information in accordance with this Privacy Policy.</p>

                    <h2 class="pt-2">Interpretation and Definitions</h2>
                    <h3 class="pt-2">Interpretation</h3>
                    <p>The words of which the initial letter is capitalized have meanings defined under the following conditions. The following definitions shall have the same meaning regardless of whether they appear in singular or in plural.</p>

                    <h3 class="pt-2">Definitions</h3>
                    <p>For the purposes of this Privacy Policy:</p>
                    <ul>
                        <li> <b>Account</b> means a unique account created for You to access our Service or parts of our Service.</li>
                        <li><b>Company</b> (referred to as either "the Company", "We", "Us" or "Our" in this Agreement) refers to Amazone DropShipper.</li>
                        <li><b>Cookies</b> are small files that are placed on Your computer, mobile device or any other device by a website, containing the details of Your browsing history on that website among its many uses.</li>
                        <li><b>Country</b> refers to: Your Country.</li>
                        <li><b>Device</b> means any device that can access the Service such as a computer, a cellphone or a digital tablet.</li>
                        <li><b>Personal Data</b> is any information that relates to an identified or identifiable individual such as you or a third party.</li>
                        <li><b>Service</b> refers to our web application.</li>
                        <li><b>Service Provider</b> means any natural or legal person who processes the data on behalf of the Company. It refers to third-party companies or individuals employed by the Company to facilitate the Service, to provide the Service on behalf of the Company, to perform services related to the Service or to assist the Company in analyzing how the Service is used.</li>
                        <li><b>Usage Data</b> refers to data collected automatically, either generated by the use of the Service or from the Service infrastructure itself (for example, the duration of a page visit).</li>
                        <li><b>Website</b> refers to Amazone DropShipping, accessible from <a href="https://amazonedropshipping.com/">https://amazonedropshipping.com/</a></li>
                        <li><b>You</b> means the individual accessing or using the Service, or the company, or other legal entity on behalf of which such individual is accessing or using the Service, as applicable.</li>
                        <li><b>Your Customer:</b> means the individual that request or purchase your products or services through your online business or store.</li>
                    </ul>

                    <h2 class="pt-2">Collecting and Using Your Personal Data and Customers Personal Data </h2>
                    <h3 class="pt-2">Types of Data Collected</h3>
                    <h3 class="pt-2">Personal Data</h3>
                    <p>While using Our Service, We may ask You to provide Us with certain personally identifiable information that can be used to contact or identify You and/or your customers. Personally identifiable information may include:</p>
                    <ul>
                        <li>Email address</li>
                        <li>Shopify website</li>
                        <li>Name and last name</li>
                        <li>Address</li>
                        <li>Phone number</li>
                    </ul>
                    

                    <h3 class="mt-2">Usage Data</h3>
                    <p>Usage Data is collected automatically when using the Service.</p>
                    <p>Usage Data may include information such as Your Device's Internet Protocol address (e.g. IP address), browser type, browser version, the pages of our Service that You visit, the time and date of Your visit, the time spent on those pages, unique device identifiers and other diagnostic data.</p>
                    <p>When You access the Service by or through a mobile device, We may collect certain information automatically, including, but not limited to, the type of mobile device You use, Your mobile device unique ID, the IP address of Your mobile device, Your mobile operating system, the type of mobile Internet browser You use, unique device identifiers and other diagnostic data.</p>


                    <h3 class="mt-2">Tracking Technologies and Cookies</h3>
                    <p>We don’t use any cookies or tracking technologies however we might rely on some <b>Web Beacons</b> to understand (for example) if our email recipients opened our email or not.</p>


                    <h3 class="mt-2">Use of Your Personal Data and Customers Personal Data</h3>
                    <p>The Company may use your Personal Data and/or your customers personal data for the following purposes:</p>
                    <ul>
                        <li><b>To provide and maintain our Service</b>, including to monitor the usage of our Service.</li>
                        <li><b>To manage Your Account:</b> to manage Your registration as a user of the Service. The Personal Data You provide can give You access to different functionalities of the Service that are available to You as a registered user.</li>
                        <li><b>For the performance of a contract</b>: the development, compliance and undertaking of the purchase contract for the products, items or services You have purchased or of any other contract with Us through the Service.</li>
                        <li><b>To contact You</b>: To contact You by email or other equivalent forms of electronic communication regarding updates or informative communications related to the functionalities, products or contracted services, including the security updates, when necessary or reasonable for their implementation.</li>
                        <li><b>To provide You</b> with news, special offers and general information about other goods, services and events which we offer that are similar to those that you have already purchased or enquired about unless You have opted not to receive such information.</li>
                        <li><b>To manage Your requests</b>: To attend and manage Your requests to Us.</li>
                        <li><b>For business transfers</b>: We may use Your information to evaluate or conduct a merger, divestiture, restructuring, reorganization, dissolution, or other sale or transfer of some or all </li>
                        <li>of Our assets, whether as a going concern or as part of bankruptcy, liquidation, or similar proceeding, in which Personal Data held by Us about our Service users is among the assets transferred.</li>
                        <li><b>For other purposes</b>: We may use Your information for other purposes, such as data analysis, identifying usage trends, determining the effectiveness of our promotional campaigns and to evaluate and improve our Service, products, services, marketing and your experience.</li>
                    </ul>

                    <p>We may share Your personal information in the following situations:</p>
                    <ul>
                        <li><b>With Service Providers:</b> We may share Your personal information with Service Providers to monitor and analyze the use of our Service, to contact You.</li>
                        <li><b>For business transfers:</b>We may share or transfer Your personal information in connection with, or during negotiations of, any merger, sale of Company assets, financing, or acquisition of all or a portion of Our business to another company.</li>
                        <li><b>With Affiliates:</b>We may share Your information with Our affiliates, in which case we will require those affiliates to honor this Privacy Policy.</li>
                        <li><b>With business partners: </b>We may share Your information with Our business partners or joint venture partners.</li>
                    </ul>


                    <h3 class="mt-2">Retention of Your Personal Data and Customers Personal Data</h3>
                    <p>The Company will retain Your Personal Data and customers personal data only for as long as is necessary for the purposes set out in this Privacy Policy. We will retain and use Your Personal Data to the extent necessary to comply with our legal obligations (for example, if we are required to retain your data to comply with applicable laws), resolve disputes, and enforce our legal agreements and policies.</p>
                    <p>The Company will also retain Usage Data and customers personal data for internal analysis purposes. Usage Data is generally retained for a shorter period of time, except when this data is used to strengthen the security or to improve the functionality of Our Service, or We are legally obligated to retain this data for longer time periods.</p>


                    <h3 class="mt-2">Transfer of Your Personal Data and Customers Personal Data</h3>
                    <p>Part of your information and customers information, including Personal Data, is processed at our Service providers places where their processing centres are located. It means that this information may be transferred to — and maintained on — computers located outside of Your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from Your jurisdiction.</p>
                    <p>Your consent to this Privacy Policy followed by Your submission of such information represents Your agreement to that transfer.</p>
                    <p>The Company will take all steps reasonably necessary to ensure that Your data and customers data is treated securely and in accordance with this Privacy Policy.</p>


                    <h3 class="pt-2">Disclosure of Your Personal Data and Customers Personal Data</h3>
                    
                    <h3 class="mt-2">Business Acquisition or Joint Ventures</h3>
                    <p>If the Company is involved in a merger, acquisition or joint Venture, Your Personal Data and customers personal data may be transferred. We will provide notice before Your Personal Data is transferred and becomes subject to a different Privacy Policy.</p>

                    <h3 class="mt-2">Law enforcement</h3>
                    <p>Under certain circumstances, the Company may be required to disclose Your Personal Data and/or customers personal data if required to do so by law or in response to valid requests by public authorities (e.g. a court or a government agency).</p>

                    <h3 class="mt-2">Other legal requirements</h3>
                    <p>The Company may disclose Your Personal Data and/or customers personal data in the good faith belief that such action is necessary to:</p>
                    <ul>
                        <li>Comply with a legal obligation</li>
                        <li>Protect and defend the rights or property of the Company</li>
                        <li>Prevent or investigate possible wrongdoing in connection with the Service</li>
                        <li>Protect the personal safety of Users of the Service or the public</li>
                        <li>Protect against legal liability</li>
                    </ul>


                    <h3 class="mt-2">We don’t sell yours or your customers information or Personal Data</h3>
                    <p>We don’t, under any circumstance, sell or share your information or personal data or your customers information with any other entity that is not explicitly indicated in this Privacy Policy.</p>


                    <h2 class="mt-2">Security of Your Personal Data and Customers Personal Data</h2>
                    <p>The security of Your Personal Data and customers personal data is important to Us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While We strive to use commercially acceptable means to protect Personal Data, We cannot guarantee its absolute security.</p>


                    <h2 class="mt-2">User Rights </h2>
                    <p>You have the right to access, correct, or delete your personal information that we have collected. You may also have the right to restrict or object to certain types of processing of your personal information. If you have any questions or concerns about how we handle your personal information, please contact us using the contact information provided below.</p>


                    <h2 class="mt-2">Links to Other Websites</h2>
                    <p>Our Service may contain links to other websites that are not operated by Us. If You click on a third party link, You will be directed to that third party's site. We strongly advise You to review the Privacy Policy of every site You visit.</p>
                    <p>We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p>


                    <h2 class="mt-2">Changes to this Privacy Policy</h2>
                    <p>We may update Our Privacy Policy from time to time. We will notify You of any changes by posting the new Privacy Policy on this page.</p>
                    <p>We will let You know via email and/or a prominent notice on Our Service, prior to the change becoming effective and update the "Last updated" date at the top of this Privacy Policy.</p>
                    <p>You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.</p>


                    <h2 class="mt-2">Contact Us</h2>
                    <p>If you have any questions about this Privacy Policy, You can contact us:</p>
                    <ul>
                        <li>By email: <a href="mailto:info@AmazoneDropShipping.com">info@AmazoneDropShipping.com</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Bootcamp JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    </body>
</html>
