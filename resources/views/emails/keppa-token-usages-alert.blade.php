<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>AmaZone DropShipper</title>

    <style>

        p{
            margin:0 0 16px;
        }
        h4{
            margin:0 0 12px;
        }
        .title{
            color:#2d2f69;
            font-size:18px;
        }
        .email_logo_top{
            display: block;
            margin: 20px auto;
            width: 70px;
        }

    </style>
</head>

<body bgcolor="#f5f5f5" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" offset="0" style="padding:70px 0 70px 0;">
<table width="600" height="auto" align="center" cellpadding="0" cellspacing="0" style="background-color:#fdfdfd; border:1px solid #dcdcdc; border-radius:3px !important;">
    <tr>
        <td width="600" height="auto" bgcolor="#000" border="0" style="padding:36px 48px; display:block; margin: 0px auto;">
            <h1 style="color:#ffffff; font-family:&quot; Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif; font-size:30px; line-height:150%; font-weight:300; margin:0; text-align:left;">AmaZone DropShipper</h1>
        </td>
    </tr>
    <tr>
        <td ><img src="{{asset('images/AZ-email-Logo.png')}}" class="email_logo_top" alt="" width="200px"></td>
    </tr>
    <tr >
        <div style="padding:0 3rem">
            <h1>Keepa token usages alert</h1>
    
            <p>It's seems account <strong>{{ $params['keepa_api_key'] }}</strong> usages high amount of tokens.</p>
    
    
            <h3>Alert Details:</h3>
            <p><strong>Keepa API Key:</strong> {{$params['keepa_api_key']}}</p>
            <p><strong>Token Flow Reduction:</strong> {{ $params['token_flow_reduction'] }}</p>
            <p><strong>Tokens left:</strong> {{ $params['token_left'] }}</p>
        </div>
    </tr>

</table>
</body>
</html>
