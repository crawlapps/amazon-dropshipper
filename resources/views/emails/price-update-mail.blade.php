<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>AmaZone DropShipper</title>

    <style>

        p{
            margin:0 0 16px;
        }
        h4{
            margin:0 0 12px;
        }
        .title{
            color:#2d2f69;
            font-size:18px;
        }
        .email_logo_top{
            display: block;
            margin: 20px auto;
            width: 70px;
        }

    </style>
</head>

<body bgcolor="#f5f5f5" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" offset="0" style="padding:70px 0 70px 0;">
<table width="600" height="auto" align="center" cellpadding="0" cellspacing="0" style="background-color:#fdfdfd; border:1px solid #dcdcdc; border-radius:3px !important;">
    <tr>
        <td width="600" height="auto" bgcolor="#000" border="0" style="padding:36px 48px; display:block; margin: 0px auto;">
            <h1 style="color:#ffffff; font-family:&quot; Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif; font-size:30px; line-height:150%; font-weight:300; margin:0; text-align:left;">AmaZone DropShipper</h1>
        </td>
    </tr>
    <tr>
        <td ><img src="{{asset('images/AZ-email-Logo.png')}}" class="email_logo_top" alt="" width="200px"></td>
    </tr>
    <tr>
        <td width="600" bgcolor="#fdfdfd" border="0" style="color:#737373; font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif; font-size:14px; line-height:150%; text-align:left; padding:48px;padding-top: 15px;">
            <h1>Product Price Updated</h1>

            <p>Alert! Amazon has changed the price of the product <a href="{{$params['amazon_link']}}" target="_blank">{{$params['product_name']}}</a>. To keep your store in sync we have just updated the price of this product in your store. </p>

            <p>Product: <a href="{{$params['store_link']}}" target="_blank">{{$params['store_link']}}</a></p>

            <p>Keep the good work with Amazone DropShipper!.</p>

            <p>Note: When a product price change in Amazon, we will update the product price in your store keeping the same profit percentage that you set when you imported the product to your store. Example if you imported a cup which cost was 10$ and you set 20% profit when importing, then if the product in Amazon changes price to 15$ we will update the price in your store to keep the same 20% profit margin.</p>

            <a style="color:#2d2f69;" href="www.AmazoneDropShipping.com" target="_blank">www.AmazoneDropShipping.com</a>
            <p>Amazon: US, Canada, Australia, UK, Germany, France, Italy Spain, Mexico and Brazil, Walmart: US</p>

            <p>Thanks.</p>

        </td>
    </tr>

</table>
</body>
</html>
