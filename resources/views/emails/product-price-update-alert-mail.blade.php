<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>AmaZone DropShipper</title>

    <style>
        p {
            margin: 0 0 16px;
        }

        h4 {
            margin: 0 0 12px;
        }

        .title {
            color: #2d2f69;
            font-size: 18px;
        }

        .email_logo_top {
            display: block;
            margin: 20px auto;
            width: 70px;
        }
    </style>
</head>

<body bgcolor="#f5f5f5" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" offset="0"
    style="padding:70px 0 70px 0;">
    <table width="600" height="auto" align="center" cellpadding="0" cellspacing="0"
        style="background-color:#fdfdfd; border:1px solid #dcdcdc; border-radius:3px !important;">
        <tr>
            <td width="600" height="auto" bgcolor="#000" border="0"
                style="padding:36px 48px; display:block; margin: 0px auto;">
                <h1
                    style="color:#ffffff; font-family:&quot; Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif; font-size:30px; line-height:150%; font-weight:300; margin:0; text-align:left;">
                    AmaZone DropShipper</h1>
            </td>
        </tr>
        <tr>
            <td><img src="{{ asset('images/AZ-email-Logo.png') }}" class="email_logo_top" alt="" width="200px">
            </td>
        </tr>
        <tr>
            <td width="600" bgcolor="#fdfdfd" border="0"
                style="color:#737373; font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif; font-size:14px; line-height:150%; text-align:left; padding:48px;padding-top: 15px;">
                <h1>Product Price Update Needed</h1>

                <p>Alert! We received a notification that Amazon might have changed the price of one or more of your imported products:</p>

                @foreach ($params['products'] as $product)
                    <p><a href="{{ $product['link'] }}" target="_blank">{{ $product['name'] }}</a></p>
                @endforeach

                <p>To keep your store prices in sync with Amazon go to the MY PRODUCTS section in our app and update the product(s) price(s) by clicking in the ‘Update Now!” button. To make it easier for you, we have identified the products that you might need to update with the "<span style="color: red">Update Now!</span>” button in red. </p>
                <h3>Suggested steps:</h3>

                <p>a) Click in each of the Amazon links listed above.</p>
                <p>b) Go to MY PRODUCTS page in our app and double check if the price changed.</p>
                <p>c) If the price changed, in the MY PRODUCTS page click in the ‘Update Now!” button. Prices will update.</p>
                <p>d) Check product price to see they match new Amazon prices.</p>

                <h3>Be mindful that not all notifications might be 100% accurate or maybe the price just changed by some cents. We just notify you the price change updates we receive from Amazon.</h3>

                <p>Note: When you use the 'Update Now' function (button) we will update the product price in your store keeping the same profit percentage that you set when you imported the product to your store. Example if you imported a cup which cost was 10$ and you set 20% profit when importing it means your store price is set to 12$. Then if the product in Amazon change its price to 15$, and you use the 'Update Now' function, we will update the price in your store to keep the same 20% profit margin, so your new store price will be 18$.</p>

                <p>Keep the good work with Amazone DropShipper!.</p>

                <a style="color:#2d2f69;" href="www.AmazoneDropShipping.com" target="_blank">www.AmazoneDropShipping.com</a>
                <p>Amazon: US, Canada, Australia, UK, Germany, France, Italy Spain, Mexico and Brazil, Walmart: US</p>
            </td>
        </tr>

    </table>
</body>

</html>
