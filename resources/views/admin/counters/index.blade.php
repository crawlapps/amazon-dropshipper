@extends('admin.layouts.backend')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Counters</h2>
                </div>

                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('admin.dashboard') }}"> Back</a>
                </div>
            </div>
        </div>
        @include('admin.layouts.partials.errors')

        {{-- <div class="filters" style="margin: 30px 0;display: flex;">
                <div style="margin-right: 24px;">
                    <p>From: </p>
                    <div>
                        <input type="text" id="datepicker" value="" />
                        <input type="hidden" id="date" value="" />
                    </div>
                </div>
                <div style="margin-right: 24px;">
                    <p>To: </p>
                    <div>
                        <input type="text" id="datepicker1" value="" />
                        <input type="hidden" id="date1" value="" />
                    </div>
                </div>

                <p>Display:  
                    <select name="pages" id="pages" onchange="window.location.href='/admin/counters/' + this.options[this.selectedIndex].value;">
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                    <option value="250">250</option>
                    <option value="500">500</option>
                    </select>
                </p>

                <div>
                    <button class="btn btn-primary search-filter">Go</button>
                </div>
        </div>     --}}
        
        <table class="table table-striped table-bordered" style="width:100%" id="counters-data">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>User ID</th>
                    <th>Get Products Count</th>
                    <th>RF Calls Total</th>
                    <th>Plan Name</th>
                    <th>Pushed Products Count</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($counters as $key=>$value)
                    <tr>
                        <td>{{ $value->date }}</td>
                        <td>{{ $value->user_id }}</td>
                        <td>{{ $value->total_get_products }}</td>
                        <td>{{ $value->total_rf_calls }}</td>
                        <td>{{ $value->plan_name }}</td>
                        <td>{{ $value->total_added_products }}</td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>Date</th>
                    <th>User ID</th>
                    <th>Get Products Count</th>
                    <th>RF Calls Total</th>
                    <th>Plan Name</th>
                    <th>Pushed Products Count</th>
                </tr>
            </tfoot>
        </table>

        {{-- <ul class="pagination">
            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">Next</a></li>
        </ul> --}}
    
    </div>

      <!-- ✅ Load CSS file for jQuery ui  -->
      <link
      href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css"
      rel="stylesheet"
    />

    <!-- ✅ load jQuery ✅ -->
    <script
      src="https://code.jquery.com/jquery-3.6.0.min.js"
      integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
      crossorigin="anonymous"
    ></script>

    <!-- ✅ load jquery UI ✅ -->
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"
      integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA=="
      crossorigin="anonymous"
      referrerpolicy="no-referrer"
    ></script>
    <script type="text/javascript">
        $(function () {
            var $datepicker = $('#datepicker');
            $datepicker.datepicker();
            $datepicker.datepicker('setDate', new Date());

            var $datepicker = $('#datepicker1');
            $datepicker.datepicker();
            $datepicker.datepicker('setDate', new Date());

            $("#datepicker").on("change",function(){
                var selected = $(this).val();
                $('#date').val(selected);
            });

            $("#datepicker1").on("change",function(){
                var selected = $(this).val();
                $('#date1').val(selected);
            });

            $(document).ready(function() {
                $('.search-filter').click(function() {
                    let d1 = $('#date').val();
                    let d2 = $('#date1').val();
                    let counter = $('#pages').find(":selected").val();

                    let url = "{{url('/admin/counters/',"__")}}";
                    url = url.replace("__", counter);
                    url = url + '?start_date=' + d1 + '&end_date=' + d2;
                    $.ajax({
                        url: url,
                        type: "get",
                        data: {'_method': 'get', '_token': "{{ csrf_token() }}"},
                        success: function (data) {
                            console.log(data);
                            // window.location.reload();
                        },
                    });
                });
            }); 
        });
    </script>
@endsection


