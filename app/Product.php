<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{

    use SoftDeletes;


    protected $fillable = ['collection_id',
        'user_id',
        'shopify_id',
        'shopify_handle',
        'type',
        'locale',
        'source',
        'source_url',
        'description',
        'amazon_associate_link',
        'show_prices',
        'affiliate',
        'saller_ranks',
        'auto_update',
        'is_tracked',
        'updateInterval',
        'reviews',
        'history',
        'categoryes_tree',
        'status',
        'imported_product_plan',
        'keepa_updated_at',
        'is_get_price_update_notification',
        'is_price_updated_manually'
       ];


    protected $casts = [

        'collection_id' => 'array',

    ];

    protected $dates = ['deleted_at'];

}
