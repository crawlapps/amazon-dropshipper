<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanContents extends Model
{
    protected $table = 'plan_contents';

    protected $fillable = ['db_plans_id','contents'];

    protected $casts = [
        'contents' => 'array',
    ];
}
