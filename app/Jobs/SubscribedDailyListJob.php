<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Exception;
use App\User;
use DB;
use App\MainDailyListService;
class SubscribedDailyListJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            logger('=============== START::JOB :: SubscribedDailyListJob =============');


            $subscribed_plans = ['Suite','Startup','Pro', 'Super'];

            $subscribed_plan_ids = DB::table('plans')->select('id')->whereNotNull('interval')->whereIn('name',$subscribed_plans)->pluck('id');

            logger("subscribed_plan_ids => ".$subscribed_plan_ids);

            if(count($subscribed_plan_ids) > 0){

            $records = DB::table('users')
            ->join('products','users.id','=','products.user_id')
            ->join('variants as variant', 'products.id', '=', 'variant.product_id')
            ->select('products.id as product_id','users.id as user_id','products.source_url','variant.source_price_amazon','users.created_at')
            ->where('products.source', '=',"Amazon")
            ->where('variant.is_main', '=',"1")
            ->whereIn('users.plan_id',$subscribed_plan_ids)
            // ->whereIn('products.imported_product_plan',$subscribed_plans)
            ->whereNotNull('variant.shopify_id')
            ->whereNull('products.deleted_at')
            ->orderBy('products.created_at', 'desc')
            ->get()->toArray();

             logger("records");
             logger(json_encode($records));

             if(count($records) > 0){
                foreach($records as $key=>$value){

                  $existProductRecord = MainDailyListService::where('shopify_product_id', $value->product_id)->where('user_id',$value->user_id)->get();

                  logger("existProductRecord");
                  logger(json_encode($existProductRecord));

                  if(!count($existProductRecord) > 0){
                   $source_url = $value->source_url;
                   $product_url = strtok($source_url, '?');

                   $mainList = new MainDailyListService;
                   $mainList->user_id = $value->user_id;
                   $mainList->shopify_product_id = $value->product_id;
                   $mainList->product_url = $product_url;
                   $mainList->source_price_amazon = $value->source_price_amazon;
                   $mainList->list_type = "Subscribed";
                   $mainList->save();

                }
              }
            }
         }

            logger('=============== END::JOB :: SubscribedDailyListJob =============');
        }
        catch(Exception $e){
            logger('=============== ERROR::JOB :: SubscribedDailyListJob =============');
            logger(json_encode($e->getMessage()));
            logger('=============== ERROR::JOB :: END =============');

        }
    }
}
