<?php

namespace App\Jobs;

use App\Mail\SendKeepTokenStatusMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Osiset\ShopifyApp\Storage\Models\Plan as ModelsPlan;
use Exception;
use Keepa\KeepaAPI;
use Keepa\API\Request as KeepaAPIRequest;
use DB;
use App\Product;
use App\Variant;
use App\User;
use App\MainDailyListService;
use Illuminate\Support\Facades\Mail;

class KeepaTrackingAddProductsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $user_id = '';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            logger('=============== START::JOB :: KeepaTracking AddProducts  Job =============');
            $user_id = $this->user_id;
            logger("user_id => ".$user_id);

            $subscribed_plans = ['Suite','Startup','Pro', 'Super'];

            $user = User::find($user_id);
            $plan = ModelsPlan::find($user->plan_id);

            $products = Product::select('source_url','locale','id')
            ->where('locale','<>',"")
            ->where('is_tracked','<>',1)
            ->whereIn('imported_product_plan', $subscribed_plans)
            ->orderBy('id', 'desc')
            ->where('user_id',$user_id)
            ->limit($plan->max_regular_product_import)
            ->where('source',"Amazon")->get();

            logger(json_encode($products));

            logger('======= get tokenstatus before aPI call =======');

            $tokenRes = $this->getKeepaTokenStatus();

            $previewsTokensLeft = $tokenRes->tokensLeft;

            logger("Total Product  => " .count($products));

             $response_info = [];

             if(count($products) > 0){

               foreach($products as $key=>$value){

                        logger("value => ".$value->locale );

                        $tokenRescon = $this->getKeepaTokenStatus();

                        $previewsTokensLeftcon = $tokenRescon->tokensLeft;

                        if($previewsTokensLeftcon > 0 ) {

                        $locale = $value->locale;
                        $domainID = constant("Keepa\objects\AmazonLocale::$locale");
                        if($domainID) {
                        logger("domainID => ".$domainID );

                        $source_url =  $value->source_url;
                        preg_match('/\/([a-zA-Z0-9]{10})/',$source_url,$parsed);

                        if(count($parsed) > 0){
                        $asin = strtoupper($parsed[1]);
                        logger("asin => ".$asin );

                        if($asin) {

                          $mainDomainId = constant("Keepa\objects\AmazonLocale::$locale");

                          if($mainDomainId){
                          $updateInterval = 12;

                          $accessKey = config('const.autoupdate_keepa_api_key');

                        //   Tracking value in range
                          $thresholdValues = [
                              [
                                  "thresholdValue" => 999999999,
                                  "domain" => $mainDomainId,
                                  "csvType" => 0,
                                  "isDrop" => true
                              ],
                              // track any price increases
                              [
                                  "thresholdValue" => 0,
                                  "domain" => $mainDomainId,
                                  "csvType" => 0,
                                  "isDrop" => false
                              ],
                              [
                                "thresholdValue" => 999999999,
                                "domain" => $mainDomainId,
                                "csvType" => 1,
                                "isDrop" => true
                            ],
                            // track any price increases
                            [
                                "thresholdValue" => 0,
                                "domain" => $mainDomainId,
                                "csvType" => 1,
                                "isDrop" => false
                            ],
                          ];

                          $notifyIf = [
                              [
                                  "domain" => $mainDomainId,
                                  "csvType" => 0,
                                  "notifyIfType" => 1            //   0: OUT_OF_STOCK, 1: BACK_IN_STOCK
                              ],
                          ];

                          $metaData = [
                              "product_id" => $value->id,
                              "source_url" => urldecode($source_url)
                          ];

                          $params = [
                              "asin" => $asin,
                              "mainDomainId" => $mainDomainId,
                              "ttl" => 0,
                              "expireNotify" => true,
                              "desiredPricesInMainCurrency" => false,
                              "updateInterval" => $updateInterval,
                              "metaData" => $value->id,
                              "thresholdValues" => $thresholdValues,
                              "notifyIf" => $notifyIf,
                              "notificationType" => [ true, true, true, true, true, true, true ],
                              "individualNotificationInterval" => 5
                          ];

                          $apiUrl = "https://api.keepa.com/tracking/?key=".$accessKey."&"."type=add";

                          $query_param = json_encode($params, true);

                          logger("params");
                          logger("-----------------------------------------------");
                          logger($params);
                          logger("------------------------------------------------");

                          $curl = curl_init();

                          curl_setopt_array($curl, array(
                              CURLOPT_URL => $apiUrl,
                              CURLOPT_RETURNTRANSFER => true,
                              CURLOPT_ENCODING => '',
                              CURLOPT_MAXREDIRS => 10,
                              CURLOPT_TIMEOUT => 0,
                              CURLOPT_FOLLOWLOCATION => true,
                              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                              CURLOPT_POSTFIELDS => $query_param,
                              CURLOPT_CUSTOMREQUEST => 'POST',
                              CURLOPT_HTTPHEADER => array(
                                  "Content-Type: application/json",
                              ),
                          ));

                          $response = curl_exec($curl);

                          logger("------------------------------------------------");
                          logger("Resposne ");
                          logger(json_encode($response));
                          logger("------------------------------------------------");

                          $response = json_decode($response);

                          $response_info[] = $response;

                          $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                          curl_close($curl);

                          $variant = Variant::select('source_price_amazon')->where('product_id', $value->id)->where('is_main', 1)->first();
                          $mainList = new MainDailyListService;
                          $mainList->user_id = $user->id;
                          $mainList->shopify_product_id = $value->id;
                          $mainList->product_url = $value->source_url;
                          $mainList->source_price_amazon = ($variant) ? $variant->source_price_amazon : 0;
                          $mainList->list_type = "Subscribed";
                          $mainList->save();

                          DB::table('products')
                          ->where('id', $value->id)
                          ->update(['is_tracked' => 1, 'updateInterval' => $updateInterval ]);

                         $currentTokensLeft = $response->tokensLeft;

                         logger("currentTokensLeft => ". $currentTokensLeft);

                         if($currentTokensLeft <=0) {
                            logger("Delay the script execution for 1 seconds ");
                            logger("Get assets, Waiting 1 seconds for a credit");
                            sleep(1);
                            $totalTokensLeft = $currentTokensLeft;
                            do{
                             sleep(1);
                             $tokenResconcheck = $this->getKeepaTokenStatus();
                             $totalTokensLeft = $tokenResconcheck->tokensLeft;

                            }while($totalTokensLeft <= 0 );

                         }
                       }
                     }
                   }
                  }
                }
              }

              logger('======= get tokenstatus after aPI call =======');
               $this->getKeepaTokenStatus();

             }

             return true;
            logger('=============== END::JOB :: KeepaTracking AddProducts Script Job =============');
           }catch(Exception $e){
            logger('=============== ERROR::JOB :: KeepaTracking AddProducts Script Job =============');
            logger(json_encode($e->getMessage()));
            logger('=============== ERROR::JOB :: END =============');

        }
    }


    public function getKeepaTokenStatus(){
        try{
            $apiKey = config('const.autoupdate_keepa_api_key');
            $api = new KeepaAPI($apiKey);
            $r = KeepaAPIRequest::getTokenStatusRequest();
            $response = $api->sendRequestWithRetry($r);

            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => $response->url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);

            logger("**************************");
            logger("Token === response");
            logger("**************************");
            logger($response);
            logger("**************************");

            curl_close($curl);

            $res = json_decode($response);

            // Trigger mail to the admin if token flow reduction is greater
            if($res->tokenFlowReduction >= 18) {
                sendFlowReductionMail($apiKey, $res);
            }

            return $res;

        }catch(\Exception $e){
            logger('=============== ERROR:: getKeepaTokenStatus ===============');
            logger($e);
        }
    }

}
