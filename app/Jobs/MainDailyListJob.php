<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Exception;
use App\Jobs\AmazonUrlsAPIDeletesAllJob;
use App\MainDailyListService;
use App\Models\AmazonUrlsAPIAdds;

class MainDailyListJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            logger('=============== START::JOB :: MainDailyListJob =============');
          

            logger("amazon_urls_api_key");
            $amazon_urls_api_key = config('const.amazon_urls_api_key');
            logger($amazon_urls_api_key);
            
           $params = [
                "data" => [
                    [
                        "confirm" => 1
                    ]
                ]
            ];

            $apiUrl = "http://50.16.141.15/amazon-api/delete/AllUrls";

            $query_param = json_encode($params, true);

            logger("query Param cred");
            logger($query_param);

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $apiUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_POSTFIELDS => $query_param,
                CURLOPT_CUSTOMREQUEST => 'DELETE',
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                    "api_key:".base64_encode($amazon_urls_api_key)
                ),
            ));

            $response = curl_exec($curl);

            if($response){

            logger("res");
            $response =  json_decode($response);

            $status = $response->response->status;

            if($status==200){   

       MainDailyListService::select("*")->chunk(500, function ($mainDailyList) {
                           
            logger("mainDailyList");

            logger(json_encode($mainDailyList));
            
            $product_urls = [];          

            if(count($mainDailyList) > 0){
                
                foreach($mainDailyList as $key=>$value){

                    $new_params = [];
                    $new_params['product_url'] = $value->product_url;                  
                    $new_params['old_price'] = $value->source_price_amazon;
                    $new_params['shopify_product_id'] = $value->shopify_product_id;

                    array_push($product_urls,$new_params);

                }
            }

            logger("amazon_urls_api_key");
            $amazon_urls_api_key = config('const.amazon_urls_api_key');
            logger($amazon_urls_api_key);

            $callback_url = url('/').'/api/webhook/amazon/get/urls';

            logger("----callback_url---".$callback_url);


            if(count($product_urls) > 0) {
                $params = [
                    "data" => $product_urls,
                    "callback_url" => $callback_url
                ];

            $apiUrl = "http://50.16.141.15/amazon-api/add/Urls";

            $query_param = json_encode($params, true);

            logger("query Param cred");
            logger($query_param);

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $apiUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_POSTFIELDS => $query_param,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                    "api_key:".base64_encode($amazon_urls_api_key)
                ),
            ));

            $response = curl_exec($curl);

            logger("===Response===");
            logger($response);

            if($response){

                logger("res");

                $response =  json_decode($response);

                $status = $response->response->status;

                if($status==200){
                    $result = $response->response->result;

                    array_pop($result);

                    if(count($result) > 0){
                         foreach($result as $key=>$value){

                            logger('Resssssssssss');
                            logger(json_encode($value));
                             $error  = isset($value->error) ? $value->error : "";                            
                             $shopify_product_id = isset($value->shopify_product_id) ? $value->shopify_product_id : 0;
                             $product_id = isset($value->product_id) ? $value->product_id : 0;

                             logger("shopify_product_id => ".$shopify_product_id);                                                         

                             MainDailyListService::where('shopify_product_id','=',$shopify_product_id)                                     
                                       ->update([
                                           "product_id" => $product_id                                          
                                       ]);
                                                                
                         }
                    }
                }
            }

            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            logger('HTTP code: '.$httpcode);

            curl_close($curl);

            }
            
          });

        }
    
      }

    

            logger('=============== END::JOB :: MainDailyListJob =============');
        }
        catch(Exception $e){
            logger('=============== ERROR::JOB :: MainDailyListJob =============');
            logger(json_encode($e->getMessage()));
            logger('=============== ERROR::JOB :: END =============');

        }
   
   
   
   
   
    }
}
