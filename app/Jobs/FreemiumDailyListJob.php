<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Exception;
use App\User;
use DB;
use App\MainDailyListService;
class FreemiumDailyListJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            logger('=============== START::JOB :: FreemiumDailyListJob =============');

            $CURRENT_DATE =  \Carbon\Carbon::now()->subDays(31);

            logger("CURRENT_DATE =>".$CURRENT_DATE);

            $freemium_plan = DB::table('plans')->where('type','=',"ONETIME")->value('id');

            logger("freemium_plan => ".$freemium_plan);

            if($freemium_plan){

            $records = DB::table('users')
            ->join('products','users.id','=','products.user_id')
            ->join('variants as variant', 'products.id', '=', 'variant.product_id')
            ->select('products.id as product_id','users.id as user_id','products.source_url','variant.source_price_amazon','users.created_at')
            ->where('products.source', '=',"Amazon")
            ->where('variant.is_main', '=',"1")
            ->where('users.plan_id', '=',$freemium_plan)
            ->whereNotNull('variant.shopify_id')
            ->whereNull('products.deleted_at')
            ->whereDate('users.created_at', '>',$CURRENT_DATE)
            ->orderBy('products.created_at', 'desc')
            ->get()->toArray();

             logger("records");
             logger(json_encode($records));


             if(count($records) > 0){
                foreach($records as $key=>$value){

              $existProductRecord = MainDailyListService::where('shopify_product_id', $value->product_id)->where('user_id',$value->user_id)->get();

              logger("existProductRecord");
              logger(json_encode($existProductRecord));

              if(!count($existProductRecord) > 0){

                    $source_url = $value->source_url;
                    $product_url = strtok($source_url, '?');

                   $mainList = new MainDailyListService;
                   $mainList->user_id = $value->user_id;
                   $mainList->shopify_product_id = $value->product_id;
                   $mainList->product_url = $product_url;
                   $mainList->source_price_amazon = $value->source_price_amazon;
                   $mainList->list_type = "Freemium";
                   $mainList->save();

                  }
               }
            }
         }

            logger('=============== END::JOB :: FreemiumDailyListJob =============');
        }
        catch(Exception $e){
            logger('=============== ERROR::JOB :: FreemiumDailyListJob =============');
            logger(json_encode($e->getMessage()));
            logger('=============== ERROR::JOB :: END =============');

        }
    }
}
