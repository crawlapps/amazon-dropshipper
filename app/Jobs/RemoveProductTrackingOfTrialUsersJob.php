<?php

namespace App\Jobs;

use App\Models\Counters;
use App\Product;
use App\Traits\KeepaCustomAPITrait;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RemoveProductTrackingOfTrialUsersJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use KeepaCustomAPITrait;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $users = User::where('plan_id', 1)->whereNull('deleted_at')->select(['id', 'name', 'email', 'plan_id'])->get();

            if(count($users) > 0) {
                $product_count = 0;
                $CURRENT_DATE = Carbon::now();
                logger($CURRENT_DATE);
                foreach($users as $key => $user) {
                    if ($product_count > 500) {
                        break;
                    }

                    logger("=======> User");
                    logger($user);
                    $counterData = Counters::where('user_id', $user->id)->where('status', 'active')->orderBy('id', 'desc')->first();
                    logger("======= Counter Data ======");
                    logger($counterData);

                    if(isset($counterData)) {
                        $end_date = $counterData->end_date;
                        logger('====> End Date');
                        logger($end_date);

                        if ($CURRENT_DATE > $end_date) {
                            logger('Plan Expired');

                            $products = Product::select('source_url','locale','id')
                            ->where('locale','<>',"")
                            ->where('is_tracked',1)
                            ->orderBy('id', 'desc')
                            ->where('user_id',$user->id)
                            ->where('source',"Amazon")->get();


                            // $product_count += count($products);
                            if (count($products) > 0) {
                                foreach($products as $key=>$product) {
                                    if ($product_count > 500) {
                                        break;
                                    }

                                    $param = [
                                        'product_id' => $product->id,
                                    ];

                                    $this->keepaRemoveProductTracking($user->id, $param);
                                    $product_count++;
                                }
                            }
                        } else {
                            logger("Plan not expired");
                        }
                    }
                }

                logger("=========== Done product count ==========");
                logger($product_count);
                logger('==========================================');
            }
        }
        catch (Exception $e) {
            logger("=============ERROR :: Job :: RemoveProductTrackingOfTrialUsersJob===================");
            logger($e);
            return true;
        }
    }
}
