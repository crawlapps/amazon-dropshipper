<?php

namespace App\Jobs;

use App\MainDailyListService;
use App\Models\ShopifyShop;
use App\Traits\KeepaCustomAPITrait;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class CheckStoreStatusJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use KeepaCustomAPITrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            logger("========================================================");
            logger("START :: JOB :: Check store status");
            logger("========================================================");

            $users = User::whereNull('deleted_at')->whereIn('store_status', ["OPEN", "REOPEN", null])->whereIn('plan_id', [16,7, 15, 9, 11, 13, 12, 10, 8])->get();

            logger("=======> User count:: ". count($users));

            $closedStore = [];
            $diffrentError = [];
            $trackedProducts = [];
            $trackedProductCount = 0;

            if($users) {
                foreach ($users as $user) {
                    if($user) {
                        logger("========> User");
                        logger(json_encode($user));
                        $shop = $user->api()->rest('GET', 'admin/shop.json');

                        logger('========> Shop');
                        logger(json_encode($shop));

                        // if($user->name == 'crawlapps-amazon-dropshipping1.myshopify.com') {
                        //     $this->updateStoreData($user);
                        //     $user->update(['store_status' => "CLOSED"]);
                        // }
                        if($shop['errors']) {
                            if($shop['body'] == "Unavailable Shop" || $shop['body'] == 'Not Found') {
                                logger("==========> Start:: Trigger shop closing functionality.");
                                array_push($closedStore, $user->id);
                                $mainlist = MainDailyListService::where('user_id', $user->id)->count();
                                array_push($trackedProducts, $mainlist);
                                $trackedProductCount += $mainlist;

                                $this->updateStoreData($user);
                                $user->update(['store_status' => "CLOSED"]);
                            } else {
                                logger("Diffrent Error ======>");
                                array_push($diffrentError, $user->id);
                            }
                        }
                        else {
                            logger("Store is open not closed");
                        }
                    } else {
                        logger("=======> User not found");
                    }
                }
            }

            logger("=============> Closed Store ::" . json_encode($closedStore));
            logger("=============> Diffrent Error:: " . json_encode($diffrentError));
            logger("=============> Tracked Products:: " . json_encode($trackedProducts));
            logger("=============> Total Tracked Product count:: " . $trackedProductCount);


            logger("========================================================");
            logger("END :: JOB :: Check store status");
            logger("========================================================");
        }catch(Exception $e){
            logger("========================================================");
            logger('========== ERROR:: JOB :: Check store status');
            logger("========================================================");
            logger(json_encode($e->getMessage()));

            logger("========================================================");
        }
    }


    public function updateStoreData($user) {
        logger("=========== Start :: Update Store Data ===============");

        MainDailyListService::where('user_id', $user->id)->delete();

        $this->keepaRemoveAllProductTracking($user->id);

        $user->password = '';
        $user->plan_id = null;
        $user->deleted_at = Carbon::now();
        $user->save();

        logger("=========== End :: Update Store Data");
    }
}
