<?php

namespace App\Jobs;

use App\EmailLog;
use App\Mail\ProductPriceUpdateAlertMail;
use App\Models\ShopifyShop;
use App\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendProductPriceUpdateAlertMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {
            logger('=============== START::Job :: Send Product Price Update ALert Mail =============');

            $excludeUpdateNowPlanList = json_decode(env('EXCLUDE_UPDATE_NOW_PLAN_LIST', [1]));
            $users = User::whereIn('plan_id', $excludeUpdateNowPlanList)->whereNull('deleted_at')->get('id');
            // logger('==============> users');
            // logger($users);

            foreach ($users as $key => $user) {
                $products = Product::where('user_id', $user->id)->where('is_tracked', 1)->where('is_get_price_update_notification', 1)->select('id', 'shopify_handle as name', 'source_url as link')->get();
                $productsIds = $products->pluck('id')->toArray();
                $products = $products->toArray();
                // logger('============> Products');
                // logger($products);

                if (count($products) > 0) {
                    $userEmail = ShopifyShop::where('user_id', $user->id)->select('main_email', 'email')->first();
                    $email = $userEmail->email;

                    if (isset($userEmail->main_email) && !empty($userEmail->main_email)) {
                        $email = $userEmail->main_email;
                    }
                    // logger('==============> To Email');
                    // logger($email);

                    $params['products'] = $products;

                    Mail::to($email)->send(new ProductPriceUpdateAlertMail($params));
                    //Email Log
                    EmailLog::create(["trigger" => "PRODUCT_PRICE_UPDATE_ALERT", 'user_id'=> $user->id, 'productsId'=> implode(',', $productsIds)]);
                    Product::where('user_id', $user->id)->where('is_tracked', 1)->where('is_get_price_update_notification', 1)->update(['is_get_price_update_notification'=> 0, 'updated_at' => Carbon::now()]);
                }
            }

            logger('=============== END::Job :: Send Product Price Update ALert Mail =============');
        } catch (Exception $e) {
            logger('=============== ERROR::Job :: Send Product Price Update ALert Mail =============');
            logger(json_encode($e->getMessage()));
            logger('=============== ERROR::Job :: END =============');
        }
    }
}
