<?php

namespace App\Jobs;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use DB;
use App\Models\AmazonUrlsAPIAdds;
use App\Traits\AmazonUrlsAPITrait;

class AmazonUrlsAPIGetsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use AmazonUrlsAPITrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            logger("========================================================");
            logger("START :: Amazon Urls API :: JOB :: GET URLs             ");
            logger("========================================================");

            $page = 1;
            $limit = 100;

            $this->SaveUrlsData($page,$limit);

            logger("========================================================");
            logger("END :: Amazon Urls API :: JOB :: GET URLs               ");
            logger("========================================================");
        }catch(Exception $e){
            logger("========================================================");
            logger('========== ERROR:: Amazon Urls API :: JOB :: GET URLs    ');
            logger("========================================================");
            logger(json_encode($e->getMessage()));

            logger("========================================================");
        }
    }

    public function SaveUrlsData($page,$limit){

        $params = [
            "page" => $page,
            "limit" => $limit
        ];

        $query_param = json_encode($params, true);

        //logger("query Param cred");
       // logger($query_param);

        $apiUrl = "http://50.16.141.15/amazon-api/get/Urls";

      //  logger("amazon_urls_api_key");
        $amazon_urls_api_key = config('const.amazon_urls_api_key');
      //  logger($amazon_urls_api_key);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $apiUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_POSTFIELDS => $query_param,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "api_key:".base64_encode($amazon_urls_api_key)
            ),
        ));

        $response = curl_exec($curl);

       logger("=== GET url Response===");
       logger($response);

           if($response){
           // logger("res");
            $response =  json_decode($response);

            $status = $response->response->status;
            if($status==200){
                if(isset($response->response->result)) {
                    $result = $response->response->result;
                    if (isset($result->data)) {
                        $data = $result->data;

                       // logger("data");

                     //   logger(json_encode($data));

                        if (count($data) > 0) {
                            foreach ($data as $key => $value) {
                                $price = isset($value->price) ? $value->price : "";
                                $value_field = isset($value->value) ? $value->value : 0;
                                $product_id = isset($value->product_id) ? $value->product_id : 0;

                                $product = AmazonUrlsAPIAdds::where("product_id",$product_id)->first();

                                if($product){

                                    AmazonUrlsAPIAdds::where("product_id", $product_id)->update([
                                        "value" => $value_field,
                                        "price" => $price,
                                        "response" => json_encode($value)
                                    ]);

                                  $this->updatePrice($value);

                                }
                            }
                        }

                        $pagination = $result->pagination;
                        $current_page = $pagination->page;
                        $limit = $pagination->limit;
                        $total_records = $pagination->total_records;

                      //  logger("total_records ". $total_records);
                        $current_page++;
                       // logger("current_page ". $current_page);
                        $total_pages = ceil($total_records/100);
                      //  logger("total_page ". $total_pages);

                        if($current_page <= $total_pages) {
                            $this->SaveUrlsData($current_page, $limit);
                        }
                    }
                }
            }
        }

        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        //logger('HTTP code: '.$httpcode);

        curl_close($curl);

    }



}
