<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Traits\KeepaCustomAPITrait;


class RemoveTrackedProductJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use KeepaCustomAPITrait;

    private $userId; 
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            logger('================= START:: Remove Tracked product from Keepa =================');
            $this->keepaRemoveAllProductTracking($userId);
            logger('================= END:: Remove Tracked product from Keepa =================');
        } catch (\Exception $e) {
            logger('================= ERROR:: Remove Tracked product from Keepa =================');
            logger($e->getMessage());
            return true;
        }
        return true;
    }
}
