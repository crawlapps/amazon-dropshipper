<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Exception;
use DB;
use App\Models\AmazonUrlsAPIAdds;
class AmazonUrlsAPIAddsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */



    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            logger("========================================================");
            logger("START :: Amazon Urls API :: JOB :: Add URLs             ");
            logger("========================================================");


            logger("amazon_urls_api_key");
            $amazon_urls_api_key = config('const.amazon_urls_api_key');
            logger($amazon_urls_api_key);

           // $stat_date = \Carbon\Carbon::now()->subMonths(4);

            $existProductRecord = AmazonUrlsAPIAdds::select('variant_db_id')->pluck('variant_db_id')->toArray();

            $productsUrls = DB::table('products')
                ->join('variants as variant', 'products.id', '=', 'variant.product_id')
                ->select('products.id as shopify_product_id','products.source_url','variant.title','variant.source_price','variant.shopify_price','variant.source_price_amazon','variant.id as variant_db_id', 'products.created_at')
                ->where('products.source', '=',"Amazon")
                ->where('variant.is_main', '=',"1")
                ->whereNotIn('variant.id',$existProductRecord)
                ->whereNotNull('variant.shopify_id')
                ->orderBy('products.created_at', 'desc')
                ->limit(500)
                ->get()->toArray();

           // logger("total-products -> ".count($productsUrls));

            logger("============================================================");
           // logger(json_encode($productsUrls));
            logger("============================================================");

            $product_urls = [];
            $product_details = [];
            if(count($productsUrls) > 0){
                foreach($productsUrls as $key=>$value){

                    $new_params = [];
                    $product_detail_params = [];

                    $source_url = $value->source_url;
                    $product_url = strtok($source_url, '?');

                    $new_params['product_url'] = $product_url;
                    $new_params['product_name'] = $value->title;
                    $new_params['old_price'] = $value->source_price_amazon;
                    $new_params['shopify_product_id'] = $value->shopify_product_id;

                    $product_detail_params['variant_db_id'] = $value->variant_db_id;
                    $product_detail_params['old_price'] = $value->shopify_price;
                    $product_detail_params['amazon_old_price'] = $value->source_price_amazon;
                   
                    array_push($product_urls,$new_params);
                    array_push($product_details,$product_detail_params);

                }
            }

           // logger("product_urls");
           // logger(json_encode($product_urls));


            $callback_url = url('/').'/api/webhook/amazon/get/urls';


            if(count($product_urls) > 0) {
                $params = [
                    "data" => $product_urls,
                    "callback_url" => $callback_url
                ];

            $apiUrl = "http://50.16.141.15/amazon-api/add/Urls";

            $query_param = json_encode($params, true);

            logger("query Param cred");
            // logger($query_param);

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $apiUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_POSTFIELDS => $query_param,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                    "api_key:".base64_encode($amazon_urls_api_key)
                ),
            ));

            $response = curl_exec($curl);

            logger("===Response===");
            logger($response);

            if($response){
                logger("res");
                $response =  json_decode($response);

                $status = $response->response->status;

                if($status==200){
                    $result = $response->response->result;

                    array_pop($result);
                    if(count($result) > 0){
                         foreach($result as $key=>$value){

                            logger('Resssssssssss');
                            logger(json_encode($value));
                             $error  = isset($value->error) ? $value->error : "";
                             $product_url =  isset($value->product_url) ? $value->product_url : "";
                             $record_no = isset($value->record_no) ? $value->record_no : 0;
                             $product_id = isset($value->product_id) ? $value->product_id : 0;
                             $shopify_product_id = isset($value->shopify_product_id) ? $value->shopify_product_id : 0;

                             logger("shopify_product_id => ".$shopify_product_id);
                             
                             $variantInfo = $this->getVariantInfo($product_details,$record_no);

                             logger("variantInfo");
                             logger($variantInfo);

                             logger("variant_db_id");
                             logger($variantInfo['variant_db_id']);

                             $existRecord = AmazonUrlsAPIAdds::where('record_no',$record_no)->where('variant_db_id',$variantInfo['variant_db_id'])->get();

                             if(count($existRecord) <=0) {

                                 AmazonUrlsAPIAdds::create([
                                     "error" => $error,
                                     "product_url" => $product_url,
                                     "record_no" => $record_no,
                                     "product_id" => $product_id,
                                     "old_price" => $variantInfo['old_price'],
                                     "amazon_old_price" => $variantInfo['amazon_old_price'],
                                     "shopify_product_id" => $shopify_product_id,
                                     "variant_db_id" => $variantInfo['variant_db_id'],                                     
                                 ]);

                             }else{

                                 AmazonUrlsAPIAdds::where('record_no',$record_no)
                                       ->where('variant_db_id',$variantInfo['variant_db_id'])
                                       ->update([
                                           "error" => $error,
                                       ]);

                             }
                         }
                    }
                }
            }

            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            logger('HTTP code: '.$httpcode);

            curl_close($curl);

            }

            logger("========================================================");
            logger("END :: Amazon Urls API :: JOB :: Add URLs               ");
            logger("========================================================");
        }catch(Exception $e){
            logger("========================================================");
            logger('========== ERROR:: Amazon Urls API :: JOB :: Add URLs    ');
            logger("========================================================");
            logger($e);

            logger("========================================================");
        }
    }

    public function getVariantInfo($product_details,$record_no){
            $variant = [];

            $index = (int)$record_no - (int)1;

            logger("index=> ".$index);

            if(count($product_details) >= $index ){

                $data = $product_details[$index];

                logger("data");

                logger(json_encode($data));

                $variant = $data;

                return $variant;

                // logger("old-price ->" .$old_price);
            }

            return $variant;
    }
}
