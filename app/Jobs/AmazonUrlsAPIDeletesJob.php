<?php

namespace App\Jobs;

use App\Models\AmazonUrlsAPIAdds;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Exception;
use DB;
class AmazonUrlsAPIDeletesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            logger("========================================================");
            logger("START :: Amazon Urls API :: JOB :: Delete URLs             ");
            logger("========================================================");


            logger("amazon_urls_api_key");
            $amazon_urls_api_key = config('const.amazon_urls_api_key');
            logger($amazon_urls_api_key);

            $productsUrls = DB::table('amazon_urls_api_adds')->select('product_id')->limit(500)->get()->toArray();

            $product_urls = [];
            if(count($productsUrls) > 0){
                foreach($productsUrls as $key=>$value){
                    $product_id = $value->product_id;
                    array_push($product_urls,["product_id" => $product_id]);
                }
            }

           // logger("product_urls");
           // logger(json_encode($product_urls));


            if(count($product_urls) > 0) {
                $params = [
                    "data" => $product_urls
                ];

                $apiUrl = "http://50.16.141.15/amazon-api/delete/Urls";

                $query_param = json_encode($params, true);

                logger("query Param cred");
               // logger($query_param);

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $apiUrl,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_POSTFIELDS => $query_param,
                    CURLOPT_CUSTOMREQUEST => 'DELETE',
                    CURLOPT_HTTPHEADER => array(
                        "Content-Type: application/json",
                        "api_key:".base64_encode($amazon_urls_api_key)
                    ),
                ));

                $response = curl_exec($curl);

                logger("===Response===");
                logger($response);

                if($response){
                    logger("res");
                    $response =  json_decode($response);

                    $status = $response->response->status;
                    if($status==200){
                        $productsID = array_column($product_urls, 'product_id');
                        AmazonUrlsAPIAdds::whereIn('product_id',$productsID)->forceDelete();
                        AmazonUrlsAPIAdds::where('product_id',0)->forceDelete();

                    }
                }

                $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                logger('HTTP code: '.$httpcode);

                curl_close($curl);

                $this->handle();

            }

            logger("========================================================");
            logger("END :: Amazon Urls API :: JOB :: Delete URLs               ");
            logger("========================================================");
        }catch(Exception $e){
            logger("========================================================");
            logger('========== ERROR:: Amazon Urls API :: JOB :: Delete URLs    ');
            logger("========================================================");
            logger(json_encode($e->getMessage()));

            logger("========================================================");
        }
    }
}
