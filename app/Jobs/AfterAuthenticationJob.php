<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\ShopifyShop;
use App\Traits\OrderFulfillmentTrait;
use App\User;

class AfterAuthenticationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, OrderFulfillmentTrait;

    protected $shopAuthUser;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        logger("AfterAuthenticateJob Job is handle!");

        try {
            $this->shopAuthUser = \Auth::user();
            $shop = User::where('name', $this->shopAuthUser['name'])->firstOrFail();

            $setting = DB::table('settings')->insertOrIgnore(['user_id' => $shop->id]);

            if ($shop) {

                $exist_shop = ShopifyShop::where('user_id', $shop['id'])->first();

                if (!$exist_shop) {

                    $shopApi = $shop->api()->rest('GET', '/admin/api/' . env('SHOPIFY_API_VERSION') . '/shop.json');

                    if (!$shopApi['errors']) {
                        $shopApi = $shopApi['body']['shop'];

                        ShopifyShop::updateOrCreate(
                            ["shop_id" => $shopApi['id']],
                            [
                                "shop_id" => $shopApi['id'],
                                "name" => $shopApi['name'],
                                "email" => $shopApi['email'],
                                "domain" => $shopApi['domain'],
                                "province" => $shopApi['province'],
                                "country" => $shopApi['country'],
                                "address1" => $shopApi['address1'],
                                "zip" => $shopApi['zip'],
                                "city" => $shopApi['city'],
                                "primary_locale" => $shopApi['primary_locale'],
                                "country_code" => $shopApi['country_code'],
                                "country_name" => $shopApi['country_name'],
                                "currency" => $shopApi['currency'],
                                "shop_owner" => $shopApi['shop_owner'],
                                "weight_unit" => $shopApi['weight_unit'],
                                "province_code" => $shopApi['province_code'],
                                "plan_name" => $shopApi['plan_name'],
                                "user_id" => $shop['id']
                            ]
                        );

                        //   Add The Fulfilement Service
                        $this->fulfilment_service($shop, 'zinc-fulfilment');
                        $this->fulfilment_service($shop, 'Amazone-fulfilment');

                    } else {
                        //  logger("error during call Shop API !");
                    }
                } else {
                    // logger("Shop Exist!");
                }
            } else {
            }
        } catch (\Exception $e) {
            logger("AfterAuthenticateJob Job is Exception!");
            logger($e);
        }
    }
}
