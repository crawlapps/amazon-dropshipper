<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Osiset\ShopifyApp\Storage\Models\Plan as ModelsPlan;
use Exception;
use Keepa\KeepaAPI;
use Keepa\API\Request as KeepaAPIRequest;
use DB;
use App\Product;
use App\User;
use App\Traits\KeepaCustomAPITrait;

class KeepaTrackingRemoveDowngradeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    use KeepaCustomAPITrait;

    private $user_id = '';
    private $max_imports = '';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id, $max_imports)
    {
        $this->user_id = $user_id;
        $this->max_imports = $max_imports;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            logger('=============== START::JOB :: KeepaTrackingRemoveDowngrade Job =============');
            $user_id = $this->user_id;

            $products = Product::select('id')
            ->where('is_tracked',1)
            ->orderBy('id', 'desc')
            ->where('user_id',$user_id)
            ->limit($this->max_imports)
            ->get();

            if(count($products) > 0){
                $productIds = $products->pluck('id');

                $removableProduct = Product::select('id')->where('user_id',$user_id)->where('is_tracked',1)->whereNotIn('id', $productIds)->get();

                foreach($removableProduct as $key=>$removeProduct){
                    $arr['product_id'] = $removeProduct->id;
                    $this->keepaRemoveProductTracking($user_id, $arr);
                }

            }

            logger('======= get tokenstatus before aPI call =======');


             return true;
            logger('=============== END::JOB :: KeepaTracking AddProducts Script Job =============');
           }catch(Exception $e){
            logger('=============== ERROR::JOB :: KeepaTracking AddProducts Script Job =============');
            logger(json_encode($e->getMessage()));
            logger('=============== ERROR::JOB :: END =============');

        }
    }
}
