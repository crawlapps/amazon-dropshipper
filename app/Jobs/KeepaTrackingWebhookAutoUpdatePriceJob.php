<?php

namespace App\Jobs;

use App\KeepaTrackingProductsResponse;
use App\MainDailyListService;
use App\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Traits\AmazonUrlsAPITrait;
use App\User;
use App\Variant;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;

class KeepaTrackingWebhookAutoUpdatePriceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use AmazonUrlsAPITrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */


    private $product_id = '';
    private $asin = '';
    private $keepa_notifiaction_id = '';

    public function __construct($product_id, $asin = null, $keepa_notifiaction_id = '')
    {
        $this->product_id = $product_id;
        $this->asin = $asin;
        $this->keepa_notifiaction_id = $keepa_notifiaction_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            logger('=============== START::JOB :: KeepaTracking webhook AutoUpdate price  Job =============');
            $product_id = $this->product_id;
            logger("product_id => ".$product_id);

            $product = Product::select('id', 'user_id', 'imported_product_plan')->find($product_id);
            logger('=======================> Product');
            logger($product);

            if($product) {
                $user = User::select('id', 'plan_id')->find($product->user_id);
                logger("=======================> User");
                logger($user);
                // Plan id list which not getting direct product update
                $excludeUpdateNowPlanList = json_decode(env('EXCLUDE_UPDATE_NOW_PLAN_LIST', [1]));


                // Update plan detail in keepa tracking product response table
                KeepaTrackingProductsResponse::where('id', $this->keepa_notifiaction_id)->update(['plan_name' => $product->imported_product_plan]);

                if(in_array($user->plan_id, $excludeUpdateNowPlanList)) {
                    $this->updateIsGetKeepaNotificationFlag($product_id);
                } else {
                    logger("=== Preminum user =====> Auto update check");
                    if($this->qualifyToPriceUpdate()) {
                        logger("==========> Auto udpate start....");
                        $this->getLatestPriceFromKeepa($product_id, $this->keepa_notifiaction_id);
                    } else {
                        logger("Not qualify for auto update");
                    }
                }
            } else {
                $this->keepaRemoveProductTracking($this->asin);
            }


        }catch(Exception $e){
            logger('=============== ERROR::JOB ::  KeepaTracking webhook AutoUpdate price Job =============');
            logger(json_encode($e->getMessage()));
            logger('=============== ERROR::JOB :: END =============');

        }
    }

    public function updateIsGetKeepaNotificationFlag($product_id) {
        logger("==========================> Start:: Update is get keepa notification flag");
        Product::where('id', $product_id)->update(['is_get_price_update_notification'=> true, 'is_price_updated_manually' => true, 'updated_at' => Carbon::now()]);
        logger("==========================> End:: Update is get keepa notification flag");
    }

    public function keepaRemoveProductTracking($asin) {
        if($asin) {
            $accessKey = config('const.autoupdate_keepa_api_key');

            $apiUrl = "https://api.keepa.com/tracking/?key=".$accessKey."&"."type=remove&asin=".$asin;

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $apiUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                ),
            ));

            $response = curl_exec($curl);

            logger("------------------------------------------------");
            logger("Resposne ");
            logger(json_encode($response));
            logger("------------------------------------------------");

            $response = json_decode($response);

            $response_info[] = $response;

            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            curl_close($curl);
        }
    }

    // check price changes are greater than 3%
    public function qualifyToPriceUpdate() {
        $keepaResponse = KeepaTrackingProductsResponse::where('id', $this->keepa_notifiaction_id)->first();

        if($keepaResponse) {
            $webhookResponse = $keepaResponse->webhook_response;
            logger('==============> Webhook Response');
            logger(json_encode($webhookResponse));
            $latestPrice = $webhookResponse['currentPrices'][1] / 100;
            $mainVariant = Variant::where('product_id', $keepaResponse->product_id)->where('is_main', true)->whereNull('deleted_at')->first();

            if($mainVariant) {
                $currentPrice = $mainVariant->source_price_amazon;
                $percentageOfCurrentPrice = $currentPrice * 0.03; //Calculating percentage price of current price
                $priceDiffrence = abs($latestPrice - $currentPrice);

                logger("================================> Price Changes");
                logger("==============> Current price :: $currentPrice");
                logger("==============> Latest price :: $latestPrice");
                logger("==============> Percentage of price :: $percentageOfCurrentPrice");
                KeepaTrackingProductsResponse::where('id', $this->keepa_notifiaction_id)->update(['old_amazon_price'=> $currentPrice, 'new_amazon_price'=> $latestPrice]);
                if($priceDiffrence >= $percentageOfCurrentPrice) {
                    logger("================> Product auto updating:: Notification ID => " . $keepaResponse->id . " ASIN =>" . $keepaResponse->asin);
                    KeepaTrackingProductsResponse::where('id', $this->keepa_notifiaction_id)->update(['to_be_updated' => true]);
                    return true;
                }
            } else {
                logger("===============> Main Variant not found...");
            }
        } else {
            logger("===============> Keepa response not found...");
        }
        return false;
    }
}
