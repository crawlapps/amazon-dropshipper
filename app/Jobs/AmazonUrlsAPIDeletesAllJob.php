<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Exception;
use DB;
use App\Models\AmazonUrlsAPIAdds;

class AmazonUrlsAPIDeletesAllJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            logger("========================================================");
            logger("START :: Amazon Urls API :: JOB :: Delete URLs All      ");
            logger("========================================================");


            logger("amazon_urls_api_key");
            $amazon_urls_api_key = config('const.amazon_urls_api_key');
            logger($amazon_urls_api_key);

           // logger("product_urls");
           // logger(json_encode($product_urls));
          
                $params = [
                    "data" => [
                        [
                            "confirm" => 1
                        ]
                    ]
                ];

                $apiUrl = "http://50.16.141.15/amazon-api/delete/AllUrls";

                $query_param = json_encode($params, true);

                logger("query Param cred");
                logger($query_param);

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $apiUrl,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_POSTFIELDS => $query_param,
                    CURLOPT_CUSTOMREQUEST => 'DELETE',
                    CURLOPT_HTTPHEADER => array(
                        "Content-Type: application/json",
                        "api_key:".base64_encode($amazon_urls_api_key)
                    ),
                ));

                $response = curl_exec($curl);

                logger("===Response===");
                logger($response);

                if($response){
                    logger("res");
                    $response =  json_decode($response);

                    $status = $response->response->status;
                    if($status==200){                    
                        AmazonUrlsAPIAdds::truncate();
                    }
                }

                $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                logger('HTTP code: '.$httpcode);

                curl_close($curl);           

            logger("========================================================");
            logger("END :: Amazon Urls API :: JOB :: Delete URLs   All      ");
            logger("========================================================");
            
        }catch(Exception $e){
            logger("========================================================");
            logger('========== ERROR:: Amazon Urls API :: JOB :: Delete URLs  All ');
            logger("========================================================");
            logger(json_encode($e->getMessage()));

            logger("========================================================");
        }
    }
}
