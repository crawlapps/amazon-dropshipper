<?php

namespace App\Jobs;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use DB;
use App\Models\AmazonUrlsAPIAdds;
use App\Traits\AmazonUrlsAPITrait;
use App\MainDailyListService;
class getUrlsWebhookJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use AmazonUrlsAPITrait;

    public $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            logger("========================================================");
            logger("START :: Amazon Urls API :: JOB :: GET URLs callback_url ");
            logger("========================================================");

            $data  = $this->data;

            logger(json_encode($data));

            logger("Total count data => ". count($data) );

            if (count($data) > 0) {
                foreach ($data as $key => $value) {

                    logger("========================================================");
                    logger("product_id :: from url API =>".  $value['product_id']);
                    logger("value fields :: from url API =>". $value['value']);
                    logger("========================================================");

                    $value_field = isset($value['value']) ? $value['value'] : 0;
                    $product_id = isset($value['product_id']) ? $value['product_id'] : 0;

                    $product = MainDailyListService::where("product_id",$product_id)->first();

                    if($product){

                        MainDailyListService::where("product_id", $product_id)->update([
                            "value" => $value_field,
                            "response" => json_encode($value)
                        ]);

                        $this->updatePrice($value);

                    }
                }
            }



            logger("========================================================");
            logger("END :: Amazon Urls API :: JOB :: GET URLs  callback_url   ");
            logger("========================================================");
        }catch(Exception $e){
            logger("========================================================");
            logger('========== ERROR:: Amazon Urls API :: JOB :: GET URLs callback_url  ');
            logger("========================================================");
            logger(json_encode($e->getMessage()));

            logger("========================================================");
        }
    }


}
