<?php

namespace App\Jobs;

use App\Models\Counters;
use App\Traits\GraphQLTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Osiset\ShopifyApp\Services\ChargeHelper;
use Osiset\ShopifyApp\Objects\Values\ChargeReference;
use App\User;
use Carbon\Carbon;
use Osiset\ShopifyApp\Storage\Models\Charge;
use Illuminate\Support\Facades\DB;
use DateTime;
class TrackChargeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use GraphQLTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            logger('========== START:: TrackChargeJob =========');
            $users = User::select('id')->whereNotNull(['password', 'plan_id'])->where('deleted_at', null)->where('plan_id', '>', 1)->get();

            foreach($users as $key=>$user){
                $counter = Counters::where('user_id', $user->id)->where('status', 'active')->orderBy('created_at', 'desc')->whereDate('end_date', Carbon::today())->where('created_at', '<', now()->subDays(30)->endOfDay())->first();

                if($counter){
                    logger('Update Counter :: ' . $counter->id);
                    $counter->status = 'canceled';
                    $counter->save();
    
                    $newCounter = new Counters;
                    $newCounter->user_id = $counter->user_id;
                    $newCounter->charge_id = $counter->charge_id;
                    $newCounter->plan_id = $counter->plan_id;
                    $newCounter->regular_product_count = 0;
                    $newCounter->regular_product_variant_count = 0;
                    $newCounter->affiliate_product_count = 0;
                    $newCounter->affiliate_product_variant_count = 0;
                    $newCounter->auto_update_count = 0;
                    $newCounter->auto_update_affiliate = 0;
                    $newCounter->status = 'active';
                    $newCounter->start_date = date('Y-m-d H:i:s');
                    $newCounter->end_date = date('Y-m-d H:i:s',strtotime($newCounter->start_date.' + 30 days'));
                    $newCounter->save();
                }               
            }
        }catch( \Exception $e ){
            DB::rollBack();
            logger('========== ERROR:: TrackChargeJob =========');
            logger($e);
        }
    }
}
