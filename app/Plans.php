<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plans extends Model
{
    protected $table = 'plans';

    protected $fillable = ['type','name','price','interval','capped_amount','terms','trial_days','test','on_install', 'max_regular_product_import','max_affiliate_product_import'];

    public function plan_info(){
        return $this->hasOne(PlanContents::class, 'db_plans_id', 'id' );
    }
}
