<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProductPriceUpdateAlertMail extends Mailable
{
    use Queueable, SerializesModels;
    public $params;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        logger("START:: Mail --------------------.");
        logger("Product price update email alert mail!");
        logger("Sender IP: " . request()->ip());

        $subject = "Product Price Change";

        return $this->view('emails.product-price-update-alert-mail')->with([
            'params' => $this->params
        ])->subject($subject);

        logger("END:: Mail ----------------------.");
    }
}
