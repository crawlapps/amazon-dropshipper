<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PriceUpdateMail extends Mailable
{
    use Queueable, SerializesModels;
    public $params;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        logger("mail--------------------.");
        logger("Product price update email! ");
        logger("Requester IP: " . request()->ip());


        $subject = "Product Price Updated";

        return $this->view('emails.price-update-mail')->with([
            'params' => $this->params
        ])->subject($subject);

        logger("--------------------mail.");
    }
}
