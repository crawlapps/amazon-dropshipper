<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendKeepTokenStatusMail extends Mailable
{
    use Queueable, SerializesModels;

    private $params;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        logger("mail--------------------.");
        logger("user receives an order the email! ");
        logger("Requester IP: " . request()->ip());


        $subject = "Keepa token usages alert!";
        return $this->view('emails.keppa-token-usages-alert')->with([
            'params' => $this->params
        ])->subject($subject);

       logger("--------------------mail.");
    }
}
