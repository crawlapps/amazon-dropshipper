<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KeepaTrackingProductsResponse extends Model
{
    protected $table = 'keepa_tracking_products_responses';

    protected $fillable = [
        'product_id',
        'asin',
        'source_url',
        'webhook_response',
        'to_be_updated',
        'old_amazon_price',
        'new_amazon_price',
        'plan_name'
    ];

    protected $casts = [
        'webhook_response' => 'array',
    ];
}
