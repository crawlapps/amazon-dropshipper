<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AmazonUrlsAPIAdds extends Model
{

    use SoftDeletes;

    protected $table = 'amazon_urls_api_adds';

    protected $fillable = [
        'shopify_product_id',
        'variant_db_id',
        'product_url',
        'record_no',
        'product_id',
        'error',
        'value',
        'price',
        'old_price',
        'amazon_new_price',
        'amazon_old_price',
        'response'        
    ];

    protected $dates = ['deleted_at'];

    protected $casts = [
        'response' => 'array',
    ];

}
