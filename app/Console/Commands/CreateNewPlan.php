<?php

namespace App\Console\Commands;

use App\PlanContents;
use App\Plans;
use Illuminate\Console\Command;
use Throwable;

class CreateNewPlan extends Command
{

    private $planDetails;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plan:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new plan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            logger("=============== Start: Create super plan ===============");
            echo "\n===================== Plan ==========================\n\n";
            $planDetails['type'] = readline("Plan type (RECURRING | ONETIME) : ");
            $planDetails['name'] = readline("Plan name: ");
            $planDetails['price'] = (float)readline("Plan Price: ");
            $planDetails['interval'] = readline("Plan interval (EVERY_30_DAYS | ANNUAL): ");
            $planDetails['capped_amount'] = (float)readline("Capped_ampunt: ");
            $planDetails['terms'] = readline("Plan terms: ");
            $planDetails['trial_days'] = (int)readline("Trail days: ");
            $planDetails['test'] = (int)readline("Test plan (1 | 0): ");
            $planDetails['on_install'] = (int)readline("On install (1 | 0): ");
            $planDetails['max_regular_product_import'] = (int)readline("Maximum regular product import count: ");
            $planDetails['max_affiliate_product_import'] = readline("Max affiliate product import count (unlimited | number): ");

            // $plan = new Plans();
            // $plan->type = readline("Plan type (RECURRING | ONETIME) : ");
            // $plan->name = readline("Plan name: ");
            // $plan->price = (float)readline("Plan Price: ");
            // $plan->interval = readline("Plan interval (EVERY_30_DAYS | ANNUAL): ");
            // $plan->capped_amount = (float)readline("Capped_ampunt: ");
            // $plan->terms = readline("Plan terms: ");
            // $plan->trial_days = (int)readline("Trail days: ");
            // $plan->test = (int)readline("Test plan (1 | 0): ");
            // $plan->on_install = (int)readline("On install (1 | 0): ");
            // $plan->max_regular_product_import = (int)readline("Maximum regular product import count: ");
            // $plan->max_affiliate_product_import = readline("Max affiliate product import count (unlimited | number): ");
            // $plan->save();

            // $plan_content = new PlanContents();
            // $plan_content->db_plans_id = $plan['id'];
            // $plan_content->contents = $this->planDetail['contents'];
            // $plan_content->save();

            logger("=============== End: Create super plan ===============");
        } catch(Throwable $e) {
            logger("=============== Error: Create super plan ===============");
            logger($e);
        }
    }
}
