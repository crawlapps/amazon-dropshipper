<?php

namespace App\Console\Commands;

use App\PlanContents;
use App\Plans;
use Illuminate\Console\Command;
use Throwable;

class CreateSuperBillingPlan extends Command
{

    // private $planDetail = [
    //     'type' => 'RECURRING',
    //     'name' => 'Super',
    //     'price' => 3500,
    //     'interval' => "EVERY_30_DAYS",
    //     'capped_amount' => 0.00,
    //     'terms' => 'Every 30 days cycle starts at subscription day',
    //     'trial_days' => 0,
    //     'test' => 0,
    //     'on_install' => 0,
    //     'max_regular_product_import' => 3500,
    //     'max_affiliate_product_import' => 'unlimited',
    //     'contents' => [
    //         "is_popular" => "false",
    //         "header_background_color" => "rgb(134 193 71)",
    //         "header_plan_background_color" => "rgba(134,193,71,0.2)",
    //         "save_text" => "",
    //         "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
    //         "row2_html" => "<p><b>Automated daily prices/stock Sync</b> with<br> Amazon to update your <br>store prices/stock automatically <br> (3500 products/mo) </p>",
    //         "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
    //         "row4_html" => "<p><b>Get Product Data & Statistic</b> <ul><li>* Estimated Monthly Sales</li><li>* Best Seller Rank (BSR)</li><li>* Price History Chart</li><li>* Best Seller Categories<br> (Only Amazon)</li></ul> </p>",
    //         "row5_html" => "<p><b>Unlimitted Amazon <br> Affiliates Import</br></p>",
    //         "row6_html" => "<p><b>Automated Order <br> Fulfilment option </b><br>(soon)</p>",
    //     ]
    // ];

    private $plans = [
        [
            'type' => 'RECURRING',
            'name' => 'Super',
            'price' => 3500,
            'interval' => "EVERY_30_DAYS",
            'capped_amount' => 0.00,
            'terms' => 'Every 30 days cycle starts at subscription day',
            'trial_days' => 0,
            'test' => 0,
            'on_install' => 0,
            'max_regular_product_import' => 3500,
            'max_affiliate_product_import' => 'unlimited',
            'contents' => [
                "is_popular" => "false",
                "header_background_color" => "rgb(134 193 71)",
                "header_plan_background_color" => "rgba(134,193,71,0.2)",
                "save_text" => "",
                "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
                "row2_html" => "<p><b>Automated daily prices/stock Sync</b> with<br> Amazon to update your <br>store prices/stock automatically <br> (3500 products/mo) </p>",
                "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
                "row4_html" => "<p><b>Get Product Data & Statistic</b> <ul><li>* Estimated Monthly Sales</li><li>* Best Seller Rank (BSR)</li><li>* Price History Chart</li><li>* Best Seller Categories<br> (Only Amazon)</li></ul> </p>",
                "row5_html" => "<p><b>Unlimitted Amazon <br> Affiliates Import</br></p>",
                "row6_html" => "<p><b>Automated Order <br> Fulfilment option </b><br>(soon)</p>",
            ]
        ],
        [
            'type' => 'RECURRING',
            'name' => 'Pro',
            'price' => 300,
            'interval' => "EVERY_30_DAYS",
            'capped_amount' => 10.00,
            'terms' => 'Every 30 days cycle starts at subscription day',
            'trial_days' => 0,
            'test' => 1,
            'on_install' => 0,
            'max_regular_product_import' => 300,
            'max_affiliate_product_import' => 'unlimited',
            'contents' => [
                "is_popular" => "true",
                "header_background_color" => "#000",
                "header_plan_background_color" => "rgba(0,0,0,0.2)",
                "save_text" => "<span>Just 165/mo with yearly plan!</span>",
                "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
                "row2_html" => "<p><b>Automated daily prices/stock Sync</b> with<br> Amazon to update your <br>store prices/stock automatically <br> (300 products/mo) </p>",
                "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
                "row4_html" => "<p><b>* Get Product Data & Statistic</b> <ul><li>* Estimated Monthly Sales</li><li>* Best Seller Rank (BSR)</li><li>* Price History Chart</li><li>* Best Seller Categories<br> (Only Amazon)</li></ul> </p>",
                "row5_html" => "<p><b>Unlimitted Amazon <br> Affiliates Import</br></p>",
                "row6_html" => "<p><b>Automated Order <br> Fulfilment option </b><br>(soon)</p>",
            ]

        ],
        [
            'type' => 'RECURRING',
            'name' => 'Suite',
            'price' => 50,
            'interval' => "EVERY_30_DAYS",
            'capped_amount' => 10.00,
            'terms' => 'Every 30 days cycle starts at subscription day',
            'trial_days' => 0,
            'test' => 1,
            'on_install' => 0,
            'max_regular_product_import' => 50,
            'max_affiliate_product_import' => 'unlimited',
            'contents' => [
                "is_popular" => "false",
                "header_background_color" => "rgb(134 193 71)",
                "header_plan_background_color" => "rgba(134,193,71,0.2)",
                "save_text" => "<span>Just 25$/mo with yearly plan!</span>",
                "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
                "row2_html" => "<p><b>Automated daily prices/stock Sync</b> with<br> Amazon to update your <br>store prices/stock automatically <br> (50 products/mo) </p>",
                "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
                "row4_html" => "<p><b>Get Product Data & Statistic</b> <ul><li>* Estimated Monthly Sales</li><li>* Best Seller Rank (BSR)</li><li>* Price History Chart</li><li>* Best Seller Categories<br> (Only Amazon)</li></ul> </p>",
                "row5_html" => "<p><b>Unlimitted Amazon <br> Affiliates Import</br></p>",
                "row6_html" => "<p><b>Automated Order <br> Fulfilment option </b><br>(soon)</p>",
            ]
        ]
    ];


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plan:createSuperPlan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create super plan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            logger("=============== Start: Create super plan ===============");
            foreach ($this->plans as $planDetail) {
                $plan = new Plans();
                $plan->type = $planDetail['type'];
                $plan->name = $planDetail['name'];
                $plan->price = $planDetail['price'];
                $plan->interval = $planDetail['interval'];
                $plan->capped_amount = $planDetail['capped_amount'];
                $plan->terms = $planDetail['terms'];
                $plan->trial_days = $planDetail['trial_days'];
                $plan->test = $planDetail['test'];
                $plan->on_install = $planDetail['on_install'];
                $plan->max_regular_product_import = $planDetail['max_regular_product_import'];
                $plan->max_affiliate_product_import = $planDetail['max_affiliate_product_import'];
                $plan->save();


                $plan_content = new PlanContents();
                $plan_content->db_plans_id = $plan['id'];
                $plan_content->contents = $planDetail['contents'];
                $plan_content->save();
            }

            logger("=============== End: Create super plan ===============");
        } catch(Throwable $e) {
            logger("=============== Error: Create super plan ===============");
            logger($e);
        }

    }
}
