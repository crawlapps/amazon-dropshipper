<?php

namespace App\Console\Commands;

use App\Jobs\RemoveProductTrackingOfTrialUsersJob;
use Illuminate\Console\Command;

class RemoveProductTrackingOfTrialUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'RemoveProductTrackingOfTrailUsers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command remove product price tracking of whose users trail period is over';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            logger('=============== START::CRON :: RemoveProductTrackingOfTrialUsers =============');

            RemoveProductTrackingOfTrialUsersJob::dispatch();

            logger('=============== END::CRON :: RemoveProductTrackingOfTrialUsers =============');
        }
        catch(Exception $e){
            logger('=============== ERROR::CRON :: RemoveProductTrackingOfTrialUsers =============');
            logger(json_encode($e->getMessage()));
            logger('=============== ERROR::CRON :: END =============');

        }
    }
}
