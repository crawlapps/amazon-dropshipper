<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\AmazonUrlsAPIGetsJob;

class AmazonUrlsAPIGetsCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amazon-get-urls:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read Documentation => http://50.16.141.15/amazon-api/docs/';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            logger('=============== START::CRON :: AmazonUrlsAPIGetsJob =============');

            AmazonUrlsAPIGetsJob::dispatch();

            // logger('=============== END:: AmazonUrlsAPIGetsJob =============');
        }catch( \Exception $e ){
            logger('=============== ERROR :: CRON ::  AmazonUrlsAPIGetsJob =============');
            logger(json_encode($e->getMessage()));
        }
    }
}
