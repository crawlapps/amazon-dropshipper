<?php

namespace App\Console\Commands;

use App\User;
use Exception;
use Illuminate\Console\Command;

class CheckWebhookListCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:checkWebhooklist';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check webhook list and register if app uninstall is not registered';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        try {
            logger('=============== START::Command :: Check Webhook list =============');
            $users = User::whereNull('deleted_at')->whereIn('plan_id', [16,7, 15, 9, 11, 13, 12, 10, 8])->orderBy('id', 'desc')->get();

            if(count($users) > 0) {
                foreach ($users as $user) {
                    if($user) {
                        // $webhookList = webhooksList($user->id);

                        // if(!$webhookList['errors']){
                        //     $hasRegisterAppUninstallHook = false;
                        //     if(count($webhookList) > 0) {
                        //         foreach($webhookList as $webhook) {
                        //             if($webhook['topic'] == 'app/uninstalled') {
                        //                 $hasRegisterAppUninstallHook = true;
                        //                 break;
                        //             }
                        //         }
                        //     }
                        // }



                        // if($hasRegisterAppUninstallHook) {
                        //     logger('===========> Missing to register app uninstall');
                        // }

                        // logger("==========> Webhook list <===========");
                        // logger(json_encode($webhookList));

                        $this->makeWebhooksFromConfig($user);
                    }
                }
            }

            logger('=============== END::Command :: Check Webhook list =============');

        } catch (Exception $e) {
            logger('=============== ERROR::Command :: Check Webhook list =============');
            logger(json_encode($e->getMessage()));
        }
    }

    public function makeWebhooksFromConfig(User $user)
    {
        logger('============= Start :: Mannual webhooks register ' . $user->name . ' ==============');
        $webhooks = $user->api()->rest('GET', 'admin/webhooks.json');
        if ($webhooks['errors']) {
            return false;
            logger('============= Error :: Mannual webhooks register ==============');
            logger($webhooks);
            // dump('ERROR::');
            // dd($webhooks);
        }
        $webhooks = $webhooks['body']->container['webhooks'];
        if (count($webhooks) == 8) {
            logger('============= Start :: All webhooks exist ==============');
            return $webhooks;
        }
        logger(count($webhooks));

        $exists = function (array $webhook, $webhooks): bool {
            if (count($webhooks) == 0) {
                return false;
            }
            foreach ($webhooks as $shopWebhook) {
                if ($shopWebhook['address'] === $webhook['address'] && $shopWebhook['topic'] === $webhook['topic']) {
                    // Found the webhook in our list
                    return true;
                }
            }
            return false;
        };

        $createWebhookRes = [];
        foreach (config('shopify-app.webhooks') as $configWebhook) {
            // Check if the required webhook exists on the shop
            if (!$exists($configWebhook, $webhooks)) {
                logger('Will Create');
                $createWebhook = $user->api()->rest(
                    'POST',
                    'admin/webhooks.json',
                    [
                        "webhook" =>
                        [
                            "address" => env('AWS_ARN_WEBHOOK_ADDRESS'),
                            "topic" => $configWebhook['topic'],
                            "format" => "json"
                        ]
                    ]
                );
                if (!$createWebhook['errors']) {
                    $createWebhook = $createWebhook['body']->container['webhook'];
                    $createWebhookRes[] = [$createWebhook['id'], $createWebhook['topic']];
                } else {
                    logger(json_encode($createWebhook));
                }
            } else {
                logger('Will Not Create');
            }
        }
        // dd($createWebhookRes);
        return $createWebhookRes;
    }
}
