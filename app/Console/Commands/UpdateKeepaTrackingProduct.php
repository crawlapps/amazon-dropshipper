<?php

namespace App\Console\Commands;

use App\Jobs\KeepaTrackingUpdateProductsIndividualNotificationInterval;
use Illuminate\Console\Command;

class UpdateKeepaTrackingProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:keepaTackingProduct';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update keepa tracking product individual notification insterval param';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            logger('=============== START::Command :: UpdateKeepaTrackingProduct =============');

            KeepaTrackingUpdateProductsIndividualNotificationInterval::dispatch();

            logger('=============== END:: Command :: UpdateKeepaTrackingProduct =============');
        } catch (\Exception $e) {
            logger('=============== ERROR :: Command ::  UpdateKeepaTrackingProduct =============');
            logger(json_encode($e->getMessage()));
        }
    }
}
