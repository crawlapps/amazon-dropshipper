<?php

namespace App\Console\Commands;

use App\PlanContents;
use App\Plans;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use Throwable;

class UpdatePlanDetails extends Command
{

    private $plans = [
        [
            'type' => 'RECURRING',
            'name' => 'Suite',
            'price' => 50,
            'interval' => "EVERY_30_DAYS",
            'capped_amount' => 10.00,
            'terms' => 'Every 30 days cycle starts at subscription day',
            'trial_days' => 0,
            'test' => 1,
            'on_install' => 0,
            'max_regular_product_import' => 100,
            'max_affiliate_product_import' => 500,
            'contents' => [
                "is_popular" => "true",
                "header_background_color" => "rgb(134 193 71)",
                "header_plan_background_color" => "rgba(134,193,71,0.2)",
                "save_text" => "<span>Just 25$/mo with yearly plan!</span>",
                "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
                "row2_html" => "<p><b>Automated daily prices/stock Sync</b> with<br> Amazon to update your <br>store prices/stock automatically <br> (100 products/mo) </p>",
                "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
                "row4_html" => "<p><b>Get Product Data & Statistic</b> <ul><li>* Estimated Monthly Sales</li><li>* Best Seller Rank (BSR)</li><li>* Price History Chart</li><li>* Best Seller Categories<br> (Only Amazon)</li></ul> </p>",
                "row5_html" => "<p><b>Unlimitted Amazon <br> Affiliates Import</br></p>",
                "row6_html" => "<p><b>Automated Order <br> Fulfilment option </b><br>(soon)</p>",
            ]
        ],
        [
            'type' => 'RECURRING',
            'name' => 'Startup',
            'price' => 35,
            'interval' => "EVERY_30_DAYS",
            'capped_amount' => 10.00,
            'terms' => 'Every 30 days cycle starts at subscription day',
            'trial_days' => 0,
            'test' => 1,
            'on_install' => 0,
            'max_regular_product_import' => 25,
            'max_affiliate_product_import' => 500,
            'contents' => [
                "is_popular" => "false",
                "header_background_color" => "#f1620f",
                "header_plan_background_color" => "rgba(241,98,15,0.2)",
                "save_text" => "<span>Just 25$/mo with yearly plan!</span>",
                "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
                "row2_html" => "<p><b>Automated Price Change Alerts</b> with Amazon so you can update  <br>your store prices/stock  <br>(25 products/mo) </p>",
                "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
                "row4_html" => "<p><b>Get Product Data & Statistic</b> <ul><li>* Estimated Monthly Sales</li><li>* Best Seller Rank (BSR)</li><li>* Price History Chart</li><li>* Best Seller Categories<br> (Only Amazon)</li></ul> </p>",
                "row5_html" => "<p><b>Unlimitted Amazon <br> Affiliates Import</br></p>",
                "row6_html" => "<p><b>Automated Order <br> Fulfilment option </b><br>(soon)</p>",
            ]
        ],
        [
            'type' => 'RECURRING',
            'name' => 'Pro',
            'price' => 300,
            'interval' => "EVERY_30_DAYS",
            'capped_amount' => 10.00,
            'terms' => 'Every 30 days cycle starts at subscription day',
            'trial_days' => 0,
            'test' => 1,
            'on_install' => 0,
            'max_regular_product_import' => 500,
            'max_affiliate_product_import' => 500,
            'contents' => [
                "is_popular" => "false",
                "header_background_color" => "#000",
                "header_plan_background_color" => "rgba(0,0,0,0.2)",
                "save_text" => "<span>Just 165/mo with yearly plan!</span>",
                "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
                "row2_html" => "<p><b>Automated daily prices/stock Sync</b> with<br> Amazon to update your <br>store prices/stock automatically <br> (500 products/mo) </p>",
                "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
                "row4_html" => "<p><b>* Get Product Data & Statistic</b> <ul><li>* Estimated Monthly Sales</li><li>* Best Seller Rank (BSR)</li><li>* Price History Chart</li><li>* Best Seller Categories<br> (Only Amazon)</li></ul> </p>",
                "row5_html" => "<p><b>Unlimitted Amazon <br> Affiliates Import</br></p>",
                "row6_html" => "<p><b>Automated Order <br> Fulfilment option </b><br>(soon)</p>",
            ]

        ],
        [
            'type' => 'RECURRING',
            'name' => 'Suite',
            'price' => 300,
            'interval' => "ANNUAL",
            'capped_amount' => 10.00,
            'terms' => 'only one time at subscription time',
            'trial_days' => 0,
            'test' => 1,
            'on_install' => 0,
            'max_regular_product_import' => 100,
            'max_affiliate_product_import' => 500,
            'contents' => [
                "is_popular" => "true",
                "header_background_color" => "rgb(134 193 71)",
                "header_plan_background_color" => "rgba(134,193,71,0.2)",
                "save_text" => "<span>Save 300$ with yearly plan!</span>",
                "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
                "row2_html" => "<p><b>Automated daily prices/stock Sync</b> with<br> Amazon to update your <br>store prices/stock automatically <br> (100 products/mo) </p>",
                "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
                "row4_html" => "<p><b>Get Product Data & Statistic</b> <ul><li>* Estimated Monthly Sales</li><li>* Best Seller Rank (BSR)</li><li>* Price History Chart</li><li>* Best Seller Categories<br> (Only Amazon)</li></ul> </p>",
                "row5_html" => "<p><b>Unlimitted Amazon <br> Affiliates Import</br></p>",
                "row6_html" => "<p><b>Automated Order <br> Fulfilment option </b><br>(soon)</p>",
            ]

        ],
        [
            'type' => 'RECURRING',
            'name' => 'Startup',
            'price' => 180,
            'interval' => "ANNUAL",
            'capped_amount' => 10.00,
            'terms' => 'only one time at subscription time',
            'trial_days' => 0,
            'test' => 1,
            'on_install' => 0,
            'max_regular_product_import' => 25,
            'max_affiliate_product_import' => 500,
            'contents' => [
                "is_popular" => "false",
                "header_background_color" => "#f1620f",
                "header_plan_background_color" => "rgba(241,98,15,0.2)",
                "save_text" => "<span>Save 240$ with yearly plan!</span>",
                "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
                "row2_html" => "<p><b>Automated Price Change Alerts</b> with /<br>Amazon so you can update  <br>your store prices/stock  <br>(25 products/mo) </p>",
                "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
                "row4_html" => "<p><b>Get Product Data & Statistic</b> <ul><li>* Estimated Monthly Sales</li><li>* Best Seller Rank (BSR)</li><li>*  Price History Chart</li><li>*  Best Seller Categories<br> (Only Amazon)</li></ul> </p>",
                "row5_html" => "<p><b>Unlimitted Amazon <br> Affiliates Import</br></p>",
                "row6_html" => "<p><b>Automated Order <br> Fulfilment option </b><br>(soon)</p>",
            ]

        ],
        [
            'type' => 'RECURRING',
            'name' => 'Pro',
            'price' => 2000,
            'interval' => "ANNUAL",
            'capped_amount' => 10.00,
            'terms' => 'only one time at subscription time',
            'trial_days' => 0,
            'test' => 1,
            'on_install' => 0,
            'max_regular_product_import' => 500,
            'max_affiliate_product_import' => 500,
            'contents' => [
                "is_popular" => "false",
                "header_background_color" => "#000",
                "header_plan_background_color" => "rgba(0,0,0,0.2)",
                "save_text" => "<span>Save 1,588$ with yearly plan!</span>",
                "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
                "row2_html" => "<p><b>Automated daily prices/stock Sync</b> with<br> Amazon to update your <br>store prices/stock automatically <br> (500 products/mo) </p>",
                "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
                "row4_html" => "<p><b>Get Product Data & Statistic</b> <ul><li>* Estimated Monthly Sales</li><li>* Best Seller Rank (BSR)</li><li>* Price History Chart</li><li>* Best Seller Categories<br> (Only Amazon)</li></ul> </p>",
                "row5_html" => "<p><b>Unlimitted Amazon <br> Affiliates Import</br></p>",
                "row6_html" => "<p><b>Automated Order <br> Fulfilment option </b><br>(soon)</p>",
            ]
        ]
    ];

    private $free_plan_contents =  [
        "is_popular" => "false",
        "header_background_color" => "#6c6c6c",
        "header_plan_background_color" => "rgba(108,108,108,0.2)",
        "save_text" => "<span class='no-save-text'> </span>",
        "row1_html" => "<p>Works with Amazon US, CA<br> UK, DE, AU, JPN, IN, BR, MX, IT<br> SP, FR & Walmart US</p>",
        "row2_html" => "<p><b>Automated Price Change Alerts</b> with Amazon so you can update  <br>your store prices/stock  <br>(Only for 30 days) </p>",
        "row3_html" => "<p><b>Import User Reviews</b> <br> from Amazon and Walmart <br> (with images) </p>",
        "row4_html" => "<p><b>Get Product Data & Statistic</b> <ul><li>* Estimated Monthly Sales</li><li>* Best Seller Rank (BSR)</li>
                          <li>* Price History Chart</li><li>* Best Seller Categories<br> (Only Amazon)</li></ul> </p>",
        "row5_html" => "<p>10 Amazon Affliates <br> Imports</p>",
        "row6_html" => "<p>Automated Order <br> Fulfilment option <br>(just 1st order) <br>(soon)</p>",
    ];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:planDetails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update existing plan contents';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        try {
            logger('========================== Start: Update plan details ==========================');

            PlanContents::where('db_plans_id', 1)->update(['contents'=> $this->free_plan_contents, 'updated_at'=> Carbon::now()]);

            foreach($this->plans as $plan) {
                $plan_id = Plans::where('name', $plan['name'])->where('interval', $plan['interval'])->value('id');
                PlanContents::where('db_plans_id', $plan_id)->update(['contents'=> $plan['contents'], 'updated_at' => Carbon::now()]);
            }
            logger('========================== End: Update plan details ==========================');
        } catch (Throwable $e) {
            logger('========================== Error: Update plan details ==========================');
            logger($e);
        }
    }
}
