<?php

namespace App\Console\Commands;

use App\Jobs\SendProductPriceUpdateAlertMailJob;
use Exception;
use Illuminate\Console\Command;

class SendProductPriceUpdateAlertMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:sendProductPriceAlert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send product price update mail to freeminum users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            logger('=============== START::CRON :: Send Product Price Update ALert Mail =============');

            SendProductPriceUpdateAlertMailJob::dispatch();

            logger('=============== END::CRON :: Send Product Price Update ALert Mail =============');
        }
        catch(Exception $e){
            logger('=============== ERROR::CRON :: Send Product Price Update ALert Mail =============');
            logger(json_encode($e->getMessage()));
            logger('=============== ERROR::CRON :: END =============');

        }
    }
}
