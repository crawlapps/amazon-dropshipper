<?php

namespace App\Console\Commands;

use App\Jobs\CheckStoreStatusJob;
use Illuminate\Console\Command;

class CronCheckStoreStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:checkStoreStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check closed store and perform remove tracking, update status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            logger('=============== START::CRON :: Check store status =============');

            CheckStoreStatusJob::dispatch();

            logger('=============== END::CRON :: Check store status =============');
        }
        catch(Exception $e){
            logger('=============== ERROR::CRON :: Check store status =============');
            logger(json_encode($e->getMessage()));
            logger('=============== ERROR::CRON :: END =============');

        }
    }
}
