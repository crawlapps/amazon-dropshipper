<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\MainDailyListJob;

class MainDailyListCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'main-daily-list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            logger('=============== START::CRON :: MainDailyListCron =============');

            MainDailyListJob::dispatch();

            logger('=============== END::CRON :: MainDailyListCron =============');
        }
        catch(Exception $e){
            logger('=============== ERROR::CRON :: MainDailyListCron =============');
            logger(json_encode($e->getMessage()));
            logger('=============== ERROR::CRON :: END =============');

        }
    }
}
