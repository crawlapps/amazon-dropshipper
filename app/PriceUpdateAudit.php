<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceUpdateAudit extends Model
{
    protected $fillable = ['product_id', 'user_id', 'asin', 'keepa_notification_id', 'old_amazon_price', 'new_amazon_price', 'old_store_price', 'new_store_price', 'update_type'];
}
