<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainDailyListService extends Model
{
    protected $table = 'main_daily_list_services';

    protected $fillable = [
        'user_id',
        'shopify_product_id',
        'product_id',
        'product_url',
        'source_price_amazon',
        'list_type',
        'error',
        'value',
        'amazon_new_price',
        'response'              
    ];

    protected $casts = [
        'response' => 'array',
    ];
}
