<?php

namespace App\Traits;

use App\EmailLog;
use App\Mail\PriceUpdateMail;
use App\Models\ShopifyShop;
use App\Product;
use App\User;
use Illuminate\Support\Facades\Mail;

/**
 * Trait AutoUpdate.
 */
trait SendEmailTrait
{
    public function sendUpdatePriceEmail($user_id, $product_id) {
        try{
            logger('====================== START:: SendUpdatePriceEmail ======================');
            $user = User::find($user_id);
            if( $user ){
                $product = Product::where('id', $product_id)->first();
                $shop = ShopifyShop::where('user_id', $user_id)->first();

                if(isset($shop['main_email'])) {
                    $to_mail = $shop['main_email'];
                } else {
                    $to_mail = $shop['email'];
                }
                $params = [];
                $params['amazon_link'] = $product['source_url'];
                $params['product_name'] = $product['shopify_handle'];
                $params['store_link'] = "https://".$shop['domain']. "/products/" .  $product['shopify_handle'];
                // Mail::to($to_mail)->send(new PriceUpdateMail($params));

                // Email log
                // logger("========> Track Update price mail");
                // EmailLog::create(["trigger" => "PRODUCT_PRICE_UPDATE", 'user_id'=> $shop->user_id, 'product_id' => $product_id]);
                // logger("========> Track Update price mail");

            }else {
                logger('====================== INTERNAL ERROR:: User '. $user_id .' not found... ======================');
            }

            logger('====================== END:: SendUpdatePriceEmail ======================');
            return [];
        }catch( \Exception $e ){
            logger('====================== ERROR:: SendUpdatePriceEmail ======================');
            logger($e);
        }
    }
}
