<?php

namespace App\Traits;

use App\MainDailyListService;
use Exception;
use Response;
use DB;
use App\Product;
use App\User;
use Osiset\ShopifyApp\Storage\Models\Plan as ModelsPlan;

trait KeepaCustomAPITrait
{


    public function testt()
    {

        return "hello";

    }


    public function keepaSetWebhook()
    {

        try {
            logger("=============START :: Traits :: keepaSetWebhook===================");

            $accessKey = config('const.autoupdate_keepa_api_key');

            $webhookURL = route('keepa.webhook');

            logger("webhookURL");
            logger($webhookURL);

            $apiUrl = "https://api.keepa.com/tracking/?key=".$accessKey."&"."type=webhook&url=".$webhookURL; //Get Tracking

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $apiUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                ),
            ));

            $response = curl_exec($curl);

            logger("Res :: set-Webhook");
            logger(json_encode($response));


            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            logger('HTTP code: '.$httpcode);

            curl_close($curl);

            return $httpcode;

        } catch (Exception $e) {
            logger("=============ERROR :: Traits :: keepaSetWebhook===================");
            return Response::json(["data" => $e], 422);
        }


    }


    public function keepaAddProductTracking($user_id, $param)
    {

        try {

            logger("=============START :: Traits :: add Track Product===================");

            $is_set_webhok = DB::table('keepa_webhook')->select('is_set_webhook')->where('user_id',
                $user_id)->value("is_set_webhook");

            if (!$is_set_webhok && $is_set_webhok !== 0) {

                $webhookRes = $this->keepaSetWebhook();

                if ($webhookRes == 200 || $webhookRes == 400) {
                    DB::table('keepa_webhook')->insert([
                        'is_set_webhook' => 1,
                        'user_id' => $user_id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    ]);
                }
            }

            $asin = $param['asin'];
            $mainDomainId = $param['mainDomainId'];
            $updateInterval = $param['updateInterval'];
            $product_id =  $param['product_id'];

            logger("updateInterval". $updateInterval);

            $isReachedLimit = $this->calculatePlanWiseTrackings($user_id);

            if($isReachedLimit != ''){
                $params['product_id'] = $isReachedLimit;
                $this->keepaRemoveProductTracking($user_id, $params);
            }

            $accessKey = config('const.autoupdate_keepa_api_key');

            $thresholdValues = [
                                   [
                                       "thresholdValue" => 999999999,
                                        "domain" => $mainDomainId,
                                        "csvType" => 0,
                                        "isDrop" => true
                                    ],
                                    // track any price increases
                                    [
                                        "thresholdValue" => 0,
                                        "domain" => $mainDomainId,
                                        "csvType" => 0,
                                        "isDrop" => false
                                    ],
                                    [
                                        "thresholdValue" => 999999999,
                                        "domain" => $mainDomainId,
                                        "csvType" => 1,
                                        "isDrop" => true
                                    ],
                                    // track any price increases
                                    [
                                        "thresholdValue" => 0,
                                        "domain" => $mainDomainId,
                                        "csvType" => 1,
                                        "isDrop" => false
                                    ],
                               ];

            $notifyIf = [
                [
                    "domain" => $mainDomainId,
                    "csvType" => 0,
                    "notifyIfType" => 1            //   0: OUT_OF_STOCK, 1: BACK_IN_STOCK
                ],
            ];

            $params = [
                "asin" => $asin,
                "mainDomainId" => $mainDomainId,
                "ttl" => 0,
                "expireNotify" => true,
                "desiredPricesInMainCurrency" => false,
                "updateInterval" => $updateInterval,
                "metaData" => $product_id,
                "thresholdValues" => $thresholdValues,
                "notifyIf" => $notifyIf,
                "notificationType" => [ true, true, true, true, true, true, true ],
                "individualNotificationInterval" => 5
            ];

            logger("tracking param");
            logger(json_encode($params));

            $apiUrl = "https://api.keepa.com/tracking/?key=".$accessKey."&"."type=add";

            $query_param = json_encode($params, true);

            logger("query Param cred");
            logger($query_param);

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $apiUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_POSTFIELDS => $query_param,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                ),
            ));

            $response = curl_exec($curl);

            logger("===Response===");
            logger(json_encode($response));

            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            logger('HTTP code: '.$httpcode);

            curl_close($curl);

            return true;

            logger("=============END :: Traits ::  add Track Product===================");

        } catch (Exception $e) {
            logger("=============ERROR :: Traits ::  add Track Product===================");
            return Response::json(["data" => $e], 422);
        }
    }

    public function calculatePlanWiseTrackings($user_id){
        try{
            $user = User::find($user_id);
            $plan = ModelsPlan::find($user->plan_id);
            $totalTrackings = Product::where(['user_id'=> $user_id, 'is_tracked' => 1])->count();

            logger('Total tracking :: ' .$totalTrackings );
            $product_id = '';
            if($totalTrackings >= $plan->max_regular_product_import){
                $product = Product::select('id')->where('is_tracked', 1)->first();
                $product_id = ($product) ? $product->id : '';
            }
            logger('product_id:: ' .$product_id );

            return $product_id;
        }catch(\Exception $e){
            logger("=============ERROR :: Traits ::  calculatePlanWiseTrackings===================");
            logger($e);
        }
    }

    public function keepaRemoveProductTracking($user_id, $param){

        try{

         logger("=============START :: Traits :: remove Track Product====================");
         $product_id =  $param['product_id'];

         logger("Remove Product is => ". $product_id);

         $products = Product::select('source_url','locale','id')
         ->where('locale','<>',"")
         ->where('is_tracked',1)
         ->where('id',$product_id)
         ->where('source',"Amazon")->first();

          if($products){

                    $source_url =  $products->source_url;
                    preg_match('/\/([a-zA-Z0-9]{10})/',$source_url,$parsed);

                    if(count($parsed) > 0){
                    $asin = strtoupper($parsed[1]);
                    logger("asin => ".$asin );

                    if($asin) {

                    $accessKey = config('const.autoupdate_keepa_api_key');

                    $apiUrl = "https://api.keepa.com/tracking/?key=".$accessKey."&"."type=remove&asin=".$asin;

                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $apiUrl,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'GET',
                        CURLOPT_HTTPHEADER => array(
                            "Content-Type: application/json",
                        ),
                    ));

                    $response = curl_exec($curl);

                    logger("------------------------------------------------");
                    logger("Resposne ");
                    logger(json_encode($response));
                    logger("------------------------------------------------");

                    $response = json_decode($response);

                    $response_info[] = $response;

                    $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                    curl_close($curl);

                    $affected = DB::table('products')
                    ->where('id', $products->id)
                    ->update(['is_tracked' => 0, 'updateInterval' => 0]);

                    logger('KeepaCustomAPITrait :: ' . $affected);

                    // remove from main daily list
                    MainDailyListService::where('shopify_product_id', $products->id)->delete();
                    return true;
                  }
               }
            }

            logger("=============END::Traits :: remove Track Product====================");


        }catch(\Exception $e){
           logger("=============ERROR::Traits :: remove Track Product====================");
           logger($e);

        }
   }


   public function keepaRemoveAllProductTracking($user_id){

    try{

     logger("=============START :: Traits :: remove all Track Product====================");


     $products = Product::select('source_url','locale','id')
     ->where('locale','<>',"")
     ->where('is_tracked',1)
     ->orderBy('id', 'desc')
     ->where('user_id',$user_id)
     ->where('source',"Amazon")->get();


     logger("Total Product  => " . count($products));

     $accessKey = config('const.autoupdate_keepa_api_key');

     if(count($products) > 0){

        foreach($products as $key=>$value){

                 $source_url =  $value->source_url;
                 preg_match('/\/([a-zA-Z0-9]{10})/',$source_url,$parsed);

                 if(count($parsed) > 0){
                 $asin = strtoupper($parsed[1]);
                 logger("asin => ".$asin );

                 if($asin) {

                   $apiUrl = "https://api.keepa.com/tracking/?key=".$accessKey."&"."type=remove&asin=".$asin;

                   $curl = curl_init();

                   curl_setopt_array($curl, array(
                       CURLOPT_URL => $apiUrl,
                       CURLOPT_RETURNTRANSFER => true,
                       CURLOPT_ENCODING => '',
                       CURLOPT_MAXREDIRS => 10,
                       CURLOPT_TIMEOUT => 0,
                       CURLOPT_FOLLOWLOCATION => true,
                       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                       CURLOPT_CUSTOMREQUEST => 'GET',
                       CURLOPT_HTTPHEADER => array(
                           "Content-Type: application/json",
                       ),
                   ));

                   $response = curl_exec($curl);

                   logger("------------------------------------------------");
                   logger("Resposne ");
                   logger(json_encode($response));
                   logger("------------------------------------------------");

                   $response = json_decode($response);

                   $response_info[] = $response;

                   $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                   curl_close($curl);

                   DB::table('products')
                   ->where('id', $value->id)
                   ->update(['is_tracked' => 0, 'updateInterval' => 0]);

                   $deleted_products[] = ["id" => $value->id,"asin" => $asin];

                   $currentTokensLeft = $response->tokensLeft;

                  logger("currentTokensLeft => ". $currentTokensLeft);

                  if($currentTokensLeft <=0) {
                     logger("Delay the script execution for 1 seconds ");
                     logger("Get assets, Waiting 1 seconds for a credit");
                     sleep(1);
                   }
              }
           }
       }

       logger('======= get tokenstatus after aPI call =======');
      // $this->getKeepaTokenStatus();

      }

      return true;

         logger("=============END::Traits :: remove Track Product====================");

    }catch(\Exception $e){

       logger("=============ERROR::Traits :: remove Track Product====================");
       return Response::json([
           "error" => $e->getMessage()
       ],422);

    }
}


    public function getKeepaListNames()
    {

        try {
            logger("=============START :: Traits :: ListNames===================");

            $accessKey = config('const.autoupdate_keepa_api_key');

            $webhookURL = route('keepa.webhook');

            $apiUrl = "https://api.keepa.com/tracking/?key=".$accessKey."&type=listNames"; //Get Tracking

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $apiUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                ),
            ));

            $response = curl_exec($curl);

            //dd($response);
            return json_decode($response);

            logger("Res :: ListNames");
            logger(json_encode($response));


            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            logger('HTTP code: '.$httpcode);

            curl_close($curl);

            return $httpcode;

        } catch (Exception $e) {
            logger("=============ERROR :: Traits :: ListNames===================");
            return Response::json(["data" => $e], 422);
        }


    }

    public function keepaGetTrackProduct($asin)
    {

        try {
            logger("=============START :: Traits :: Track list===================");

            $accessKey = config('const.keepa_api_key');

            $webhookURL = route('keepa.webhook');

            $apiUrl = "https://api.keepa.com/tracking/?key=".$accessKey."&"."type=get&asin=".$asin; //Get Tracking

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $apiUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                ),
            ));

            $response = curl_exec($curl);

            return json_decode($response);

            logger("Res :: ListNames");
            logger(json_encode($response));


            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            logger('HTTP code: '.$httpcode);

            curl_close($curl);

            return $httpcode;

        } catch (Exception $e) {
            logger("=============ERROR :: Traits :: Track list===================");
            return Response::json(["data" => $e], 422);
        }


    }




}
