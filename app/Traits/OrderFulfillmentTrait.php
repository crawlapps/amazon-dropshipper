<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;

trait OrderFulfillmentTrait
{

    function getFullfilmentOrders($shop, $orderId)
    {
        $fullfilmentOrders = $shop->api()->rest('GET', '/admin/api/' . env('SHOPIFY_API_VERSION') . '/orders/' . $orderId . '/fulfillment_orders.json');
        logger('======== Order summery');
        logger(json_encode($fullfilmentOrders));
        // dd($fullfilmentOrders);
        return $fullfilmentOrders;
    }

    function fulfilledOrder($shop, $fullfilmentOrdersId)
    {
        $payload = [
            "fulfillment" => [
                "line_items_by_fulfillment_order" => [
                    [
                        "fulfillment_order_id" => $fullfilmentOrdersId
                    ]
                ]
            ]
        ];
        $fulfilmentReq = $shop->api()->rest('POST', '/admin/api/' . env('SHOPIFY_API_VERSION') . '/fulfillments.json', $payload);

        logger('======== Order summery');
        logger(json_encode($fulfilmentReq));
        // dd($fulfilmentReq);
        return $fulfilmentReq;
    }

    public  function fulfilment_service($shop, $name)
    {

        try{
            $fulfilments = [
                "fulfillment_service" => [
                    'name' => $name,
                    'callback_url' => env('APP_URL'),
                    'inventory_management' => true,
                    'tracking_support' => true,
                    'requires_shipping_method' => true,
                    'fulfillment_orders_opt_in' => true,
                    'format' => 'json',
                ],

            ];
            $fulfilment_api  = $shop->api()->rest('POST', '/admin/api/' . env('SHOPIFY_API_VERSION') . '/fulfillment_services', $fulfilments);

            return $fulfilment_api;
        }
        catch(\Exception $e){
            return $e;
        }


    }

    public function is_fulfilment_registered($shop)
    {

        $count = 0;

        $endPoint = $shop->api()->rest('GET', '/admin/api/2023-04/fulfillment_services.json');
        $collects = $endPoint['body']->container['fulfillment_services'];
        if ($collects !== [] && $collects != null) {
            foreach ($collects as $service) {
                if ($service['name'] == "zinc-fulfilment" ||  $service['name'] == "Amazone-fulfilment") {
                    $count = $count + 1;
                }
            }
            if ($count >= 2) {
                return response()->json(true);
            } else {
                return response()->json(false);
            }
        } else {
            return response()->json(false);
        }
    }
}
