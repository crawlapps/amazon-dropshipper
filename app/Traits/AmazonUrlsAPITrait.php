<?php

namespace App\Traits;

use App\Models\AmazonUrlsAPIAdds;
use App\Models\Counters;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\AppController;
use App\Mail\SendKeepTokenStatusMail;
use Keepa\KeepaAPI;
use Keepa\API\Request as KeepaAPIRequest;
use Keepa\API\ResponseStatus;
use Keepa\helper\CSVType;
use Keepa\helper\CSVTypeWrapper;
use Keepa\helper\ProductAnalyzer;
use App\MainDailyListService;
use App\PriceUpdateAudit;
use Illuminate\Support\Facades\Mail;

/**
 * Trait AutoUpdate.
 */
trait AmazonUrlsAPITrait
{
    /*
//    1) If  price is read successfully and Price is same as the price in the List then it must return back the value ´1´ )
//
//    2) If price is read successfully and Price is higher than price in list, then it must return back the price ´2´
//
//    3) If price is read and Price is lower than price in list, then it must return back the price ´-1´
//
//    4) If price is 0 or product is explicitly not available it must be returned to the service as  ´0´.
*/
    use SendEmailTrait;

    public function updatePrice($urlsAPIRes)
    {
        logger('====================== START:: updatePrice ======================');

        if ($urlsAPIRes) {

            $value_field = isset($urlsAPIRes['value']) ? $urlsAPIRes['value'] : 0;
            $product_id = isset($urlsAPIRes['product_id']) ? $urlsAPIRes['product_id'] : 0;

            $productUrls = MainDailyListService::where("product_id", $product_id)->first();
            if ($productUrls) {
                $product_db_id = $productUrls->shopify_product_id;
                $user_id = $productUrls->user_id;

                $product = DB::table('products')->where('id', $product_db_id)->first();

                if ($product) {

                    $shop = User::where('id', $user_id)->first();

                    if ($shop) {
                        logger("====urlsAPIRes====");
                        logger(json_encode($urlsAPIRes));

                        // if ($value_field !== 0) {

                        if ($value_field == -1 || $value_field == 2 || $value_field == 0) {
                            logger('Call keepa for product :: ' . $product_id);
                            $this->getLatestPriceFromKeepa($product_db_id);
                        }

                        // } else {

                        //If price is 0 or product is explicitly not available it must be returned to the service as  ´0´.

                        // $this->setVariantOutOfStock($user_id, $db_variant->shopify_id);
                        //}

                    } //Shop End

                    DB::table('products')
                        ->where('id', $product_db_id)
                        ->update(['updated_at' => date('Y-m-d H:i:s')]);
                }
            }
        }

        logger('====================== END:: updatePrice ======================');
    }

    public function getLatestPriceFromKeepa($productID, $keepa_notification_id = '')
    {

        try {

            logger('=======================================================');
            logger('========== START:: getLatestPriceFromKeepa =========');
            logger('=======================================================');

            $db_product =  DB::table('products')->where('id', $productID)->whereNull('deleted_at')->first();
            $source_url =  $db_product->source_url;

            $user_id = $db_product->user_id;

            $to_currency = $this->getShopCurrency($db_product->user_id);
            $from_currency = get_country_currency($db_product->locale);


            preg_match('/\/([a-zA-Z0-9]{10})/', $source_url, $parsed);
            $asin = strtoupper($parsed[1]);

            $apiKey = config('const.keepa_api_key');
            logger("========auto-update-keepa API key=========");
            logger($apiKey);

            $api = new KeepaAPI($apiKey);

            logger('======= get tokenstatus before aPI call =======');
            $tokenRes = $this->getKeepaTokenStatus();
            logger("=====tokenRes=====");

            $tokensLeft = $tokenRes->tokensLeft;
            logger("tokensLeft  ===> " . $tokensLeft);

            if ($tokensLeft > 0) {

                $r = KeepaAPIRequest::getProductRequest(
                    constant("Keepa\objects\AmazonLocale::$db_product->locale"),
                    0,
                    null,
                    null,
                    0,
                    true,
                    [$asin]
                );

                $response = $api->sendRequestWithRetry($r);
                $includeVariants = true;
                $apiVariants = [];
                $variants = [];
                $price = 0.00;

                if ($to_currency !== $from_currency) {
                    $latestFromRate = LatestExchangeRates('USD', $from_currency);
                    $latestToRate = LatestExchangeRates('USD', $to_currency);
                }

                $is_error = false;

                switch ($response->status) {
                    case ResponseStatus::OK:
                        // Check that at least one non-empty product is returned
                        if (
                            isset($response->products) && is_array($response->products) && count($response->products) > 0 &&
                            isset($response->products[0]->title) && $response->products[0]->title
                        ) {

                            $product = $response->products[0];

                            $lowestPrice = "AMAZON";
                            $amazonPriceHistory = ProductAnalyzer::getLast($product->csv[CSVType::AMAZON],
                            CSVTypeWrapper::getCSVTypeFromIndex(CSVType::AMAZON));

                            $newPriceHistory =  ProductAnalyzer::getLast($product->csv[CSVType::MARKET_NEW],
                            CSVTypeWrapper::getCSVTypeFromIndex(CSVType::MARKET_NEW));

                            if($amazonPriceHistory == -1 && $newPriceHistory != -1) {
                                $lowestPrice = "NEW";
                            } else if( $amazonPriceHistory > $newPriceHistory ) {
                                $lowestPrice = "NEW";
                            }


                            if($lowestPrice == "AMAZON") {
                                $currentAmazonPrice = $amazonPriceHistory;
                            } else {
                                $currentAmazonPrice = $newPriceHistory;
                                // $history_is_graph = false;
                            }

                            logger("=======> amazon history price");
                            logger($amazonPriceHistory);

                            logger("=======> New price");
                            logger($newPriceHistory);
                            // $currentAmazonPrice = ProductAnalyzer::getLast($product->csv[CSVType::AMAZON], CSVTypeWrapper::getCSVTypeFromIndex(CSVType::AMAZON));

                            // if ($currentAmazonPrice == -1) {
                            //     $currentAmazonPrice = ProductAnalyzer::getLast(
                            //         $product->csv[CSVType::MARKET_NEW],
                            //         CSVTypeWrapper::getCSVTypeFromIndex(CSVType::MARKET_NEW)
                            //     );
                            // }

                            if ($currentAmazonPrice != -1) {
                                logger("Inside the price update");
                                $price = number_format($currentAmazonPrice / 100, 2, '.', '');
                                logger("=======> Price :: $price");

                                logger("========> from currency :: $from_currency");
                                logger("========> to currency :: $to_currency");
                                $source_price_amazon = $price;
                                //                           $price =  $appC->calculateCurrency($from_currency, $to_currency, $price);
                                $price  = ($from_currency == $to_currency) ? round($price, 2) : round((($price * $latestToRate) / $latestFromRate), 2);
                                logger("price:: $price");
                            }

                            // Add variants [extra call to API]
                            if ($includeVariants && $product->variations) {
                                foreach ($product->variations as $variant) {
                                    $apiVariants[] = $variant->asin;
                                    $variantAttributes[$variant->asin] = $variant->attributes;
                                }
                            }


                            $auditData = [
                                "keepa_notification_id" => $keepa_notification_id,
                                "update_type" => "AUTO_UPDATE"
                            ];

                            logger("============> Current Amazon Price:: $currentAmazonPrice");
                            logger("============> Price:: $price");

                            $this->updatePriceAmazon($productID, [], $user_id, $price, 'amazon', $db_product->auto_update, $source_price_amazon, $auditData, $lowestPrice);
                        } else {
                            $is_error = true;
                            logger('Could not fetch product. Please double-check your entry or try a different URL.');
                        }

                        break;
                    default:
                        if ($response->error && $response->error->message && $response->error->message) {
                            logger($response->error->message);
                        }
                }

                // TODO: Re-factor to use single API call function for main product & variants
                if (count($apiVariants)) {
                    if (count($apiVariants) > 100) {
                        $apiVariants = array_slice($apiVariants, 0, config('const.max_variants_num'));
                    }


                    $tokenRes = $this->getKeepaTokenStatus();
                    logger("=====tokenRes=====");

                    $tokensLeft = $tokenRes->tokensLeft;
                    logger("Match Token left");
                    logger("Total variants => " . count($apiVariants));
                    logger("tokensLeft => " . $tokensLeft);

                    if ($tokensLeft < count($apiVariants)) {
                        logger("Delay the script execution for 1 seconds ");
                        logger("Get assets, Waiting 1 seconds for a credit");
                        sleep(1);
                    }

                    if ($tokensLeft > 0) {
                        $r = KeepaAPIRequest::getProductRequest(constant("Keepa\objects\AmazonLocale::$db_product->locale"), 0, null, null, 0, true, $apiVariants);

                        $response = $api->sendRequestWithRetry($r);

                        switch ($response->status) {
                            case ResponseStatus::OK:

                                // Check that at least one non-empty product is returned
                                if (
                                    isset($response->products) && is_array($response->products) && count($response->products) > 0 &&
                                    isset($response->products[0]->title) && $response->products[0]->title
                                ) {
                                    for ($i = 0; $i < min(config('const.max_variants_num'), count($response->products)); $i++) {
                                        $product = $response->products[$i];


                                        $lowestPrice = "AMAZON";
                                        $amazonPriceHistory = ProductAnalyzer::getLast($product->csv[CSVType::AMAZON],
                                        CSVTypeWrapper::getCSVTypeFromIndex(CSVType::AMAZON));

                                        $newPriceHistory =  ProductAnalyzer::getLast($product->csv[CSVType::MARKET_NEW],
                                        CSVTypeWrapper::getCSVTypeFromIndex(CSVType::MARKET_NEW));

                                        if($amazonPriceHistory == -1 && $newPriceHistory != -1) {
                                            $lowestPrice = "NEW";
                                        } else if( $amazonPriceHistory > $newPriceHistory ) {
                                            $lowestPrice = "NEW";
                                        }


                                        if($lowestPrice == "AMAZON") {
                                            $currentAmazonPrice = $amazonPriceHistory;
                                        } else {
                                            $currentAmazonPrice = $newPriceHistory;
                                            // $history_is_graph = false;
                                        }

                                        // $currentAmazonPrice = ProductAnalyzer::getLast(
                                        //     $product->csv[CSVType::AMAZON],
                                        //     CSVTypeWrapper::getCSVTypeFromIndex(CSVType::AMAZON)
                                        // );
                                        // if ($currentAmazonPrice == -1) {
                                        //     $currentAmazonPrice = ProductAnalyzer::getLast(
                                        //         $product->csv[CSVType::MARKET_NEW],
                                        //         CSVTypeWrapper::getCSVTypeFromIndex(CSVType::MARKET_NEW)
                                        //     );
                                        // }

                                        $price = number_format(
                                            $currentAmazonPrice / 100,
                                            2,
                                            '.',
                                            ''
                                        );

                                        //                               $variants[$i]['source_price'] = $appC->calculateCurrency($from_currency, $to_currency, $price);

                                        $variants[$i]['source_price_amazon'] = $price;
                                        $variants[$i]['source_price']  = ($from_currency == $to_currency) ? round($price, 2) : round((($price * $latestToRate) / $latestFromRate), 2);
                                        $variants[$i]['source_id'] = $product->asin;
                                        $variants[$i]['lowest_price'] = $lowestPrice;

                                    }
                                }

                            default:
                                if ($response->error && $response->error->message && $response->error->message) {
                                    $is_error = true;
                                    $errors[] = $response->error->message;
                                }
                        }
                    }
                }

                // get tokenstatus after aPI call
                logger('======= get tokenstatus after aPI call =======');
                $this->getKeepaTokenStatus();

                if (!$is_error) {
                    $auditData = [
                        "keepa_notification_id" => $keepa_notification_id,
                        "update_type" => "AUTO_UPDATE"
                    ];

                    // update price
                    logger('Call updatePriceAmazon :: ' . $productID);
                    $this->updatePriceAmazon($productID, $variants, $user_id, $price, 'amazon', $db_product->auto_update, $source_price_amazon, $auditData);

                    // update counter
                    //               $this->updateCounter($db_product->affiliate, $user_id, count($variants));
                } else {
                    logger('Error for product :: ' . $productID);
                    logger(json_encode($errors));
                }
            }
            logger('========== END:: getLatestPriceFromKeepa =========');
        } catch (\Exception $e) {

            logger('========== ERROR:: getLatestPriceFromKeepa =========');
            logger(json_encode($e->getMessage()));
        }
    }

    public function calculatePrice($old_source_price, $old_shopify_price, $new_source_price)
    {

        //old method
        // $difference = $old_shopify_price - $old_source_price;
        // $new_shopify_price = ( $difference != 0 ) ? ($new_source_price + $difference) : $new_source_price;

        // new method

        if ($old_shopify_price != $old_source_price  && $old_source_price > 0) {

            $percentage = number_format((($old_shopify_price - $old_source_price) / $old_source_price * 100), 2);

            $percentage = str_replace(',', '', $percentage);

            logger("percentage " . $percentage);
            logger("new_source_price " . $new_source_price);


            $added_value = ($new_source_price / 100) * $percentage;
            logger($added_value);
            $new_shopify_price = number_format(($new_source_price +  $added_value),  2);

            $new_shopify_price = str_replace(',', '', $new_shopify_price);

            $new_shopify_price = ($new_shopify_price >= $new_source_price) ? $new_shopify_price : $new_source_price;
        } else {
            $new_shopify_price = $new_source_price;
        }


        return $new_shopify_price;
    }

    public function setVariantOutOfStock($user_id, $variantId)
    {
        try {
            logger('====================== START:: setVariantOutOfStock ======================');
            $user = User::find($user_id);
            $inventory_id = $this->getVariantInventoryItemId($user_id, $variantId);

            logger("inventory_id");
            logger(json_encode($inventory_id));


            if ($inventory_id != '') {
                $inventoryLevels = $this->getVariantInventoryLevels($user_id, $inventory_id);
                if (!empty($inventoryLevels)) {
                    foreach ($inventoryLevels as $ikey => $ival) {
                        $this->setInventoryManagement($user_id, $variantId, 'shopify');

                        $endPoint = '/admin/api/' . env('SHOPIFY_API_VERSION') . '/inventory_levels/set.json';

                        $set = [
                            'location_id' => $ival->location_id,
                            'inventory_item_id' => $ival->inventory_item_id,
                            'available' => 0,
                        ];

                        $sh_set = $user->api()->rest('POST', $endPoint, $set);

                        logger("sh_set");
                        logger(json_encode($sh_set));

                        DB::table('variants')->where('shopify_id', '=', $variantId)->update([
                            'inventory' => 0,
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);
                    }
                }
            } else {
                logger('====================== Inventory id not found ======================');
            }

            logger('====================== END:: setVariantOutOfStock ======================');
        } catch (\Exception $e) {
            logger('====================== ERROR:: setVariantOutOfStock ======================');
            logger($e);
        }
    }

    public function setInventoryManagement($user_id, $variantId, $type)
    {
        try {
            logger('====================== START:: setInventoryManagement ======================');
            $user = User::find($user_id);
            $modVariant = [
                'variant' => [
                    'id' => $variantId,
                    'inventory_management' => $type
                ]
            ];
            $variantReq = $user->api()->rest(
                'PUT',
                '/admin/api/' . env('SHOPIFY_API_VERSION') . '/variants/' . $variantId . '.json',
                $modVariant
            );

            if (!$variantReq['errors']) {
                $get_asset_call_limit = collect($variantReq['response']->getHeader('x-shopify-shop-api-call-limit'))->first();



                if ($get_asset_call_limit == '39/40') {

                    logger("Delay the script execution for 1 seconds ");
                    logger("Get assets, Waiting 1 seconds for a credit");
                    sleep(1);
                }
            }



            logger('====================== END:: setInventoryManagement ======================');
        } catch (\Exception $e) {
            logger('====================== ERROR:: setInventoryManagement ======================');
            logger($e);
        }
    }

    public function getVariantInventoryItemId($user_id, $variantId)
    {
        try {
            logger('====================== START:: getVariantInventoryItemId ======================');
            $user = User::find($user_id);
            $inventory_id = '';
            if ($user) {
                $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/variants/' . $variantId . '.json';
                $parameter['fields'] = 'id,inventory_item_id';
                $sh_variant = $user->api()->rest('GET', $endPoint, $parameter);

                logger("sh_variant");
                logger(json_encode($sh_variant));

                if (!$sh_variant->errors) {
                    $inventory_id = (@$sh_variant->body->variant->inventory_item_id) ? $sh_variant->body->variant->inventory_item_id : '';
                } else {
                    // logger(json_encode($sh_variant));
                }
            } else {
                logger('====================== INTERNAL ERROR:: User ' . $user_id . ' not found... ======================');
            }

            logger('====================== END:: getVariantInventoryItemId ======================');
            return $inventory_id;
        } catch (\Exception $e) {
            logger('====================== ERROR:: getVariantInventoryItemId ======================');
            logger($e);
        }
    }

    public function getVariantInventoryLevels($user_id, $inventory_id)
    {
        try {
            logger('====================== START:: getVariantInventoryLevels ======================');
            $user = User::find($user_id);
            if ($user) {
                $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/inventory_levels.json';
                $parameter['inventory_item_ids'] = $inventory_id;
                $sh_inventory_level = $user->api()->rest('GET', $endPoint, $parameter);

                logger("sh_inventory_level");
                logger(json_encode($sh_inventory_level));


                if (!$sh_inventory_level->errors) {
                    return $sh_inventory_level->body->inventory_levels;
                } else {
                    logger(json_encode($sh_inventory_level));
                }
            } else {
                logger('====================== INTERNAL ERROR:: User ' . $user_id . ' not found... ======================');
            }

            logger('====================== END:: getVariantInventoryLevels ======================');
            return [];
        } catch (\Exception $e) {
            logger('====================== ERROR:: getVariantInventoryLevels ======================');
            logger($e);
        }
    }

    public function getShopCurrency($user_id)
    {

        logger('========== START:: getShopCurrency =========');

        $shop = User::find($user_id);
        $parameter['fields'] = 'currency';
        $shop_result = $shop->api()->rest('GET', 'admin/api/' . env('SHOPIFY_API_VERSION') . '/shop.json', $parameter);

        if (!$shop_result['errors']) {
            $get_asset_call_limit = collect($shop_result['response']->getHeader('x-shopify-shop-api-call-limit'))->first();

            logger("get_asset_call_limit " . $get_asset_call_limit);

            if ($get_asset_call_limit == '39/40') {

                logger("Delay the script execution for 1 seconds ");
                logger("Get assets, Waiting 1 seconds for a credit");
                sleep(1);
            }
        }
        logger(json_encode($shop_result));
        return (!$shop_result['errors']) ? $shop_result['body']['shop']['currency'] : '';
    }

    public function updatePriceAmazon($productID, $variants, $user_id, $price, $source, $autoUpdate, $source_price_amazon = 0, $auditData = [], $lowest_price = null)
    {
        logger('=======================================================');
        logger('====================== START:: updatePriceAmazon ======================');
        logger('=======================================================');

        $shop = User::where('id', $user_id)->first();

        logger("variants");
        logger(json_encode($variants));

        logger("price");
        logger($price);

        logger('=======================================================');

        $autoUpdate = json_decode($autoUpdate);
        $saveShopify = (@$autoUpdate->save_shopify == 1) ? true : false;

        if (count($variants) > 0) {
            logger('Update price for multiple variant :: ' . $productID);
            foreach ($variants as $vkey => $vval) {

                $db_variant = DB::table('variants')->where('product_id', $productID)->where(
                    'source_id',
                    $vval['source_id']
                )->first();

                if ($vval['source_price'] > 0) {
                    if ($db_variant) {
                        logger('Variant_id :: ' . $db_variant->id);
                        $newPrice = $this->calculatePrice(
                            $db_variant->source_price,
                            $db_variant->shopify_price,
                            $vval['source_price']
                        );

                        logger('=======================================================');
                        logger("calculatePrice new price");
                        logger($newPrice);

                        $affected = MainDailyListService::where("shopify_product_id", $productID)->update([
                            "amazon_new_price" => $vval['source_price_amazon']
                        ]);

                        logger('Affected amazonurl for multiple :: ' . $db_variant->id . ' :: ' . $affected);

                        $modVariant = [
                            'variant' => [
                                'id' => $db_variant->shopify_id,
                                'price' => $newPrice,
                                'inventory_management' => null
                            ]
                        ];

                        $affected = DB::table('variants')->where([
                            ['product_id', '=', $productID],
                            ['source_id', '=', $vval['source_id']]
                        ])->update([
                            'source_price_amazon' => $vval['source_price_amazon'],
                            'source_price' => $vval['source_price'], 'shopify_price' => $newPrice,
                            'updated_at' => date('Y-m-d H:i:s'),
                            'lowest_price' => $vval['lowest_price']
                        ]);

                        // Save audit data
                        $auditData['product_id'] = $productID;
                        $auditData['user_id'] = $user_id;
                        $auditData['asin'] = $vval['source_id'];
                        $auditData['old_amazon_price'] = $db_variant->source_price;
                        $auditData['new_amazon_price'] = $vval['source_price'];
                        $auditData['old_store_price'] = $db_variant->shopify_price;
                        $auditData['new_store_price'] = $newPrice;

                        PriceUpdateAudit::create($auditData);

                        logger('Affected variant for multiple :: ' . $db_variant->id . ' :: ' . $affected);
                        $variantReq = $shop->api()->rest(
                            'PUT',
                            '/admin/api/' . env('SHOPIFY_API_VERSION') . '/variants/' . $db_variant->shopify_id . '.json',
                            $modVariant
                        );

                        if (!$variantReq['errors']) {
                            $get_asset_call_limit = collect($variantReq['response']->getHeader('x-shopify-shop-api-call-limit'))->first();
                            if ($get_asset_call_limit == '39/40') {
                                logger("Delay the script execution for 1 seconds ");
                                logger("Get assets, Waiting 1 seconds for a credit");
                                sleep(1);
                            }
                            $inventoryItem =  [
                                'id' => $variantReq['body']['variant']['inventory_item_id'],
                                'cost' => $vval['source_price']
                            ];
                            $inventoryReq = $shop->api()->rest('PUT', '/admin/api/'. env('SHOPIFY_API_VERSION') .'/inventory_items/'. $variantReq['body']['variant']['inventory_item_id'] .'.json',
                            ['inventory_item' => $inventoryItem]);
                        }
                    }
                } else {
                    // Save audit data for out of stock variants
                    if($db_variant)  {
                        $auditData['product_id'] = $productID;
                        $auditData['user_id'] = $user_id;
                        $auditData['asin'] = $vval['source_id'];
                        $auditData['old_amazon_price'] = $db_variant->source_price;
                        $auditData['new_amazon_price'] = 0;
                        $auditData['old_store_price'] = $db_variant->shopify_price;
                        $auditData['new_store_price'] = 0;
                        PriceUpdateAudit::create($auditData);
                    }

                    MainDailyListService::where("shopify_product_id", $productID)->update([
                        "amazon_new_price" => $vval['source_price_amazon']
                    ]);
                }
            }
        } else {
            logger('Update price for variant :: ' . $productID);
            $db_variant = DB::table('variants')->where('product_id', $productID)->first();
            logger(json_encode($db_variant));
            if ($price > 0) {
                if ($db_variant) {

                    logger('DB variant updates :: ' . $db_variant->id);
                    $newPrice = $this->calculatePrice($db_variant->source_price, $db_variant->shopify_price, $price);

                    logger('=======================================================');
                    logger("if no any varaints");
                    logger("calculatePrice new price");
                    logger($newPrice);

                    $modVariant = [
                        'variant' => [
                            'id' => $db_variant->shopify_id,
                            'price' => $newPrice,
                            'inventory_management' => null
                        ]
                    ];

                    $affected = DB::table('variants')->where('product_id', $productID)->update([
                        'source_price' => $price,
                        'shopify_price' => $newPrice,
                        'source_price_amazon' => $source_price_amazon,
                        'lowest_price' => $lowest_price,
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);

                    // Save audit data
                    $auditData['product_id'] = $productID;
                    $auditData['user_id'] = $user_id;
                    $auditData['asin'] = $db_variant->source_id;
                    $auditData['old_amazon_price'] = $db_variant->source_price;
                    $auditData['new_amazon_price'] = $price;
                    $auditData['old_store_price'] = $db_variant->shopify_price;
                    $auditData['new_store_price'] = $newPrice;

                    PriceUpdateAudit::create($auditData);


                    logger('Affected amazonurl for single :: ' . $db_variant->id . ' :: ' . $affected);

                    $affected = MainDailyListService::where("shopify_product_id", $productID)->update([
                        "amazon_new_price" => $source_price_amazon
                    ]);

                    logger('Affected amazonurl for single :: ' . $db_variant->id . ' :: ' . $affected);
                    $variantReq = $shop->api()->rest(
                        'PUT',
                        '/admin/api/' . env('SHOPIFY_API_VERSION') . '/variants/' . $db_variant->shopify_id . '.json',
                        $modVariant
                    );

                    if (!$variantReq['errors']) {

                        $get_asset_call_limit = collect($variantReq['response']->getHeader('x-shopify-shop-api-call-limit'))->first();

                        if ($get_asset_call_limit == '39/40') {

                            logger("Delay the script execution for 1 seconds ");
                            logger("Get assets, Waiting 1 seconds for a credit");
                            sleep(1);
                        }
                    }
                }
            } else {
                // Save audit data for out of stock variants
                if($db_variant)  {
                    $auditData['product_id'] = $productID;
                    $auditData['user_id'] = $user_id;
                    $auditData['asin'] = $db_variant->source_id;
                    $auditData['old_amazon_price'] = $db_variant->source_price;
                    $auditData['new_amazon_price'] = 0;
                    $auditData['old_store_price'] = $db_variant->shopify_price;
                    $auditData['new_store_price'] = 0;

                    PriceUpdateAudit::create($auditData);
                }

                MainDailyListService::where("shopify_product_id", $productID)->update([
                    "amazon_new_price" => $source_price_amazon
                ]);
            }
        }

        // Send Update price email
        // $this->sendUpdatePriceEmail($user_id, $productID);

        DB::table('products')
            ->where('id', $productID)
            ->update(['updated_at' => date('Y-m-d H:i:s'), 'keepa_updated_at' => date('Y-m-d H:i:s')]);

        logger('====================== END:: updatePriceAmazon ======================');
    }

    public function updateCounter($affiliate, $user_id, $variantCount)
    {
        logger('====================== START:: updateCounter ======================');

        logger('===== Auto update Counter :: Shop ID :: ' . $user_id . ' ========');

        $charge = DB::table('charges')->where('user_id', $user_id)->where('status', "ACTIVE")->orderBy('created_at', 'desc')->first();
        $counter = Counters::where('user_id', $user_id)->where('charge_id', $charge->id)->where('status', 'active')->first();

        $affiliate = json_decode($affiliate);
        if ($affiliate->status) {
            $counter->auto_update_affiliate = $counter->auto_update_affiliate + 1;
        } else {
            $counter->auto_update_count = $counter->auto_update_count + 1;
        }

        $counter->save();


        logger('====================== END:: updateCounter ======================');
    }

    public function getKeepaTokenStatus()
    {
        try {
            $apiKey = config('const.keepa_api_key');
            $api = new KeepaAPI($apiKey);

            logger("========getKeepaTokenStatus :: auto-update-keepa API key=========");
            logger($apiKey);

            $r = KeepaAPIRequest::getTokenStatusRequest();
            $response = $api->sendRequestWithRetry($r);

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $response->url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            logger($response);

            $res = json_decode($response);

            // Trigger mail to the admin if token flow reduction is greater
            if($res->tokenFlowReduction >= 18) {
                sendFlowReductionMail($apiKey, $res);
            }

            return $res;
        } catch (\Exception $e) {
            logger('=============== ERROR:: getKeepaTokenStatus ===============');
            logger($e);
        }
    }
}
