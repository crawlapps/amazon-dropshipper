<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Traits\OrderFulfillmentTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{

    use OrderFulfillmentTrait;
    /**
     * Show Orders page
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // logger($request->all());
        $store = Auth::user()->name;
        $ids = [];
        if($request->has('id')) {
            array_push($ids, $request->get('id'));
        } else if($request->has('ids')) {

            $ids = $request->get('ids');
        }
        logger('====== Get orders ids');
        logger($request->get('ids'));

        return view('orders',compact('store','ids'));
    }

    public function getOrders(){
        try{
            $shop = Auth::user();
            $shopURL = 'https://' . $shop->getDomain()->toNative();

            $data['orders'] = Order::with('LineItems')->where('user_id', $shop->id)->orderBy('id', 'desc')->get();
            $data['shop']['url'] = $shopURL;
            return ['success' => true, 'data' => $data];
        }catch( \Exception $e ){
            return [ 'success' => false, 'errors' => $e->getMessage()];
        }
    }
    public function updateFulillOrderStatus(Request $request, $id) {
        try {
            // dd($request->all());
            logger("================== Start:: Order FulfilOrder Function ===============");
            logger($id);
            $shop = Auth::user();

            $fulfillmentOrdersRes =  $this->getFullfilmentOrders($shop, $id);

            if(!$fulfillmentOrdersRes['errors']) {
                if(count($fulfillmentOrdersRes['body']['fulfillment_orders']) > 0) {
                    foreach ($fulfillmentOrdersRes['body']['fulfillment_orders'] as $key => $fulfilmentOrder) {
                        $fulfilOrderRes = $this->fulfilledOrder($shop, $fulfilmentOrder['id']);
                        if(!$fulfilOrderRes['errors']) {
                            return ['success' => true, 'data' => []];
                        } else {
                            logger('=========> Error while fulfill order');
                            logger(json_encode($fulfilOrderRes));
                        }
                    }
                }
            } else if($fulfillmentOrdersRes['errors'] && $fulfillmentOrdersRes['status'] == 422 && str_contains($fulfillmentOrdersRes['body'][0], 'unfulfillable status= closed')) {
                $order = Order::where('shopify_order_id', $id)->first();
                $order->fulfillment_status = 'fulfilled';
                $order->save();
            } else {
                logger('=========> Error while fulfilment orders call...');
                logger(json_encode($fulfillmentOrdersRes));
            }
            return ['success' => false, 'msg' => "Error while fulfil order"];
        } catch (\Throwable $e) {
            return [ 'success' => false, 'errors' => $e->getMessage()];
        }
    }
}
