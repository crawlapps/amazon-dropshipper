<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Counters;
use App\Models\RainForestCounter;
use Illuminate\Http\Request;

class CountersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(Request $request, $page)
    {
        // $counters = RainForestCounter::paginate($page);
        $counters = RainForestCounter::where('created_at', '>', now()->subDays(30)->endOfDay())->get();
        return view('admin.counters.index' , compact('counters'));
    }

}
