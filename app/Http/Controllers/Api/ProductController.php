<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Product;
use App\User;
use App\KeepaTrackingProductsResponse;
use Exception;
use Illuminate\Http\Request;
use Response;
use App\Traits\KeepaCustomAPITrait;
use Facade\FlareClient\Http\Response as HttpResponse;
use DB;
use Keepa\objects\AmazonLocale;
use Keepa\helper\CSVType;
use Keepa\helper\CSVTypeWrapper;
use Keepa\helper\ProductAnalyzer;
use Keepa\KeepaAPI;
use Keepa\API\Request as KeepaAPIRequest;
use App\Jobs\KeepaTrackingAddProductsScriptJob;
use App\Jobs\KeepaTrackingWebhookAutoUpdatePriceJob;
use App\Mail\SendKeepTokenStatusMail;
use Illuminate\Support\Facades\Mail;

class ProductController extends Controller
{
    use KeepaCustomAPITrait;

   public  function checkAffiliate(Request $request){
       try{
           $shop_domain = $request->shop;
           $is_review = $request->is_review_class;

           if ($shop_domain) {
               $user = User::where('name', $shop_domain)->first();
               if ($user) {

                   $product = Product::where('user_id', $user->id)->where('shopify_handle', $request->handle)->first();

                   $reviews = [];
                   if ($product) {
                       $data['is_product'] = true;
                       $buttonstyle = json_decode($product->affiliate);
                       if ($buttonstyle->status) {
                           $data['is_product'] = true;
                           $data['redirect'] = $product->source_url;
                           $data['show_data'] = $buttonstyle->button_text;
                           $data['style'] = $buttonstyle->button_style;
                       } else {
                           $data['is_product'] = false;
                       }

                       if ($is_review) {
                           $db_reviews = (array) json_decode($product->reviews);
                           $reviews = $db_reviews;
                       }
                   } else {
                       $data['is_product'] = false;
                   }

                   return ($is_review && isset($reviews['is_show_front']) && $reviews['is_show_front']==true ) ?
                       Response::json([
                           'review' => \View::make('review', ["reviews" => $reviews])->render(), "data" => $data
                       ], 200) :
                       Response::json(["data" => $data], 200);
               }else{
                   $data['is_product'] = false;
                   return  Response::json(["data" => $data], 200);
               }
           }
       }catch (\Exception $e){
           return Response::json(["data" => $e], 422);
       }
   }





        public function keepaTrackProductWebhookSet(Request $request){

           try{

               logger("=============START :: Webhook :: SET Keepa Track Product===================");

              $input = $request->all();

              logger("Input");
              logger(json_encode($input));

               $res = $this->keepaSetWebhook();

               return  Response::json(['success' => true,"data" => $res, "message" => "Webhook set successfully." ], 200);

              logger("=============END :: Webhook ::SET Keepa Track Product===================");

           }catch(\Exception $e){
               logger("=============ERROR :: Webhook ::SET Keepa Track Product===================");
               return Response::json(["data" => $e],422);
           }


        }

    public function keepaTrackProductWebhook(Request $request){

        try{

            logger("=============START :: Webhook :: Keepa Track Product===================");

            $input = $request->all();

            logger("Input");
            logger(json_encode($input));

            $asin = $input['asin'];
            logger("asin => ".$asin);

            $currentPrices = $input['currentPrices'];

            $metaData = $input['metaData'];

            $currentAmazonPrice = $currentPrices[0];
            $price = 0;
            if ($currentAmazonPrice != -1) {
                $price = number_format($currentAmazonPrice / 100, 2, '.', '');
//                           $price =  $appC->calculateCurrency($from_currency, $to_currency, $price);

            }

            $trackedResDB = new KeepaTrackingProductsResponse;
            $trackedResDB->product_id = $metaData;
            $trackedResDB->asin = $asin;
            $trackedResDB->source_url = '';
            $trackedResDB->webhook_response = $input;
            $trackedResDB->save();

            $product_id = $metaData;
            $keepa_notifiaction_id = $trackedResDB->id;

            KeepaTrackingWebhookAutoUpdatePriceJob::dispatch($product_id, $asin, $keepa_notifiaction_id);

            return  Response::json(['success' => true, "price"=>$price,  "message" => "Keepa Tracking Products Response Saved Successfully." ], 200);

            logger("=============END :: Webhook :: Keepa Track Product===================");

        }catch(\Exception $e){
            logger("=============ERROR :: Webhook :: Keepa Track Product===================");
            logger($e);
            return Response::json(["data" => $e->getMessage()],422);
        }


    }

    public function keepaListNames(Request $request){

        try{

            logger("=============START :: Keepa Named Lists===================");


             $listReq = $this->getKeepaListNames();

             logger(json_encode($listReq));


            return  Response::json(['success' => true, "data" => $listReq ,"message" => "Get Named Lists
 successfully." ], 200);

            logger("=============END ::  Keepa Named Lists===================");

        }catch(\Exception $e){
            logger("=============ERROR ::  Keepa Named Lists===================");
            return Response::json(["data" => $e],422);
        }

    }


    public function addProductTracking(Request $request){

        try{

            logger("=============START :: Keepa addProductTracking===================");

            $asin = $request->asin;
            $locale = $request->locale;

            if(!$locale) {
                return Response::json(['success' => false, "message" => "product Locale(US,IN etc.) required."], 200);
            }

            if($asin) {

                $mainDomainId = constant("Keepa\objects\AmazonLocale::$locale");

                $updateInterval = 1;

                $accessKey = config('const.keepa_api_key');

                $thresholdValues = [
                    [
                        "thresholdValue" => 999999999,
                        "domain" => $mainDomainId,
                        "csvType" => 0,
                        "isDrop" => true
                    ],
                    // track any price increases
                    [
                        "thresholdValue" => 0,
                        "domain" => $mainDomainId,
                        "csvType" => 0,
                        "isDrop" => false
                    ],
                    [
                        "thresholdValue" => 999999999,
                        "domain" => $mainDomainId,
                        "csvType" => 1,
                        "isDrop" => true
                    ],
                    // track any price increases
                    [
                        "thresholdValue" => 0,
                        "domain" => $mainDomainId,
                        "csvType" => 1,
                        "isDrop" => false
                    ],
                ];

                $params = [
                    "asin" => $asin,
                    "mainDomainId" => $mainDomainId,
                    "ttl" => 0,
                    "expireNotify" => true,
                    "desiredPricesInMainCurrency" => false,
                    "updateInterval" => $updateInterval,
                    "metaData" => "Test Product Tracking",
                    "thresholdValues" => $thresholdValues,

                    // "notifyIf" => [],
                    "notificationType" => [ true, true, true, true, true, true, true ],
                    "individualNotificationInterval" => 5
                ];

                $apiUrl = "https://api.keepa.com/tracking/?key=".$accessKey."&"."type=add";

                $query_param = json_encode($params, true);

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $apiUrl,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_POSTFIELDS => $query_param,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_HTTPHEADER => array(
                        "Content-Type: application/json",
                    ),
                ));

                $response = curl_exec($curl);

                $response = json_decode($response);

                $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                curl_close($curl);

                return Response::json(['success' => true,"data" => $response, "message" => "Product Tracking asin successfully."], 200);

            }else{

                return Response::json(['success' => false, "message" => "product ASIN required."], 200);
            }

            logger("=============END ::  Keepa addProductTracking===================");

        }catch(\Exception $e){
            logger("=============ERROR ::  Keepa addProductTracking===================");
            return Response::json(["data" => $e],422);
        }


    }


    public function getProductTracking(Request $request){

        try{

            logger("=============START :: Keepa getProductTracking===================");

            $asin = $request->asin;

              if($asin) {
                  $listReq = $this->keepaGetTrackProduct($asin);

                  logger(json_encode($listReq));

                  return Response::json(['success' => true,"data" => $listReq, "message" => "Get Tracking
 by asin successfully."], 200);
              }

            logger("=============END ::  Keepa getProductTracking===================");

        }catch(\Exception $e){
            logger("=============ERROR ::  Keepa getProductTracking===================");
            return Response::json(["data" => $e],422);
        }

    }


    public function addProductTrackingScript(Request $request){

         try{

          logger("=============START::Keepa Add bulk produc trackingt====================");

             KeepaTrackingAddProductsScriptJob::dispatch();

           return Response::json(['success' => true, "message" => "Products are being added on Tracking list in the background."], 200);


              logger("=============END::Keepa Add bulk produc trackingt====================");

         }catch(\Exception $e){

            logger("=============ERROR::Keepa Add bulk produc trackingt====================");
            return Response::json([
                "error" => $e->getMessage()
            ],422);

         }
    }

    public function removeProductTrackingScript(Request $request){

        try{

         logger("=============START::Keepa Remove bulk produc trackingt====================");

         $products = Product::select('source_url','locale','id')
         ->where('locale','<>',"")
         ->where('is_tracked',1)
         ->orderBy('id', 'desc')
         ->where('source',"Amazon")->limit(20)->get();

         logger('======= get tokenstatus before aPI call =======');

         $tokenRes = $this->getKeepaTokenStatus();

         $previewsTokensLeft = $tokenRes->tokensLeft;

         logger("Total Product  => " .count($products));

          $response_info = [];
          $deleted_products = [];

          if(count($products) > 0){

            foreach($products as $key=>$value){

                     $source_url =  $value->source_url;
                     preg_match('/\/([a-zA-Z0-9]{10})/',$source_url,$parsed);

                     if(count($parsed) > 0){
                     $asin = strtoupper($parsed[1]);
                     logger("asin => ".$asin );

                     if($asin) {

                      $accessKey = config('const.autoupdate_keepa_api_key');


                       $apiUrl = "https://api.keepa.com/tracking/?key=".$accessKey."&"."type=remove&asin=".$asin;

                       $curl = curl_init();

                       curl_setopt_array($curl, array(
                           CURLOPT_URL => $apiUrl,
                           CURLOPT_RETURNTRANSFER => true,
                           CURLOPT_ENCODING => '',
                           CURLOPT_MAXREDIRS => 10,
                           CURLOPT_TIMEOUT => 0,
                           CURLOPT_FOLLOWLOCATION => true,
                           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                           CURLOPT_CUSTOMREQUEST => 'GET',
                           CURLOPT_HTTPHEADER => array(
                               "Content-Type: application/json",
                           ),
                       ));

                       $response = curl_exec($curl);

                       logger("------------------------------------------------");
                       logger("Resposne ");
                       logger(json_encode($response));
                       logger("------------------------------------------------");

                       $response = json_decode($response);

                       $response_info[] = $response;

                       $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                       curl_close($curl);

                       DB::table('products')
                       ->where('id', $value->id)
                       ->update(['is_tracked' => 0, 'updateInterval' => 0]);

                       $deleted_products[] = ["id" => $value->id,"asin" => $asin];

                       $currentTokensLeft = $response->tokensLeft;

                      logger("currentTokensLeft => ". $currentTokensLeft);

                      if($currentTokensLeft <=0) {
                         logger("Delay the script execution for 1 seconds ");
                         logger("Get assets, Waiting 1 seconds for a credit");
                         sleep(1);
                       }

                  }
               }

           }


           logger('======= get tokenstatus after aPI call =======');
          // $this->getKeepaTokenStatus();

          }

          return Response::json(['success' => true,"data" => $response_info, "deleted_products"=>$deleted_products, "message" => "Remove Product Tracking asin successfully."], 200);


             logger("=============END::Keepa Remove bulk produc trackingt====================");

        }catch(\Exception $e){

           logger("=============ERROR::Keepa Remove bulk produc trackingt====================");
           return Response::json([
               "error" => $e->getMessage()
           ],422);

        }
   }



   public function removeAllProductTrackingScript(Request $request){

    try{

     logger("=============START::Keepa Remove All  bulk produc trackingt====================");


     logger('======= get tokenstatus before aPI call =======');

     $tokenRes = $this->getKeepaTokenStatus();

     $previewsTokensLeft = $tokenRes->tokensLeft;

     logger('======= previewsTokensLeft =======');

     logger($previewsTokensLeft);

                  $accessKey = config('const.autoupdate_keepa_api_key');

                   $apiUrl = "https://api.keepa.com/tracking/?key=".$accessKey."&"."type=removeAll";

                   $curl = curl_init();

                   curl_setopt_array($curl, array(
                       CURLOPT_URL => $apiUrl,
                       CURLOPT_RETURNTRANSFER => true,
                       CURLOPT_ENCODING => '',
                       CURLOPT_MAXREDIRS => 10,
                       CURLOPT_TIMEOUT => 0,
                       CURLOPT_FOLLOWLOCATION => true,
                       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                       CURLOPT_CUSTOMREQUEST => 'GET',
                       CURLOPT_HTTPHEADER => array(
                           "Content-Type: application/json",
                       ),
                   ));

                   $response = curl_exec($curl);

                   logger("------------------------------------------------");
                   logger("Resposne ");
                   logger(json_encode($response));
                   logger("------------------------------------------------");

                   $response = json_decode($response);

                   $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                   curl_close($curl);

                   DB::table('products')
                   ->where('is_tracked', 1)
                   ->update(['is_tracked' => 0, 'updateInterval' => 0]);

                   $currentTokensLeft = $response->tokensLeft;

                  logger("currentTokensLeft => ". $currentTokensLeft);


       logger('======= get tokenstatus after aPI call =======');
      // $this->getKeepaTokenStatus();


      return Response::json(['success' => true,"data" => $response, "message" => "Remove All Product Tracking asin successfully."], 200);


         logger("=============END::Keepa Remove bulk produc trackingt====================");

    }catch(\Exception $e){

       logger("=============ERROR::Keepa Remove bulk produc trackingt====================");
       return Response::json([
           "error" => $e->getMessage()
       ],422);

    }
}

    public function getKeepaTokenStatus(){
        try{
            $apiKey = config('const.autoupdate_keepa_api_key');
            $api = new KeepaAPI($apiKey);
            $r = KeepaAPIRequest::getTokenStatusRequest();
            $response = $api->sendRequestWithRetry($r);

            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => $response->url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);

            logger("**************************");
            logger("Token === response");
            logger("**************************");
            logger($response);
            logger("**************************");

            curl_close($curl);

            $res = json_decode($response);

            // Trigger mail to the admin if token flow reduction is greater
            if($res->tokenFlowReduction >= 18) {
                sendFlowReductionMail($apiKey, $res);
            }

            return $res;

        }catch(\Exception $e){
            logger('=============== ERROR:: getKeepaTokenStatus ===============');
            logger($e);
        }
    }


}
