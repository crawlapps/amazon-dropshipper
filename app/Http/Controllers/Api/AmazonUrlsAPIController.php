<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use App\Jobs\getUrlsWebhookJob;

class AmazonUrlsAPIController extends Controller
{
    public function getUrlsWebhook(Request $request){

        try{

            logger("=============START :: Webhook(callback_url) :: getUrls===================");

            $input = $request->all();

            logger("Input");
            logger(json_encode($input));

            $resposne = $input['data'];

            getUrlsWebhookJob::dispatch($resposne);

            return  Response::json(['success' => true,"message" => "Webhook Run successfully." ], 200);

            logger("=============END :: Webhook(callback_url) :: getUrls===================");

        }catch(\Exception $e){
            logger("=============ERROR :: Webhook(callback_url) :: getUrls===================");
            logger($e);
            return Response::json(["data" => $e],422);
        }

    }
}
