<?php

namespace App\Http\Controllers;

use App\Models\Counters;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Response;
use App\Plans;
use App\PlanContents;

class PlansController extends Controller
{
    public function index(Request $request){

        try{

            $plansData = Plans::with('plan_info')->select("*")->get()->toArray();

            if($plansData) {

                $items = [];

                foreach($plansData as $key=>$plan){
                    $item = [];
                    $item = $plan;
                    $item->max_regular_product_import = is_numeric($plan->max_regular_product_import) ? number_format($plan->max_regular_product_import) : ucfirst($plan->max_regular_product_import);

                    $item->max_affiliate_product_import = is_numeric($plan->max_affiliate_product_import)  ? number_format($plan->max_affiliate_product_import) : ucfirst($plan->max_affiliate_product_import);

                    array_push($items,$item);

                   }


                return Response::json(['success' => true,"data" => $items, "message" => "Plans retrieved successfully."], 200);
            }

            return Response::json(['success' => false,"data" => [], "message" => "Plans retrieved failed."], 200);

        }catch(\Exception $e){
            logger("=============ERROR ::  plans.index===================");
            return Response::json(["data" => $e],422);
        }

    }

    public function getPlansByInterval(Request $request,$interval){

        try{


            $plansData = Plans::with('plan_info')->select("*")->where('interval',$interval)->where('is_active', true)->orWhere('name','FREE')->get();

            if($plansData) {

                $items = [];

                foreach($plansData as $key=>$plan){
                    $item = [];
                    $item = $plan;

                    $item->max_regular_product_import = is_numeric($plan->max_regular_product_import) ? number_format($plan->max_regular_product_import) : ucfirst($plan->max_regular_product_import);

                    $item->max_affiliate_product_import = is_numeric($plan->max_affiliate_product_import)  ? number_format($plan->max_affiliate_product_import) : ucfirst($plan->max_affiliate_product_import);

                    $item->plan_contents = isset($plan->plan_info['contents']) ? $plan->plan_info['contents'] : [];

                    unset($item->plan_info);

                    array_push($items,$item);

                }

                return Response::json(['success' => true,"data" => $items, "message" => "Plans retrieved successfully."], 200);
            }

            return Response::json(['success' => false,"data" => [], "message" => "Plans retrieved failed."], 200);

        }catch(\Exception $e){
            logger("=============ERROR ::  plans.index===================");
            return Response::json(["data" => $e],422);
        }

    }


    public function setPlan(Request $request)
    {
        $shop = Auth::user();

        $errors = [];

        $planID = intval($request->post('curr_plan_id'));
        $newPlan = intval($request->post('new_plan_id'));

        $charge = DB::table('charges')->where('user_id', $shop->id)->where('status', 'ACTIVE')->orderBy('created_at', 'desc')->first();

        $counter = Counters::where('user_id', $shop->id)->where('status', 'active')->where('charge_id', $charge->id)->first();

        $plan = DB::table('plans')->where('id', $newPlan)->first();


        $downgrade_err_msg = 'For this billing cycle ['.  $counter->start_date .' - '. $counter->end_date .'] you have
                                executed more imports than the ones allowed by the
                                ['. $plan->name .' plan]. If You want to downgrade you
                                have to wait until the date of Billing End cycle.';

        $current_plan = DB::table('plans')->where('id', $planID)->get();

           if(count($current_plan)<=0){
                $errors[] = 'Invalid Plan ID: ' . $planID . '.';
            }else{
                if($planID > 1 && ($counter->regular_product_count >= $plan->max_regular_product_import || (   $plan->max_affiliate_product_import != 'unlimited' && $counter->affiliate_product_count >= $plan->max_affiliate_product_import))){
                    $errors[] = $downgrade_err_msg;
                }
            }


//        if ( ($planID < 1 || $planID > 5) || ($newPlan < 1 || $newPlan > 5)) {
//            $errors[] = 'Invalid Plan ID: ' . $planID . '.';
//        }else if( $planID >= 2 && $newPlan == 1){   // for downgrade plan
//            $errors[] = 'you can not downgrade your plan with free plan.';
//        }else if( $planID == 3 && $newPlan == 2 ){   // check counter to downgrade plan
//            if( $counter->regular_product_count > 15 ){
//                $errors[] = $downgrade_err_msg;
//            }
//        }else if( $planID == 4 && $newPlan == 2 ){
//            if( $counter->regular_product_count > 15 ){
//                $errors[] = $downgrade_err_msg;
//            }
//        }else if( $planID == 4 && $newPlan == 3 ){
//            if( $counter->regular_product_count > 100 ){
//                $errors[] = $downgrade_err_msg;
//            }
//        }else if( $planID == 5 && $newPlan == 2 ){
//            if( $counter->regular_product_count > 15 ){
//                $errors[] = $downgrade_err_msg;
//            }
//        }else if( $planID == 5 && $newPlan == 3 ){
//            if( $counter->regular_product_count > 100 ){
//                $errors[] = $downgrade_err_msg;
//            }
//        }else if( $planID == 5 && $newPlan == 4 ){
//            if( $counter->regular_product_count > 500 ){
//                $errors[] = $downgrade_err_msg;
//            }
//        }
        if (count($errors)>0) {
            return ['success' => false, 'errors' => $errors];
        }

        // Save
        try {
            DB::table('users')
                ->where('id', $shop->id)
                ->update(array('plan_id' => $planID));

        } catch(QueryException $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }
        return ['success' => true, 'errors' => $errors, 'plan_id' => $planID];
    }
}
