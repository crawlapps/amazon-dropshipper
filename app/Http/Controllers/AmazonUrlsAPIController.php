<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AmazonUrlsAPIAdds;
use App\Models\AmazonUrlsAPIGets;
use App\Jobs\AmazonUrlsAPIAddsJob;
use App\Jobs\AmazonUrlsAPIDeletesJob;
use App\Jobs\AmazonUrlsAPIDeletesAllJob;
use Response;
use Exception;
use Artisan;
use App\Jobs\FreemiumDailyListJob;
class AmazonUrlsAPIController extends Controller
{

    public function __construct(){

    }

    public function addUrls(Request $request){

         try{

             AmazonUrlsAPIAddsJob::dispatch();

             return Response::json([
                 'success' => true,
                 'message' => "Urls Add SuccessFully"

             ],200);

         }
         catch(Exception $e){
             return Response::json([
                 "success" => false,
                 "data" => [],
                 "message" => $e->getMessage()
             ],422);
         }

    }


    public function getUrls(Request $request){

        try{

            Artisan::call('amazon-get-urls:cron');

            return Response::json([
                'success' => true,
                'message' => "Urls Gets SuccessFully"

            ],200);

          }
          catch(Exception $e){
                return Response::json([
                    "success" => false,
                    "data" => [],
                    "message" => $e->getMessage()
                ],422);
            }

    }


    public function deleteUrls(Request $request){

        try{

            AmazonUrlsAPIDeletesJob::dispatch();

            return Response::json([
                'success' => true,
                'message' => "Urls Deletes SuccessFully"

            ],200);

        }
        catch(Exception $e){
            return Response::json([
                "success" => false,
                "data" => [],
                "message" => $e->getMessage()
            ],422);
        }

    }

    public function deleteAllUrls(Request $request){

        try{

            AmazonUrlsAPIDeletesAllJob::dispatch();

            return Response::json([
                'success' => true,
                'message' => "All Urls Deletes SuccessFully"

            ],200);

        }
        catch(Exception $e){
            return Response::json([
                "success" => false,
                "data" => [],
                "message" => $e->getMessage()
            ],422);
        }

    }






    public function freemiumDailyList(Request $request){

        try{

           // FreemiumDailyListJob::dispatch();
            Artisan::call('freemium-daily-list');

            return Response::json([
                'success' => true,
                'message' => "FreemiumDailyListCron run SuccessFully"

            ],200);

        }
        catch(Exception $e){
            return Response::json([
                "success" => false,
                "data" => [],
                "message" => $e->getMessage()
            ],422);
        }

    }


    public function subscribedDailyList(Request $request){

        try{

            Artisan::call('subscribed-daily-list');

            return Response::json([
                'success' => true,
                'message' => "subscribedDailyListCron run SuccessFully"

            ],200);

        }
        catch(Exception $e){
            return Response::json([
                "success" => false,
                "data" => [],
                "message" => $e->getMessage()
            ],422);
        }

    }



    public function mainDailyList(Request $request){

        try{

            Artisan::call('main-daily-list');

            return Response::json([
                'success' => true,
                'message' => "mainDailyListCron run SuccessFully"

            ],200);

        }
        catch(Exception $e){
            return Response::json([
                "success" => false,
                "data" => [],
                "message" => $e->getMessage()
            ],422);
        }

    }



}
