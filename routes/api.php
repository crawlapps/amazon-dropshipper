<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['middleware' => ['cors'], 'namespace' => 'Api'], function () {
    Route::get('check-affiliate', 'ProductController@checkAffiliate');
    Route::get('webhook/set/track-product', 'ProductController@keepaTrackProductWebhookSet');  // https://amazon-dropshipper.test/api/webhook/set/track-product
    Route::POST('webhook/track-product', 'ProductController@keepaTrackProductWebhook')->name('keepa.webhook'); // https://amazon-dropshipper.test/api/webhook/track-product

    Route::get('keepa/name-list', 'ProductController@keepaListNames'); // https://amazon-dropshipper.test/api/keepa/name-list
    Route::POST('keepa/get/track-product/', 'ProductController@getProductTracking'); //https://amazon-dropshipper.test/api/keepa/get/track-product

   // https://amazonedropshipping.crawlapps.com/api/keepa/name/list

    Route::POST('keepa/add/track-product/', 'ProductController@addProductTracking'); //https://amazonedropshipping.crawlapps.com/api/keepa/add/track-product
    Route::POST('webhook/amazon/get/urls', 'AmazonUrlsAPIController@getUrlsWebhook')->name('urls.webhook');

     //https://amazonedropshipping.crawlapps.com/api/webhook/amazon/get/urls
     //https://amazon-dropshipper.test/api/webhook/webhook/amazon/get/urls


    //Add Tracking  
    Route::get('keepa/add/track-product/script', 'ProductController@addProductTrackingScript'); 
     //https://amazonedropshipping.crawlapps.com/api/keepa/add/track-product/script
     //https://amazon-dropshipper.test/api/keepa/add/track-product/script


    //Remove Tracking 
    Route::get('keepa/remove/track-product/script', 'ProductController@removeProductTrackingScript'); 
      //https://amazonedropshipping.crawlapps.com/api/keepa/remove/track-product/script
      //https://amazon-dropshipper.test/api/keepa/remove/track-product/script


    //Remove Tracking ALL LIST  
     Route::get('keepa/remove/all/track-product/script', 'ProductController@removeAllProductTrackingScript'); 
      //https://amazonedropshipping.crawlapps.com/api/keepa/remove/all/track-product/script
      //https://amazon-dropshipper.test/api/keepa/remove/all/track-product/script


     //Token Status
    Route::get('keepa/auto-update-token-status/script', 'ProductController@getKeepaTokenStatus'); 
    
      //https://amazonedropshipping.crawlapps.com/api/keepa/auto-update-token-status/script
      //https://amazon-dropshipper.test/api/keepa/auto-update-token-status/script
   

});

//Route::post('/get-amazon-product', 'AppController@getAmazonProduct');
//Route::post('/get-walmart-product', 'AppController@getWalmartProduct');
//Route::post('/get-collections', 'AppController@getCollections');
//Route::post('/add-product', 'AppController@addProduct')->middleware(['auth.shopify']);


