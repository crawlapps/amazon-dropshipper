<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show custom-sdbar" id="sidebar">
    <div class="logo-font">

        <?php if(isset($global_content)): ?>
            <span><?php echo e($global_content->side_bar_logo_text1); ?></span>
            <span class="sub-lg"><?php echo e($global_content->side_bar_logo_text2); ?></span>
        <?php else: ?>
            <span>AmaZone</span>
            <span class="sub-lg">DropShipper</span>
        <?php endif; ?>




    </div>
    <ul class="c-sidebar-nav mt-3">
        <?php if(isset($sidebar_contents) && count($sidebar_contents)>0): ?>
            <?php $__currentLoopData = $sidebar_contents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link<?php echo e(request()->is($menu->menu_link_redirect_to) ? ' c-active' : ''); ?> <?php echo e(!$menu->status ? 'disabled': ''); ?>" style="<?php echo e(!$menu->status ? 'pointer-events: none; color: #6c757d' : ''); ?>" href="<?php echo e($menu->menu_link_redirect_to); ?>">
                        <img src="<?php echo e(asset($menu->menu_icon)); ?>" class="<?php echo e($menu->menu_slug=="import-products" ? "import-home-img" : ''); ?>"  style="<?php echo e(!$menu->status ? 'color: #6c757d' : ''); ?>"  alt="<?php echo e($menu->menu_title); ?>" />
                        
                    <?php echo e($menu->menu_title); ?>


                    </a>
                </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
       <?php else: ?>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link<?php echo e(request()->is('/import-home') ? ' c-active' : ''); ?>" href="/import-home">
                <img src="<?php echo e(asset('images/sidebar/import.png')); ?>" class="import-home-img" alt="Import Products" />
                Import Products
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link<?php echo e(request()->is('/products') ? ' c-active' : ''); ?>" href="/products">
                <img src="<?php echo e(asset('images/sidebar/task.png')); ?>"  alt="My Products" />
                My Products
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link<?php echo e(request()->is('/orders') ? ' c-active' : ''); ?>" href="/orders">
                <img src="<?php echo e(asset('images/sidebar/shopping-bag.png')); ?>"  alt="My Orders" />
                My Orders
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link<?php echo e(request()->is('/help') ? ' c-active' : ''); ?>" href="/help">
                <img src="<?php echo e(asset('images/sidebar/information.png')); ?>"  alt="Help / Videos / FAQ" />
                Help / Videos / FAQ
            </a>
        </li>

        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link<?php echo e(request()->is('/settings') ? ' c-active' : ''); ?>" href="/settings">
                <img src="<?php echo e(asset('images/sidebar/settings.png')); ?>"  alt="Settings" />
                Settings
            </a>
        </li>
        <?php endif; ?>
   














    </ul>
</div>



<?php /**PATH /var/www/html/shopify-apps/amazon-dropshipper/resources/views/layouts/sidebar.blade.php ENDPATH**/ ?>