<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


        <!-- Scripts -->
        <script src="<?php echo e(mix('js/app.js')); ?>" defer></script>

        <!-- Styles -->
        <link href="<?php echo e(mix('css/app.css')); ?>" rel="stylesheet">
        <link rel="icon" href="<?php echo e(asset('/images/favicon.ico')); ?>">

        <?php
        header("Content-Security-Policy: frame-ancestors https://".auth()->user()->name."  https://admin.shopify.com");
        ?>

    </head>
    <body class="c-app">
        <div id="app">
            <?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="c-wrapper">
                <?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <div class="c-body">
                    <main class="c-main">
                        <div class="container-fluid">
                            <div id="ui-view">
                                <div>
                                    <div class="fade-in">
                                        <?php echo $__env->yieldContent('content'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
                <footer class="c-footer">
                    <div>
                        <?php if(isset($global_content)): ?>
                            <a href="<?php echo e($global_content->footer_cpr_link); ?>"><?php echo e($global_content->footer_cpr_text1); ?></a> 
                            <a href="<?php echo e($global_content->footer_cpr_link); ?>privacy-policy" style="margin: 0 12px">Privacy Policy</a>
                            <?php echo e($global_content->footer_cpr_text2); ?>

                        <?php else: ?>
                            <a href="https://amazonedropshipping.com/">AmaZone Dropshipper</a> © <?php echo e(2018); ?> AmaZone Dropshipper.
                        <?php endif; ?>
                    </div>

                </footer>
            </div>
        </div>

    </body>


</html>
<?php /**PATH /var/www/html/shopify-apps/amazon-dropshipper/resources/views/layouts/app.blade.php ENDPATH**/ ?>