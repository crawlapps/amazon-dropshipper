<?php $__env->startSection('title', ' | Import Products'); ?>

<?php $__env->startSection('content'); ?>

    <div class="import-page">
    <div class="import-top-info">

        <p><span class="import-pre-text"><?php echo e(\Carbon\Carbon::now()->format('M d')); ?>:</span>
            <?php echo e(isset($page_contents['import_top_info_text_1']) ? $page_contents['import_top_info_text_1'] : "Walmart Import Issue is being investigated."); ?>

        </p>
        <p><span class="import-pre-text"><?php echo e(\Carbon\Carbon::now()->format('M d')); ?>:</span>
            <?php echo e(isset($page_contents['import_top_info_text_2']) ? $page_contents['import_top_info_text_2'] : "Download our new Chrome Extenstion to Import with  1 click and Auto fulfill orders. (soon)"); ?>

        </p>

    </div>


<?php if($page_contents['section2_top_title']!==null && $page_contents['section2_top_title']!==''): ?>
        <section>
        <div class="row text-center" style="width: 779px;margin: auto;">

            <div class="col-md-12">
            <h5 class="card-title"><?php echo e($page_contents['section2_top_title']); ?></h5>
            </div>

            <div class="col-md-6">
               <a href="<?php echo e($page_contents['section2_col1_link']); ?>" target="_blank" style="text-decoration: none;">
                <div class="card mb-3" style="max-width: 540px;border: none;">
                    <div class="row g-0">
                        <div class="col-md-4" style="display: flex;justify-content: center;align-items: center;">
                            <?php if($page_contents['section2_col1_image']!==null && $page_contents['section2_col1_image']!==''): ?>
                            <img src="<?php echo e(asset($page_contents['section2_col1_image'])); ?>" class="img-fluid rounded-start" alt="...">
                            <?php endif; ?>
                        </div>
                        <div class="col-md-8">
                            <div class="card-body" style="text-align: left;padding-left: 0px;">
                                <h5 class="card-title" style="color: #eda90c;"><?php echo e($page_contents['section2_col1_heading']); ?></h5>
                                <p class="card-text" style="color: black;"><?php echo e($page_contents['section2_col1_sub_title']); ?></p>
                                <div class="card-text" style="color: black;"> <?php echo $page_contents['section2_col1_content']; ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                </a>
            </div>

           <div class="col-md-6">
               <a href="<?php echo e($page_contents['section2_col2_link']); ?>" target="_blank" style="text-decoration: none;">
                   <div class="card mb-3" style="max-width: 540px;border: none;">
                       <div class="row g-0">
                           <div class="col-md-4" style="display: flex;justify-content: center;align-items: center;">
                            <?php if($page_contents['section2_col2_image']!==null && $page_contents['section2_col2_image']!==''): ?>
                                   <img src="<?php echo e(asset($page_contents['section2_col2_image'])); ?>" class="img-fluid rounded-start" alt="...">
                            <?php endif; ?>
                           </div>
                           <div class="col-md-8">
                               <div class="card-body" style="text-align: left;padding-left: 0px;">
                                   <h5 class="card-title" style="color: #eda90c;"><?php echo e($page_contents['section2_col2_heading']); ?></h5>
                                   <p class="card-text" style="color: black;"><?php echo e($page_contents['section2_col2_sub_title']); ?></p>
                                   <div class="card-text" style="color: black;"><?php echo $page_contents['section2_col2_content']; ?></div>
                               </div>
                           </div>
                       </div>
                   </div>
               </a>
           </div>

        </div>
        </section>
<?php endif; ?>
<h3 class="import-search-heading"> <?php echo e(isset($page_contents['import_search_heading']) ? $page_contents['import_search_heading'] : "Import and DropShip any Product from Amazon or Walmart"); ?></h3>

        <div class="row justify-content-center import-page">
            <div class="col-md-9">
                <!--                <p>Enter product URL from Amazon or Walmart online store, then press the Get Product button.</p>-->
                <p class="sub-title">
                    <?php echo e(isset($page_contents['import_search_sub_title']) ? $page_contents['import_search_sub_title'] : "Amazon US, CA, UK, DE, FR, SP, IT, BR, MX, IN and Walmart US"); ?>

                </p>
                <div class="import-title-divider"></div>
                <p class="import-guid-text">
                    <?php echo e(isset($page_contents['import_guid_text1']) ? $page_contents['import_guid_text1'] : "Step 1: Go to Amazon or Walmart and Copy the Product Page URL. Step 2: Paste in the box below and click"); ?>

                </p>
                <p class="import-guid-text">
                    <?php echo e(isset($page_contents['import_guid_text2']) ? $page_contents['import_guid_text2'] : "Get Product or download our Chrome extenstion and directy import from the Product page (soon)."); ?>

                </p>
            </div>
        </div>
                <product-importer :page_contents="<?php echo e(json_encode($page_contents)); ?>"></product-importer>
    </div>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/shopify-apps/amazon-dropshipper/resources/views/import.blade.php ENDPATH**/ ?>