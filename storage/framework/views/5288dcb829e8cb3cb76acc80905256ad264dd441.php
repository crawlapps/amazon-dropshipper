<?php $__env->startSection('title', ' | My Orders'); ?>

<?php $__env->startSection('content'); ?>
<h1> <?php echo e(isset($page_title) ? $page_title : "My Orders"); ?></h1>
<p>
    <?php echo e(isset($page_contents['top_text1']) ? $page_contents['top_text1'] : "Here you can view the orders for your imported products. You can see Amazone/Walmart Link, Shopify Link, Client Details, Order Details, Price."); ?>

</p>

    <?php if(isset($page_contents['top_text2_content'])): ?>
        <?php echo $page_contents['top_text2_content']; ?>

    <?php else: ?>
        <p style="color: #841111;"> The user is responsible for placing and fulfilling your orders. Please <a href="FAQ.php#orderhandling" target="_blank">read the FAQ section 'Order Handling' or click here</a> for more info. If you have questions please visit our FAQ section or Contact Us.</p>
   <?php endif; ?>

<orders-manager ids=<?php echo e(json_encode($ids)); ?>></orders-manager>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/shopify-apps/amazon-dropshipper/resources/views/orders.blade.php ENDPATH**/ ?>